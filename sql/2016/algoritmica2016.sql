-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.6.11


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema algoritmica
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ algoritmica;
USE algoritmica;

--
-- Table structure for table `algoritmica`.`ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
CREATE TABLE `ciudad` (
  `codigo_ciudad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `distancia_a_rio_cuarto` float NOT NULL,
  PRIMARY KEY (`codigo_ciudad`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `algoritmica`.`ciudad`
--

/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` (`codigo_ciudad`,`nombre`,`distancia_a_rio_cuarto`) VALUES 
 (1,'Campana (Esso)',580),
 (2,'Rosario',420),
 (3,'Villa Maria',120),
 (4,'Cordoba',220),
 (5,'Buenos Aires',600),
 (6,'Santa Rosa (LP)',400),
 (7,'Rio Cuarto',0),
 (8,'Felipe yofre',1000),
 (9,'Sunchales',450),
 (10,'San Lorenzo',420),
 (11,'Balnearia',400),
 (12,'San Agustin',140),
 (13,'San Andres de giles',580),
 (14,'Corral de Bustos',300),
 (15,'Embalse',110),
 (16,'Ramona',400),
 (17,'Holber',15),
 (18,'SAN MARTIN 2',0),
 (19,'RAMALLO',0),
 (20,'GRA, VILLEGAS',0),
 (21,'FIRMAT',0),
 (22,'Huinca Renanco',0),
 (23,'La Plata(YPF)',0),
 (24,'Holberg',0),
 (25,'Ibarlucea',0),
 (26,'Gral Villegas',0),
 (27,'San Martin',0),
 (28,'Mendoza',0),
 (29,'Dock Sud',0),
 (30,'San Marcos',0),
 (31,'Santa Rosa De Calamuchita',0),
 (32,'Arizona',0),
 (33,'Sauce Viejo',0),
 (34,'Resistencia',0),
 (35,'El Faro',0),
 (36,'Villa del Totoral',0),
 (38,'Gral. Deheza',0),
 (39,'Puerto Gral. San Martin',0),
 (40,'Terminal 6',0),
 (41,'Villa Maria de Rio Seco',0);
INSERT INTO `ciudad` (`codigo_ciudad`,`nombre`,`distancia_a_rio_cuarto`) VALUES
 (42,'Santiago del Estero',0),
 (43,'Anatuya',0),
 (44,'Tacanitas',0),
 (45,'Pescadores',0),
 (46,'Pilar (Bs. As.)',0),
 (47,'Firmat',0),
 (48,'Puerto Gral. San Martin',0),
 (49,'Colonia Caroya',0),
 (50,'San Agustin',0),
 (51,'Villa Retiro',0),
 (52,'Dean Funes',0),
 (53,'Venado Tuerto',0),
 (54,'Las Arrias',0),
 (55,'Italo Pincen',0),
 (56,'Del Campillo',0),
 (57,'Santa Fe',0),
 (58,'Pincen',0),
 (59,'Tostado',0),
 (60,'San Luis',0),
 (61,'Chabas',0),
 (62,'San Martin (Chaco)',0),
 (63,'Bell Ville',0),
 (64,'Durazno (Entre Rios)',0),
 (65,'Arrecifes',0),
 (66,'Devoto',0),
 (67,'Moldes',0),
 (68,'Villa Dolores',0),
 (69,'La Lomitas',0),
 (70,'Alcorta',0),
 (71,'Lima',0),
 (72,'Chilacito',0),
 (73,'Charlone',0),
 (74,'Maria Teresa',0),
 (75,'Bahia Blanca (Petrobras)',0),
 (76,'Villa Nueva',0),
 (77,'El Manzano',0),
 (78,'Villa Mercedes',0),
 (79,'Las Zanjitas',0),
 (80,'Morteros',0),
 (81,'La Toma',0),
 (82,'Ubuajay',0),
 (83,'Jesus Maria',0);
INSERT INTO `ciudad` (`codigo_ciudad`,`nombre`,`distancia_a_rio_cuarto`) VALUES
 (84,'Villa Guillermina',0),
 (85,'Basavilbaso ',0),
 (86,'Nueva Esperanza',0),
 (87,'Florencio Varela',0),
 (88,'Marull',0),
 (89,'villa Guayasan',0),
 (90,'Guayasan',0),
 (91,'pajonal',0),
 (92,'Junin',0),
 (93,'Villa Angela',0),
 (94,'Mocoreta',0),
 (95,'Centeno',0),
 (96,'Dos Hermanas',0),
 (97,'Esperanza',0),
 (98,'Cruz Del Eje',0),
 (99,'Villa Guay',0),
 (100,'Posadas',0),
 (101,'Christophersen',0),
 (102,'Pozo Borrado',0),
 (103,'San Javier',0),
 (104,'San Tome',0),
 (105,'Tucuman',0),
 (106,'Tatane ',0),
 (107,'Puchetta',0),
 (108,'Mercedes',0),
 (109,'Paraná',0),
 (110,'Romang',0),
 (111,'San Pedro (Misiones)',0),
 (112,'Gral. Rodriguez',0),
 (113,'Capital Sarmiento',0),
 (114,'La Banda (stgo)',0),
 (115,'Gral. Conesa',0),
 (116,'Campana Rhasa',0),
 (117,'Lujan de Cuyo',0),
 (118,'Crespo',0),
 (119,'PILAR (CBA)',0),
 (120,'colonia maria luisa',0),
 (121,'Banderas',0),
 (122,'MAR DEL PLATA',0),
 (123,'dalmacio velez sarfield',0),
 (124,'POTRERO DE GARY',0);
INSERT INTO `ciudad` (`codigo_ciudad`,`nombre`,`distancia_a_rio_cuarto`) VALUES
 (125,'Piedras blancas',0);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
