CREATE DATABASE IF NOT EXISTS  ejercicio32anio2018;
USE ejercicio32anio2018;

-- personas (dni, nombre, direccion)
DROP TABLE IF EXISTS personas cascade;
CREATE TABLE  personas(
	dni int not null,
	nombre varchar(20),
	direccion varchar(45),
	primary key (dni)
)ENGINE=INNODB;


DROP TABLE IF EXISTS `alumno`;
CREATE TABLE `alumno` (
  `nro_alumno` int(11) NOT NULL,
  `dni` int(11) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellido` VARCHAR(45) NOT NULL,
  `sexo` VARCHAR(20) NOT NULL,
  PRIMARY KEY  (`nro_alumno`)
)

ENGINE=INNODB;

-- tambos (cod_tambo, nombre_tambo, dni_dueño, dni_tambero)
DROP TABLE IF EXISTS  tambos cascade;
CREATE TABLE  tambos(
	cod_tambo int not null,
	nombre_tambo varchar(40),
	dni_dueno int references personas(dni) on delete cascade,
	dni_tambero int references personas(dni) on delete cascade,
	check (cod_tambo>0),
	primary key (cod_tambo)
)engine=InnoDB;

-- ventas (nro_venta,cod_tambo, año_venta, monto_venta, %_descuento)
DROP TABLE IF EXISTS  `ventas` cascade;
CREATE TABLE  `ventas`(
	nro_venta int not null,
	cod_tambo int references tambos(cod_tambo) on delete no action,
	ano_venta enum ('2011', '2013', '2014','2016','2017','2018'),
	monto_venta int not null,
	descuento numeric(10,2),
	primary key (nro_venta)
);

-- 3 c)
-- Categoria(nro_categoria, descripción)
DROP TABLE IF EXISTS  categoria cascade;
CREATE TABLE  categoria(
	nro_categoria int not null,
	descripcion varchar(30),
	primary key (nro_categoria)
);

-- Producto(nro_producto, descripcion, color, nro_ categoria)

DROP TABLE IF EXISTS  ejercicio32anio2018.producto cascade;
CREATE TABLE  ejercicio32anio2018.producto(
	nro_producto int not null,
	descripcion varchar(30),
	color enum ('blanco', 'negro', 'azul'),
	nro_categoria int references ejercicio32anio2018.categoria(nro_categoria),
	check (nro_producto >0),
	primary key (nro_producto)
);

-- Proveedor(nro_proveedor, nombre, apellido, dirección)
DROP TABLE IF EXISTS  ejercicio32anio2018.proveedor cascade;
CREATE TABLE  ejercicio32anio2018.proveedor(
	nro_proveedor int not null,
	nombre varchar(30),
	apellido varchar(30),
	dir varchar(30),
	primary key (nro_proveedor)
);

-- Suministra(nro_proveedor, nro_producto)
DROP TABLE IF EXISTS  ejercicio32anio2018.suministra cascade;
CREATE TABLE  ejercicio32anio2018.suministra(
	nro_proveedor int references ejercicio32anio2018.proveedor(nro_proveedor),
	nro_producto int references ejercicio32anio2018.producto(nro_producto),
	primary key (nro_proveedor, nro_producto)
);

insert into tambos values
(1,null,null,null),
(2,null,null,null),
(3,null,null,null),
(4,null,null,null);

        
insert into ventas values
(1,1,null,300,null),
(2,1,null,100,null),
(3,2,null,100,null),
(4,2,null,10,null),
(5,3,null,100,null),
(6,3,null,100,null),
(7,7,null,1000,null),
(8,7,null,1000,null);

-- 5) a 
 select cod_tambo,avg(monto_venta) from ventas v group by cod_tambo having avg(monto_venta) > 120;
-- b 
select dni,nombre, x.mx, y.mm from personas p natural join 
    (select t.dni_dueno, x.mx from tambos t 
		natural join (select cod_tambo, max(monto_venta) as mx from ventas group by cod_tambo) x) x
	natural join
    (select t.dni_dueno, y.mm from tambos t 
		natural join (select cod_tambo, min(monto_venta) as mm from ventas group by cod_tambo) y) y;
-- disgusting code 

-- c
select p.dni, x.co from personas p natural join
	(select t.dni_dueno, y.co from tambos t natural join
		(select cod_tambo, count(cod_tambo) as co from ventas) y
		) x;
        

drop trigger if exists mayus_desc;
create trigger mayus_desc before insert on categoria 
	for each row
		set new.descripcion = upper(new.descripcion);