 show tables;
-- show columns from cliente;
-- select * from itemfactura;
-- a
-- select* from producto where PRECIO < 850;
-- b
-- select producto.COD_PRODUCTO,DESCRIPCION from itemfactura, producto where itemfactura.CANTIDAD >7;
-- c
-- select * from cliente c where c.NRO_CLIENTE not in (select f.NRO_CLIENTE from factura f) order by c.APELLIDO, c.NOMBRE desc;
-- d
-- select * from producto p where p.COD_PRODUCTO not in (select i.COD_PRODUCTO from itemfactura i);
-- e
/* select * from producto p 
natural join (
	select sum(CANTIDAD) as cant,i.COD_PRODUCTO
    from itemfactura i
    group by i.COD_PRODUCTO
    )g
where g.cant >=5; 
*/
-- f
-- select c.* from cliente c natural left outer join factura;
