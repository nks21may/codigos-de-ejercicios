drop schema if exists ejercicio1anio2018 cascade;
CREATE schema ejercicio1anio2018;


-- 1) a
-- Artículo (#art, descripción, precio, cantidad, Stock_Min, Stock_Max, Mes_Ult_Movim,Fecha_Vto)
DROP TABLE IF EXISTS  ejercicio1anio2018.articulo cascade;
CREATE TABLE  ejercicio1anio2018.articulo(
   nro_art  int NOT NULL,
   DESCRIPCION  varchar(40) default NULL,
   PRECIO  decimal(9,2) default NULL,
	cantidad int default 0,
	stock_min int not null,
	stock_max int not null,
	mes_ult_mov date,
	fecha_vto date,
	check (precio>0),
	check (stock_min <= stock_max),
  PRIMARY KEY  (nro_art)
);

-- 1) b
--personas (dni, nombre, direccion)
DROP TABLE IF EXISTS  ejercicio1anio2018.personas cascade;
CREATE TABLE  ejercicio1anio2018.personas(
	dni int not null,
	nombre varchar(20),
	direccion varchar(45),
	primary key (dni)
);



--tambos (cod_tambo, nombre_tambo, dni_dueño, dni_tambero)
DROP TABLE IF EXISTS  ejercicio1anio2018.tambos cascade;
CREATE TABLE  ejercicio1anio2018.tambos(
	cod_tambo int not null,
	nombre_tambo varchar(40),
	dni_dueno int references ejercicio1anio2018.personas(dni),
	dni_tambero int references ejercicio1anio2018.personas(dni),
	check (cod_tambo>0),
	primary key (cod_tambo)
);

drop type if exists ejercicio1anio2018.ano cascade;
create type ejercicio1anio2018.ano as enum ('2011', '2013', '2014','2016','2017','2018');
--ventas (nro_venta,cod_tambo, año_venta, monto_venta, %_descuento)
DROP TABLE IF EXISTS  ejercicio1anio2018.ventas cascade;
CREATE TABLE  ejercicio1anio2018.ventas(
	nro_venta int not null,
	cod_tambo int references ejercicio1anio2018.tambos(cod_tambo),
	ano_venta ejercicio1anio2018.ano,
	monto_venta int not null,
	descuento numeric(10,2),
	primary key (nro_venta)
);

-- c)
-- Categoria(nro_categoria, descripción)
DROP TABLE IF EXISTS  ejercicio1anio2018.categoria cascade;
CREATE TABLE  ejercicio1anio2018.categoria(
	nro_categoria int not null,
	descripcion varchar(30),
	primary key (nro_categoria)
);

-- Producto(nro_producto, descripcion, color, nro_ categoria)
drop type if exists ejercicio1anio2018.colore cascade;
create type ejercicio1anio2018.colore as enum ('blanco', 'negro', 'azul');

DROP TABLE IF EXISTS  ejercicio1anio2018.producto cascade;
CREATE TABLE  ejercicio1anio2018.producto(
	nro_producto int not null,
	descripcion varchar(30),
	color colore,
	nro_categoria int references ejercicio1anio2018.categoria(nro_categoria),
	check (nro_producto >0),
	primary key (nro_producto)
);

-- Proveedor(nro_proveedor, nombre, apellido, dirección)
DROP TABLE IF EXISTS  ejercicio1anio2018.proveedor cascade;
CREATE TABLE  ejercicio1anio2018.proveedor(
	nro_proveedor int not null,
	nombre varchar(30),
	apellido varchar(30),
	dir varchar(30),
	primary key (nro_proveedor)
);

-- Suministra(nro_proveedor, nro_producto)
DROP TABLE IF EXISTS  ejercicio1anio2018.suministra cascade;
CREATE TABLE  ejercicio1anio2018.suministra(
	nro_proveedor int references ejercicio1anio2018.proveedor(nro_proveedor),
	nro_producto int references ejercicio1anio2018.producto(nro_producto),
	primary key (nro_proveedor, nro_producto)
);

INSERT INTO ejercicio1anio2018.personas(dni, nombre, direccion) VALUES
	(1,'Juan','la calle'),
	(2,'Ramona','villa maria'),
	(3,'El Indio','Bajo el puente');
select * from ejercicio1anio2018.personas;

INSERT INTO ejercicio1anio2018.tambos(cod_tambo, nombre_tambo, dni_dueno, dni_tambero) VALUES
	(12,'Juan',1,2),
	(22,'Ramona',2,3),
	(32,'El Indio',3,3);
	
insert into ejercicio1anio2018.ventas(nro_venta,cod_tambo,ano_venta,monto_venta) values
	(1,12,'2017',200),
	(2,22,'2016',500),
	(3,32,'2018',1000);
-- select * from ejercicio1anio2018.tambos;
-- select * from information_schema.tables;