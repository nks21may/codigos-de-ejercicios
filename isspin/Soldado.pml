#define Soldado byte

chan unsafe_to_safe = [0] of {Soldado, Soldado};
chan safe_to_unsafe = [0] of {Soldado};
chan stopwatch = [0] of {Soldado};
byte time;

ltl p0{(<>(time>60))}

active proctype Unsafe()
{
	bit here[4];
	Soldado s1, s2;
	here[0] =1; here[1] =1; here[2] =1; here[3] =1; 
	do ::
		//Selecciona dos soldados que este del lado inseguro
		if
		:: here[0] == 1 -> s1 = 0;
		:: here[1] == 1 -> s1 = 1;
		:: here[2] == 1 -> s1 = 2;
		:: here[3] == 1 -> s1 = 3;
		fi
		here[s1] = 0;
		if
		:: here[0] == 1 -> s2 = 0;
		:: here[1] == 1 -> s2 = 1;
		:: here[2] == 1 -> s2 = 2;
		:: here[3] == 1 -> s2 = 3;
		fi
		here[s2] = 0;

		//envía los dos soldados seleccionados al lado seguro 
		unsafe_to_safe ! s1, s2;


		//Si ya no hay soldados del lado inseguro termina el proceso
		if
		:: here[0] == 0 && here[1] == 0 && here[2] == 0 && here[3] == 0-> break;
		:: else ->skip
		fi
		
		// Recibe el Soldado que  llega desde el lado seguro
		safe_to_unsafe ? s1;
		here[s1] = 1;
		//Agrega al tiempo total, el tiempo de este último Soldado
		stopwatch ! s1
	od
}

active proctype Timer()
{
	do
	:: stopwatch ? 0 -> atomic { time=time+5}
	:: stopwatch ? 1 -> atomic { time=time+10}
	:: stopwatch ? 2 -> atomic { time=time+20}
	:: stopwatch ? 3 -> atomic { time=time+25}
	od 
}

active proctype Safe()  {
//Inicia sin ningún Soldado del lado inseguro
	bit here[4];
	Soldado s1, s2;
	here[0] =0; here[1] =0; here[2] =0; here[3] =0; 
	do ::
		//Recibe los dos soldados que fueron enviados del lado inseguro 
		unsafe_to_safe ? s1, s2; 
		here[s1] = 1;		
		here[s2] = 1;
		//Agrega al tiempo total el tiempo del Soldado más lento recibido
		if
		:: s1 >= s2 -> stopwatch ! s1;
		::else stopwatch ! s2;
		fi

		//Si ya estan los 4 soldados del lado seguro termina el proceso
		if
		:: here[0] == 1 && here[1] == 1 && here[2] == 1 && here[3] == 1 -> break;
		:: else -> skip
		fi

		//Selecciona un Soldado que esté del lado seguro
		if
		:: here[0] == 1 -> s1 = 0;
		:: here[1] == 1 -> s1 = 1;
		:: here[2] == 1 -> s1 = 2;
		:: here[3] == 1 -> s1 = 3;
		fi

		//Envía el Soldado seleccionado al lado inseguro 
		here[s1] = 0;		
		safe_to_unsafe ! s1;
	od
}