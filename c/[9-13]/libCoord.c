#include <math.h>
#include <stdio.h>

struct Punto{
	int x;
	int y;
};

typedef struct Punto Punto;

int Batroden(Punto p, Punto cc, int r){
	return (r >= sqrt((pow(fabs(cc.x - p.x), 2) + pow(fabs(cc.y - p.y),2))));
}

int distan(Punto A, Punto B){
 	return sqrt(pow(fabs(B.x - A.x), 2) + pow(fabs(B.y - A.y),2));
}