//Nicolás D'Alessandro 2016. Compilado en debian con gcc
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libCoord.h"

#define n 3 //tamaño de la manzana vertical
#define m 3 //tamaño de la manzana horizontal

#define nc 10 //tamaño vertical de la sopa
#define mc 10 //tamaño horizontal de la sopa
#define pl 20 // largo maximo de la palabra 

int A, B, rc;

Punto punt, cc, recA, recB; //tipo Punto esta declarado en la libreria

int Brec, Bcir; //boleanos
int manz[n][m];	//manzana
int opcion;

char sopa[nc][mc]; 

int pertenece(int c, int v, Punto put){ //devuelve verdadero si el punto esta en la recta
	return (c*put.x+v == put.y);
}

Punto cargaPos(){ //carga variables de tipo punto
	Punto puc;
	int a, b;
	printf("Ingrese la x del punto\n");
	scanf("%d", &a);
	printf("Ingrese la y del punto\n");
	scanf("%d", &b);
	puc.x = a;
	puc.y = b;
	system("clear");
	return puc;
}

void cargaAB(){ //carga de la formula
	printf("Ingrese el A de la formula (y=ax+b)\n");
	scanf("%d", &A);
	printf("Ingrese el B de la formula (y=ax+b)\n");
	scanf("%d", &B);
	system("clear");

}
int mayor(int a, int b){ //devuelve el mayor entre 2 numeros
	if(a>=b){
		return a;
	}
	else{
		return b;
	}
}

int menor(int a, int b){ //devuelve el menor entre 2 numeros
	if(a<=b){
		return a;
	}
	else{
		return b;
	}
}


int Bcuadra(Punto rA, Punto rB, Punto pu){ //boleano
	Punto auxM, auxm;

	//ordena los puntos del rectangulo
	auxM.x = mayor(rA.x, rB.x);
	auxM.y = mayor(rA.y, rB.y);
	auxm.x = menor(rA.x, rB.x);
	auxm.y = menor(rA.y, rB.y);

	return (((pu.x>= auxm.x) && (pu.x <= auxM.x)) && ((pu.y >= auxm.y) && (pu.y <= auxM.y))); //devuelve si el punto esta dentro del rectangulo
}

void muestraManz(int a[n][m]){ //muestra el estado actual de la manzana
	int i, k;
	for (i = 0; i < n ; ++i){
		for (k = 0; k < m; ++k){
			printf("%d |", a[i][k]);	
		}
		printf("\n");
	}
	
}

void cargaManz(){ //permite cargar la manzana
	int aux, h, u;
	for(h = 0; h < n ; h++){
		for(u = 0; u < m ; u++){
			system("clear");
			muestraManz(manz);
			printf("\n");
			printf("Ingrese el numero a cargar en la manzana \n");
			scanf("%d", &aux);
			manz[h][u] = aux;
		}
	}
}

int Bmanz(int a[n][m]){ //suma la corteza y el corazon de la manzana. Devuelve si es igual o no.
	int cor, ext, i, k;

	for (i = 0; i < n; ++i){
		for (k = 0; k < m; ++k)		{
			if ((i == 0) || (k == 0) || (i == (n-1)) || (k == (m-1))){
				cor += a[i][k];
			}
			else{
				ext += a[i][k];
			}
		}
	}
	return (cor == ext);

}

void mostrarSopa(char a[nc][mc]){ //permite visualizar la sopa.
	int j, k;
	for (j = 0; j < nc; ++j){
		for (k = 0; k < mc; ++k){
			printf(" %c", a[j][k]);
		}
		printf("\n");
	}
}

void cargaSopa(){ //carga la sopa con letras al azar
	char randomLetra;
	int a, b;

	for (a = 0; a < nc; ++a){
		for (b = 0; b < mc; ++b){
			randomLetra = 'a' + (random() % 26);
			sopa[a][b] = randomLetra;
		}
	}
}

int buscadorHorizontal(char pa[pl], int a, int b){ //busca horizontalmente en la sopa desde la posicion que le pasen
	int bol, g;
	bol = 1;
	for (g = 0; g < (strlen(pa)); ++g)	{
		if (pa[g] != sopa[a][b+g]){
			bol = 0;
		}
	}
	return bol;
}

int buscadorVertical(char pa[pl], int a, int b){ //busca verticalmente en la sopa desde la posicion que le pasen
	int bol, g;
	bol = 1;
	for (g = 0; g < (strlen(pa)); ++g)	{
		if (pa[g] != sopa[a+g][b]){
			bol = 0;
		}
	}
	return bol;
}

int encontrarPalabra(){ //recorre toda la sopa en busca de la palabra
	char pala[pl];
	int y, t, b;
	b = 1;

	printf("Ingrese la palabra a buscar (en minusculas)\n");
	scanf("%s", &pala);

	for (y = 0; y < nc; ++y){
		for (t = 0; t < mc; ++t){
			if (pala[0] == sopa[y][t]){
				if (buscadorHorizontal(pala, y, t)){
					printf("La palabra aparece desde [%d][%d] hasta [%d][%d]\n", y, t, y, (t+strlen(pala)));
				}
				if (buscadorVertical(pala, y, t)){
					printf("La palabra aparece desde [%d][%d] hasta [%d][%d]\n", y, t, y, (t+strlen(pala)));
				}
			}
		}
	}

	return 0;
}

void nueve(){ //ejercicio nueve
	cargaPos(punt);
	cargaAB();
	if (pertenece(A, B, punt)){
		printf("Esta adentro \n");
	}
	else{
		printf("No esta adentro\n");
	}
}

void diezA(){ // EJ. 10 a
	printf("Ingrese los datos del punto \n\n");
	punt = cargaPos();
	printf("Ingrese los datos del circulo \n\n");
	cc = cargaPos();
	printf("Ingres el radio del circulo\n");
	scanf("%d", &rc);
	system("clear");

	if(Batroden(punt, cc, rc)){ //Batroden llamada desde la libreria
		printf("Esta adentro\n");
	}
	else{
		printf("Esta afuera\n");
	}
}

void diezB(){ //ejercicio 10 b.
	printf("Ingrese los datos del primer punto \n\n");
	punt = cargaPos();
	printf("Ingrese los datos del segundo punto \n\n");
	cc = cargaPos();
	system("clear");

	printf("La distancia es de %d\n", distan(punt, cc)); //distan llamada desde la libreria
}

void once(){ //ejercicio n°11
	printf("Ingrese la posicion del primer punto del rectangulo\n");
	recA = cargaPos();
	printf("Ingrese la posicion del segundo punto del rectángulo\n");
	recB = cargaPos();
	printf("Ingrese la posicion del centro del circulo\n");
	cc = cargaPos();
	printf("Ingrese el radio del circulo\n");
	scanf("%d", &rc);
	system("clear");
	printf("Ingrese la posicion del punto\n");
	punt = cargaPos();

	Bcir = Batroden(punt, cc, rc);
	Brec = Bcuadra(recA, recB, punt);

	switch(Bcir){
		case 1 :
			if (Brec){
				printf("El Punto es interior al círculo y al rectángulo\n");
			}
			else{
				printf("El Punto es interior al círculo\n");
			}
		break;

		case 0 :
			if(Brec){
				printf("El Punto es interior al rectángulo\n");
			}
			else{
				printf("El Punto es exterior al círculo y al rectángulo\n");
			}
		break;
	}
}

void doce(){ //ejercicio n°12
	cargaManz();
	system("clear");
	muestraManz(manz);
	if (Bmanz(manz)){
		printf("Suman lo mismo\n");
	}
	else{
		printf("No suman lo mismo\n");
	}
}

void trece(){ //ejercicio n° 13
	cargaSopa();
	mostrarSopa(sopa);
	encontrarPalabra();
}


int main(){
	system("clear");
	do{
		printf("\n\n1. [Ej 9] Saber si un punto esta en una recta. \n");
		printf("2. [Ej 10 a] Saber si un punto esta dentro del radio de un circulo\n");
		printf("3. [Ej 10 b] Distancia entre 2 puntos\n");
		printf("4. [Ej 11] Saber si un punto esta dentro de un rectangulo y de un circulo\n");
		printf("5. [Ej 12] Saber si los limites suman lo mismo que el centro\n");
		printf("6. [Ej 13] Sopa de letras \n\n");
		printf("0. Cierra el programa\n");
		printf("Ingrese el numero de la opcion a realizar\n");
		scanf("%d", &opcion);
		system("clear");

		switch(opcion){
			case 1 : nueve();
				break;
			case 2 : diezA();
				break;
			case 3 : diezB();
				break;
			case 4 : once();
				break;
			case 5 : doce();
				break;
			case 6 : trece();
				break;
		}
	}while(opcion != 0);

	return 0;
}