//Asignaci�n de memoria din�mica. 
//Ejemeplo: Agenda de Personas (agenda.c).

#include <stdio.h> 

 struct dato { 
        char nombre[20]; 
        char telefono[12]; 
        }; 
 
 struct _agenda { 
        struct dato info; 
        struct _agenda *siguiente; 
        }; 
 

 struct _agenda *primero, *ultimo; 
 
 void mostrar_menu() { 
      printf("\n\nMen�:\n=====\n\n"); 
      printf("1.- A�adir elemento al final\n"); 
      printf("2.- A�adir elemento al inicio\n");       
      printf("3.- A�adir elemento en la n-esima posici�n\n");       
      printf("4.- Borrar elemento del inicio\n"); 
      printf("5.- Borrar elemento del final\n");
      printf("6.- Mostrar lista\n"); 
      printf("7.- Salir\n\n"); 
      printf("Escoge una opci�n: ");fflush(stdout); 
 } 
 
 
 
 /* Con esta funci�n a�adimos un elemento al final de la lista */ 
 void anadir_elemento_final() { 
      struct _agenda *nuevo; 
 
      /* reservamos memoria para el nuevo elemento */ 
      nuevo = (struct _agenda *) malloc (sizeof(struct _agenda)); 
      if (nuevo==NULL) printf( "No hay memoria disponible!\n"); 
	       
  	  char aux[1]; 
      gets(aux); 
     
      printf("Nombre: "); fflush(stdout); 
      gets((nuevo->info).nombre); 
      printf("Tel�fono: "); fflush(stdout); 
      gets((nuevo->info).telefono); 
 
      /* el campo siguiente va a ser NULL por ser el �ltimo elemento 
         de la lista */ 
      nuevo->siguiente = NULL; 
 
      /* ahora metemos el nuevo elemento en la lista. lo situamos 
         al final de la lista */ 
      /* comprobamos si la lista est� vac�a. si primero==NULL es que no 
         hay ning�n elemento en la lista. tambi�n vale ultimo==NULL */ 
      if (primero==NULL) { 
         printf( "Primer elemento\n"); 
         primero = nuevo; 
         ultimo = nuevo; 
         } 
      else { 
           /* el que hasta ahora era el �ltimo tiene que apuntar al nuevo */ 
           ultimo->siguiente = nuevo; 
           /* hacemos que el nuevo sea ahora el �ltimo */ 
           ultimo = nuevo; 
      } 
 } 
 
 void mostrar_lista() { 
      struct _agenda *auxiliar; /* lo usamos para recorrer la lista */ 
      int i; 
 
      i=0; 
      auxiliar = primero; 
      printf("\nMostrando la lista completa:\n"); 
      while (auxiliar!=NULL) { 
            printf( "Nombre: %s y Telefono: %s\n", (auxiliar->info).nombre,(auxiliar->info).telefono); 
            auxiliar = auxiliar->siguiente; 
            i++; 
      } 
      if (i==0) printf( "\nLa lista est� vac�a!!\n" ); 
 } 

 //inserta primero 
void inserta_primero (){
  struct _agenda *aux;

  aux = (struct _agenda *) malloc (sizeof(struct _agenda));
  char auxi[2];
  gets(auxi);
  printf("Nombre: "); 
  gets((aux->info).nombre); fflush(stdout);
  printf("Tel�fono: "); 
  gets((aux->info).telefono); fflush(stdout);

  aux->siguiente = primero;
  primero = aux;
  if (ultimo == NULL){
    ultimo = primero;
  }
}

 void suprimir_primero() {
  struct _agenda *aux;
  if (primero == NULL){
    printf("Esta vacio :D\n");
  }
  else{
    aux = primero;
    primero = primero->siguiente;
    free(aux);
  }
 }
 void suprimir_final() {
  struct _agenda *aux;
  if (ultimo == NULL){
    printf("Esta vacio :D\n");
  }
  else{
    aux = primero;
    while(aux->siguiente != ultimo){
      aux = aux->siguiente;
    }
    free(ultimo);
    ultimo = aux;
  }
 }

 void salir(){
  while(primero != NULL){
    suprimir_primero();
  }
  exit(0);
 } 
 
 int main() { 
     int opcion; 
 
     primero = (struct _agenda *) NULL; 
     ultimo = (struct _agenda *) NULL; 
     do { 
         mostrar_menu(); 
	scanf("%d",&opcion); 
         switch ( opcion ) { 
                case 1: anadir_elemento_final(); 
                       break; 
                case 2:  inserta_primero(); 
                        break; 
                case 3:  printf("No disponible 3 todav�a!\n"); 
                        break; 
                case 4: suprimir_primero(); 
                        break; 
                case 5:  printf("No disponible 5 todav�a!\n"); 
                        break; 
                case 6: mostrar_lista(); 
                        break; 
                case 7: salir(); 
                default: printf( "Opci�n no v�lida\n" ); 
                         break; 
             } 
     } while (opcion!='7'); 
 }
