// Nicolas Dalessandro. Com 1. Probado en debian con gcc
#include <stdio.h>
#include <stdlib.h> //para limpiar consola en linux solo
#define MAX 10
#define pal 18

int iNumero, iNumero2, iNumero3;
int iArreglo[MAX];
int iOpcion; //para el menu


int serImpar(int a){ //devuelve si un numero es impar
	return (a%2);
}

int cargarNumero(){ //cargar un numero
	int x;
	printf("Ingrese numero\n\n");
	scanf("%d", &x);
	return x;
}

void parin(){ //informa si un numero es par o no
	iNumero = cargarNumero();
	if (serImpar(iNumero)){
		printf("El numero es impar\n");
	}
	else{
		printf("El numero es par\n");
	}
}

int masGrande(int i, int o){
	return (((i+o)+abs(i-o))/2);
} //devuelve el numero mas grande entre 2

int masChico(int i, int o){ //devuelve el numero mas chico entre 2
	if(i<o){
		return i;
	}
	else{
		return o;
	}
}

int masMedio(int a, int b, int c){ //devuelve el numero del medio
	if (((a<b) && (b<c)) || ((c<b) && (b<a))){
		return b; //caso en que b sea el del medio
	}
	if (((b<a) && (a<c)) || ((c<a) && (a<b)))
	{
		return a; //caso que a es el del medio
	}
	else{
		return c; //caso que resta
	}
}
void grandezaDe3(){
	iNumero = cargarNumero();
	iNumero2 = cargarNumero();
	iNumero3 = cargarNumero();
	printf("El numero mas chico es %d \n", masChico(masChico(iNumero, iNumero2),iNumero3));
	printf("El numero del medio es %d \n", masMedio(iNumero, iNumero2, iNumero3));
	printf("El numero mas grande es %d \n", masGrande(masGrande(iNumero, iNumero2),iNumero3));
}

void ordena2(){ //informa si los numero estan ordenados
	iNumero = cargarNumero();
	iNumero2 = cargarNumero();
	iNumero3 = cargarNumero();
	if(((iNumero <= iNumero2) && (iNumero2 <= iNumero3)) || ((iNumero >= iNumero2) && (iNumero2 >= iNumero3))){
		printf("Los numeros estan ordenados ^_^\n");
	}
	else{
		printf("Los numeros no estan ordenados -_-\n");
	}
}

void cargaArreglo(){ //carga un arreglo de numeros
	int i = 0;
	system("clear");
	while (i < MAX){
		printf("Ingrese numero a guardar en el espacio %d \n", (i+1));
		scanf("%d", &iArreglo[i]);
		system("clear");
		i++;
	}
	printf("Los numeros cargados fueron: ");
	i= 0;
	while(i < MAX){
		printf("%d ", iArreglo[i]);
		i++;
	}
	printf("\n");
}

void sigArreglo(){ //muestra el siguiente del arreglo
	int i;
	for (i = 0; i < MAX; ++i){
		iArreglo[i]++;
	}
	printf("Los numeros que les siguen son: ");
	for (i = 0; i < MAX; ++i){
		printf("%d ", iArreglo[i]);
	}
	printf("\n");
}

void arregloLetras(){
	int i;
	char cPalabra[pal] = "ertwnoeblreicsxde";
	printf("La palabra cargada en el arreglo es: %s \n", &cPalabra);
	printf("La palabra leida al revez con for y paso -2 es \n");
	for (i = pal-1; i > 0; --i){
		--i;
		printf("%c", cPalabra[i]);
	}
	printf("\n");
	printf("La palabra leida al revez con do y paso -2 es \n");
	i= pal-1;
	do{
		--i;
		printf("%c", cPalabra[i]);
		--i;
	}while(i>=0);
	printf("\n");
}

void menu(){
	printf("\n1. ingrese un numero y averigue si es par\n");
	printf("2. Ingrese 3 numeros y averigue cual es el mayor\n");
	printf("3. Ingrese 3 numeros y averigue si estan ordenados\n");
	printf("4. Ingrese 10 numeros y le mostramos el siguiente\n");
	printf("5. Vea como quedo el arreglo de letras\n");
	printf("0. Cierra el programa\n\n");
	printf("Elija el numero de la opcion \n");
	scanf("%d", &iOpcion);
	system("clear");
	switch (iOpcion){
		case 1 :
			parin();
			break;
		case 2 :
			grandezaDe3();
			break;
		case 3 :
			ordena2();
			break;
		case 4 :
			cargaArreglo();
			sigArreglo();
			break;
		case 5 :
			arregloLetras();
			break;
	}
}

int main(){
	system("clear");
	do{
		menu();
	}while(iOpcion != 0);
	system("clear");
	return 0;
}