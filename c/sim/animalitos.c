#include <stdio.h>
#include <stdlib.h>

#define r 3
#define a 2
#define b 1
#define m 2
#define MIN 0
#define MAX 100

#define h 0.01

//derivada de p
double dp(double d, double p){
	return (r*p) - (a*p*d);
}

//derivada de d
double dd(double d, double p){
	return (b*p*d) - (m*d);
}


double euler(double he, double f, double derivada){
	return f + he*derivada;
}

void graficar(double x[], double p[], double d[], int NUM_PUNTOS){
    int i = 0;
	//guardar en archivos
	FILE * archivoPresas = fopen("presas.txt", "w");
    for (i=0;i<NUM_PUNTOS;i++){
  		fprintf(archivoPresas, "%lf %lf %lf \n", x[i], p[i], d[i]);
	}

	//Parte grafica 1
    //comandos
    char * configGnuplot1[] = { "set ylabel \"----animales--->\"",
                                "set xlabel \"----tiempo de ejecucion--->\"",
                                "plot \"presas.txt\" using 2 title 'Presas' with lines, \
                                 \"presas.txt\" using 3 title 'Depredadores' with lines",
                                
                               };
    FILE * ventanaGnuplot = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one
	for (i=0;i<3;i++){
		fprintf(ventanaGnuplot, "%s \n", configGnuplot1[i]);
	}

	//Parte grafica 2
	//comandos
    char * configGnuplot2[] = { "set ylabel \"----depredadores--->\"",
                                "set xlabel \"----presas--->\"",
                                "plot \"presas.txt\" using 2:3 title 'Depredadores' with lines",
                                
                               };
    FILE * ventanaGnuplot2 = popen ("gnuplot -persist", "w");
    // Executing gnuplot commands one by one
	for (i=0;i<3;i++){
		fprintf(ventanaGnuplot2, "%s \n", configGnuplot2[i]);
	}
	
	fclose(archivoPresas);	
}


int main(int argc, char const *argv[])
{
	int NUM_PUNTOS = (int)(MAX-MIN)/h;
	double antp = 2;
	double antpaux = 2;
	double antd = 2;
	double antdaux = 2;
	double t = MIN+h;
	
	
    // X, Y valores de los puntos a graficar
    double valoresX[NUM_PUNTOS];
    double presasY[NUM_PUNTOS];
    double depredadoresY[NUM_PUNTOS];

    //init
    int i=0;
    for (int i = 0; i < NUM_PUNTOS; ++i){
    	valoresX[i] = 0;
    	presasY[i] = 0;
    	depredadoresY[i] = 0;
    }

    i = 0;
    //euler
	while(t < MAX){

		antpaux = euler(h, antp, dp(antd,antp));
		antdaux = euler(h, antd, dd(antd,antp));
		antp = antpaux;
		antd = antdaux;
		t = t+h;

		//saveit
		valoresX[i] = t;
		presasY[i] = antp;
		depredadoresY[i] = antd;
		i = i+1;
	}
	graficar(valoresX,presasY,depredadoresY, NUM_PUNTOS);
	return 0;
}