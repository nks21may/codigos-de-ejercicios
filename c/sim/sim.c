#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#define NELEMENS(x) (sizeof(x)/ sizeof(x[0]))
#define N 6
#define MAXSERVER 6

struct Evento{
	bool in;
	float time;
	float service;
	int id;
	float cola;
	bool inCola;
	bool on;
}eventos[N];

float clock;
float clockAnterior;
float ocupados;
int cola;
float q;
float b;

void inic(float inter[], float service[]){
	clock = 0;
	clockAnterior = 0;
	cola = 0;
	ocupados = 0;
	q = 0;
	b = 0;
	float acum = 0;
	for (int i = 0; i < N; ++i){
		eventos[i].in = true;
		eventos[i].on = true;
		eventos[i].id = i;
		eventos[i].cola = 0;
		eventos[i].inCola = false;
		eventos[i].time = inter[i]+acum;
		acum = acum+inter[i];
		eventos[i].service = service[i];
	}
}

struct Evento getNext(){
	struct Evento e;
	e.on = false;
	for (int i = 0; i < NELEMENS(eventos); ++i){
		if (!e.on){
			e = eventos[i];
		}else{
			if (e.time > eventos[i].time && eventos[i].on){
				e = eventos[i];
			}
		}
	}
	return e;
}

void be(){
	b = ((clock - clockAnterior)*ocupados)+b;
}

void qe(){
	q = ((clock - clockAnterior)*cola)+q;
}

void arrib(struct Evento e){
	clockAnterior = clock;
	clock = e.time;
	be();
	qe();
	if (ocupados < MAXSERVER){
		ocupados = ocupados +1;
		eventos[e.id].in = false;
		eventos[e.id].time = clock + e.service;
	}else{
		eventos[e.id].inCola = true;
		eventos[e.id].cola = clock;
	}

	
}


void salid(struct Evento e){
	clockAnterior = clock;
	clock = e.time;
	be();
	qe()
	ocupados = ocupados -1;
	eventos[e.id].on = false;

	struct Evento o;
	o.cola = -1;
	for (int i = 0; i < NELEMENS(eventos); ++i)
	{
		if (eventos[i].inCola){
			if (!o.inCola){
				o = eventos[i];
			}
			if (eventos[i].cola < o.cola)
			{
				o = eventos[i];
			}
		}
	}
	if(o.cola != -1)
		arrib(o);
}

int eventLeft(){
	int k = 0;
	for (int i = 0; i < NELEMENS(eventos); ++i){
		if (eventos[i].on)
		{
			k = k+1;
		}
	}
	return k;
}

void f(int inter[], int service[]){
	printf("F********\n");
	if (NELEMENS(inter) != NELEMENS(service)){
		return;
	}
	inic(inter, service);
	struct Evento e;
	int i = 0;
	while(0 < eventLeft()){
		printf("clock: %.2f, b: %.2f, CA: %.2f \n",clock,b, clockAnterior);
		e = getNext();
		if (e.in){
			arrib(e);
		}else{
			salid(e);
		}
	}
}

int main(){
	float ar[N] = {1,1,2,1,2,1.5};
	float ser[N] = {2.5,3,2.3,1.8,1,4};
	f(ar,ser);
	printf("*********\n");
	printf("%.2f\n", clock);
	printf("%.2f\n", b);
	return 0;
}

float[] probavilityTable(float time[], float prob[]){
	if (NELEMENS(time) != NELEMENS(prob)){
		return;
	}
	if (NELEMENS(time) == 1){
		return {1f};
	}

	float ac[NELEMENS(prob)];
	int k = 0;
	bool fin = true;
	for (int i = 1; i < count && fin; ++i){
		ac[i] = ac[i-1] + prob[i];
		ac[i] = ac[i]*10.0f;
		ac[i] = (ac[i] > (floor(ac[i])+0.5f)) ? ceil(ac[i]) : floor(ac[i]);
		ac[i] = ac[i]/10.0f;
	}
	return ac;
}

float getValue(float f, float ac[], float time[]){
	float c;
	int i = 0;
	while(f > ac[i] && i < NELEMENS(ac)){
		i = i+1;
	}
	if (i>0){
		c = time[i];
	}
	return c;
}



void printEvent(struct Evento e){
	printf("------------------\n");
	printf(e.in ? "in true\n" : "in false\n");
	printf("Time: %.2f\n", e.time);
	printf("Service: %.2f\n", e.service);
	printf("id: %d\n", e.id);
	printf(e.on ? "on true\n" : "on false\n");
	printf("------------------\n");
}