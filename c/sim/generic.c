#include <stdio.h>

double calculeEuler(double ant, double h, double der){
	return ant + h*der;
}

void euler(double h, double f0, double min, double max, double (*fd)(double)){
	double ant = f0;
	double t = min;
	while(t <= max){
		ant = calculeEuler(ant, h, fd(t));
		//save ant
		t = t+h;
	}
}

int main(int argc, char const *argv[])
{
	return 0;
}