module Hola where
import Data.Char

fsthalf :: [a] -> [a]
fsthalf xs = take (length xs `div` 2) xs

sndhalf :: [a] -> [a]
sndhalf xs = drop (length xs `div` 2) xs

merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) 
         | (x <= y)  = x:(merge xs (y:ys)) 
         | otherwise = y:(merge (x:xs) ys)

mergesort :: Ord a => [a] -> [a]
mergesort [] = []
mergesort [x] = [x]
mergesort xs = merge (mergesort (fsthalf xs)) (mergesort (sndhalf xs))



--------------------

mayor:: (Int,Int) -> Int
mayor (x,y) = if x >= y then x else y

minimo:: Int -> Int -> Int
minimo x y 
 | x <= y = x
 | otherwise = y 

-- (\x y-> x+y) 3 2
--
-- x y z = a + b
--	where (a = f x y ;
--	  b = g z)
--    
type Nombre = String
type Edad = Int
type Presona = (Nombre, Edad)

data Persona = Pers Nombre Edad
juan::Persona
juan = Pers "Juan Lopez" 23

tocayo:: Presona -> Presona -> Bool
tocayo (n,_) (no,_) = n == no

-- /= es el contrario de == 

square:: Int -> Int
square n = n*n

square2:: Int -> Int
square2 0 = 0
square2 n = foldr (+) 0 []

subsecuencia:: [a] -> [[a]]
subsecuencia [] = []
subsecuencia (x:[]) = [[x]]
subsecuencia (x:xs) = subsecuencia xs ++ [x:p | p <- subsecuencia xs]

dietaok:: [Int] -> Bool
dietaok xs = sum xs == 0

work:: [Int] -> [[Int]]
work xs = filter (dietaok) (subsecuencia xs)

r:: Int -> [Int] -> [[Int]]
r 10 _ = []
r n xs = if(existe n xs) then r (n+1) xs else 
 ((n:xs): r (n+1) xs)

existe::(Eq a) => a -> [a] -> Bool
existe x [] = False
existe y (x:xs)= x==y || existe y xs

perme :: [Int] -> [[Int]]
perme [] = [[]]
perme (x:xs) = concat (map (r 0) (perme xs))

existen::(Eq a) => [a] -> [a] -> Int
existen [] _ = 0
existen (x:xs) (y:ys) = if x == y then 1+existen xs ys else existen xs ys

repetidos::(Eq a) => [[a]] -> Bool
repetidos [] = False
repetidos (x:xs) = 1 <= (length (filter (x==) xs)) || repetidos xs

nexis:: Int -> [Int] -> [Int] -> Bool
nexis x y z = x == existen y z

ejercicio:: [Int] -> Int -> [[Int]]
ejercicio xs y = filter (nexis y xs) (perme xs)

listapart:: [a] -> Int -> [[a]]
listapart [] _ = []
listapart xs n = take n xs : (listapart (drop n xs) n)

concatFts:: a -> [[a]] -> [[a]]
concatFts x ys = [x: (head ys)] ++ drop 1 ys

listam:: [Int] -> [[Int]]
listam [] = []
listam (x:[]) = [[x]]
listam (x:y:xs) = if x < y then [x: (listam (y:xs))!!0] ++ drop 1 (listam xs) else 
  [[x]] ++listam (y:xs)

data Tree a = Nil | Node (Tree a) a (Tree a)  deriving (Eq, Show)

resp:: Tree a -> Bool
resp x = profundidad x == 0

profundidad:: Tree a -> Int
profundidad Nil = 1
profundidad (Node(hi) x (hd)) = (profundidad hi - profundidad hd)

sumon:: Tree Int -> Int -> Int -> Bool
sumon Nil _ _ = False
sumon (Node(hi) x (hd)) a k = (a+x) ==k || sumon hi (a+x) k || sumon hd (a+x) k 

laRebancha:: [Char] -> [Char] -> Bool
laRebancha [] []  = True
laRebancha [] s = False
laRebancha s [] = length (filter (isUpper) s) == 0
laRebancha (x:s1) (y:s2)
 | length s1 < length s2 = False
 | x == y = laRebancha s1 s2
 | toUpper x == y =  laRebancha s1 (y:s2) || laRebancha s1 s2 
 | isLower x = laRebancha s1 (y:s2)
 | isUpper x && isLower y = False
 | otherwise = False

reemplazo:: Char -> Char -> Char
reemplazo x y
 | x == 'a' && y == 'b' = 'c'
 | x == 'b' && y == 'a' = 'c'
 | x == 'b' && y == 'c' = 'a'
 | x == 'c' && y == 'b' = 'a'
 | x == 'c' && y == 'a' = 'b'
 | x == 'a' && y == 'c' = 'b'

abcdario:: [Char] -> [Char]
abcdario [] = []
abcdario (x:[]) = [x]
abcdario (x:y:xs) 
 | (x /= y) = reemplazo x y : abcdario xs
 | otherwise =  x: (abcdario (y:xs))

fabc:: [Char] -> [Char]
fabc [] = []
fabc [x] = [x]
fabc xs 
 | length xs > length (abcdario xs) = fabc (abcdario xs)
 | otherwise = abcdario xs

minCad:: [Char] -> [Char] -> [Char]
minCad x y = if (length x < length y) then x else y

perm :: [a] -> [[a]]
perm [] = [[]]
perm (x:xs) = concat (map (interleave x) (perm xs))

interleave :: a -> [a] -> [[a]]
interleave x [] = [[x]]
interleave x (y:ys) = (x:(y:ys)):(map (y:) (interleave x ys))

finalAbs:: [Char] -> [Char]
finalAbs [] = [] 
finalAbs (x:[]) = [x] 
finalAbs (xs) = if (xs == menor (head (map (abcdario) (perm xs))) (map (abcdario) (perm xs))) then
 xs
 else finalAbs (menor (head (map (abcdario) (perm xs))) (map (abcdario) (perm xs)))

menor:: [Char] -> [[Char]] -> [Char]
menor x [] = x
menor x (y:ys) = if length x < length y then menor x ys
 else menor y ys

ilegal:: [Int] -> Int
ilegal xs = result (mergesort xs) 1

result:: [Int] -> Int -> Int
result [] x = 1
result [x] y 
 | x == y = y+1
 | x < y = y
 | otherwise = x+1
result (x:xs) y
 | x == y = result xs (y+1)
 | x < 1 = result xs y
 -- | x != y = result xs y
 | otherwise = y

pertenece::(Ord a) => [a] -> a -> Bool
pertenece [] _ = False
pertenece (x:xs) y = x==y || pertenece xs y

vv::[Int] -> Int -> Int
vv [] _ = 1
vv xs y 
 | pertenece xs y = vv xs (y+1)
 | otherwise = y

vvv:: [Int] -> Int
vvv xs = vv xs 1


