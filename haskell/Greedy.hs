module Greedy where
import Hola

-- 6.2
sume:: Int -> [Int] -> Bool
sume e n = sum n == e

factible:: Int -> Int -> [Int] -> Bool
factible n x s = sum s + x <= n

minlen:: [a] -> [a] -> [a]
minlen [] y = y 
minlen x [] = x 
minlen x y = if (length x <= length y) then x else y

filtrom:: [(Int,Int)] -> [(Int,Int)]
filtrom [] = []
filtrom (x:xs) = if snd x == 0 then filtrom xs else x: filtrom xs

sacamoneda:: [(Int,Int)] -> [(Int,Int)]
sacamoneda [] = []
sacamoneda (x:xs) = filtrom ((fst x, snd x - 1) :xs) 

-- Supongo que el conjunto de monedas me lo pasan ordenado y s vacio
monedas:: [(Int,Int)] -> Int -> [Int] -> [Int]
monedas [] _ _ = []
monedas (x:c) n s 
 | sume n s = s
 | factible n (fst x) s = minlen (monedas (sacamoneda (x:c)) n ((fst x):s)) (monedas (c) n (s))
 | (factible n (fst x) s == False) = monedas c n s

ejercicio62:: [(Int,Int)] -> Int -> [Int]
ejercicio62 xs x = monedas xs x []

-- 6.4
data Cita = Cita [Char] Int Int Int deriving (Show)
-- (des,p,inico,fin)

instance Eq Cita where
    (Cita d p i f) == (Cita d2 p2 i2 f2) = d == d2 && p == p2 && i == i2 && f == f2

instance Ord Cita where
 compare (Cita _ p i f) (Cita _ p2 i2 f2) = if (p == p2) then compare (f-i) (f2-i2) 
  else compare p p2

superposicion:: Cita -> Cita -> Bool
superposicion (Cita _ _ i1 f1) (Cita _ _ i2 f2) = (i1 < i2 && f1 > i2) || (i1 < f2 && f1 > f2) || (i1 < i2 && f1 > f2) || (i1 > i2 && f1 < f2)

superposicionGral:: Cita -> [Cita] -> Bool
superposicionGral x [] = False
superposicionGral x (y:ys) = superposicion x y || superposicionGral x ys


agenda:: [Cita] -> [Cita] -> [Cita]
agenda [] y = reverse y
agenda (x:xs) y 
 | superposicionGral x y = agenda xs y
 | otherwise = agenda xs (x:y) 

ejercicio64:: [Cita] -> [Cita]
ejercicio64 x = agenda (reverse (mergesort x)) []

