import Data.Char
--Ejemplos
qsort :: (Ord a) => [a] -> [a]
qsort [] = []
qsort (x:xs) = qsort left ++ [x] ++ qsort right where
	left = [a | a <- xs, a <= x]
	right = [b | b <- xs, b>x]

type Par a = (a,a)

data Figura = Circulo Float | Rect Float Float

area:: Figura -> Float
area (Circulo r) = pi*r^2
area (Rect b a) = b*a

--Ejemplos
--clase 1
factorial::Int->Int
factorial x = if x == 0
	then 1
	else  x*factorial(x-1)

--clase 2
eSuma::[Int]->Int
eSuma [] = 0
eSuma (x:xs) = x + eSuma xs

concatenar:: [a] -> [a] -> [a] 
concatenar xs [] = xs
concatenar [] ys = ys
concatenar (x:xs) (y:ys) = (x: concatenar xs (y:ys))

tomar:: Int -> [a] -> [a]
tomar n [] = []
tomar 0 cd = []
tomar n (s:cd) = (s: tomar (n-1) cd)

tirar:: Int -> [a] -> [a]
tirar 0 cd = cd
tirar n (s:cd) = tirar (n-1) cd
--tarea

avs:: Int -> Int
avs x = if x >= 0
	then x
	else (-x)

--edad:: (Int,Int,Int) -> (Int,Int,Int) -> Int 

hd:: [a] -> a
hd (x:xs) = x

tl:: [a] -> [a]
tl (x:xs) = xs

lost:: (Eq a) => [a] -> a
lost (x:xs) = if xs == [] 
	then x
	else lost xs

initi:: [a] -> [a]
initi (s:sx) = (s: initi sx)	

divis:: Int -> [Int]
divis x = [y | y <- [1..x], mod x y == 0]

primo:: Int -> Bool
primo x = [y | y <- [1..x], mod x y == 0] == [1,x]

primoList:: Int -> [Int] --2.7
primoList x = [y | y <- [1..x], primo y == True]

--succ:: Int -> Int
--succ x = x + 1

isEven:: Int -> Bool
isEven x = mod x 2 == 0

--Naturales

data Nat = Zero|Succ Nat deriving (Ord)

instance Eq Nat where
	Zero == Zero = True
	Zero == Succ n = False
	Succ n == Zero = False
	Succ n == Succ m = n == m

instance Enum Nat where
	succ Zero = Succ Zero 
	succ n = Succ n
	pred Zero = Zero
	pred n =  intToNat(pred (natToInt n))

instance Show Nat where 
	show m = show(natToInt m)
instance Num Nat where 
	Zero + Zero = Zero
	Zero + Succ(o) = Succ(o) 
	Succ(p) + Zero = Succ(p) 
	Succ(n) + Succ(m) = intToNat(natToInt(n) + natToInt(m))
	Zero * Zero = Zero
	Zero * Succ(n) = Zero
	Succ(n) * Zero = Zero
	Succ(n) * Succ(m) = intToNat(natToInt(n) * natToInt(m))
	abs(Zero) = Zero
	abs(Succ(n)) = Succ(n)
	fromInt n = intToNat(n)



intToNat:: Int -> Nat
intToNat 0 = Zero
intToNat n = Succ(intToNat(n-1))

natToInt:: Nat -> Int
natToInt Zero = 0
natToInt (Succ n) = 1+ natToInt(n)

natSuma:: Nat -> Nat -> Nat
natSuma Zero n = n
natSuma n Zero = n
natSuma (Succ n) (Succ m) = Succ(Succ(natSuma n m))

--Fnaturales
lSucc:: [Int] -> [Int]
lSucc [] = []
lSucc (x:xs) = succ x : lSucc xs 

sqr:: Int -> Int
sqr x = x*x

lCuad:: [Int] -> [Int]
lCuad [] = []
lCuad (x:xs) =  sqr x : lCuad xs 

--5.14
parOcho:: [Int] -> [Int]
parOcho xs = [x | x <-xs, isEven x && x > 8 ]

--5.17
prodCart:: [Int] -> [Int] -> [(Int,Int)]
prodCart xs xd = [(a,b) | a <- xs, b <- xd]

llCuad:: (Int -> Int) -> [Int] -> [Int]
llCuad f [] = []
llCuad f (x:xs) = f x : llCuad f xs

--7

nand:: Bool -> Bool -> Bool
nand True True = False
nand _ _ = True

maj:: Bool -> Bool -> Bool -> Bool
maj True True _ = True
maj True _ True = True
maj _ True True = True
maj _ _ _ = False

paar:: Int -> [Int] -> Bool
paar i xs = even (xs!!i)

positivo:: Int -> [Int] -> Bool
positivo i xs = (xs!!i)>0

parete:: [Int] -> Bool
parete xs = todos [0..length xs -1] xs paar

todos:: [Int] -> [a] -> (Int -> [a] -> Bool) -> Bool
todos is xs p = foldl (&&) True ys 
	where ys = [p i xs | i<- is]

to2:: [Int] -> [a] -> (Int -> [a] -> Bool) -> Bool
to2 is xs p = foldl (&&) True ys 
	where ys = [p i xs | i<- is, mod i 2 == 1]

sumatoria:: [Int] -> [Int] -> (Int -> [Int] -> Bool) -> Int
sumatoria is xs p = foldl (+) 0 ys
	where ys = [xs!!i | i <- is, p i xs]

-- ARboles binarios

data Tree a = Nill | Node (Tree a) a (Tree a) deriving Show

elemInArbol::(Eq a) => Tree a -> a -> Bool
elemInArbol Nill _ = False
elemInArbol (Node hi r hd) x = if x == r
	then True
	else elemInArbol hi x || elemInArbol hd x

profArbol:: Tree a -> Int
profArbol Nill = 0
profArbol (Node hi r hd) = 1 + max (profArbol hi) (profArbol hd)

sizeArbol:: Tree a -> Int
sizeArbol Nill = 0
sizeArbol (Node hi r hd) = 1 + sizeArbol hi + sizeArbol hd

insertArbol:: (Ord a) => Tree a -> a -> Tree a
insertArbol Nill e = Node Nill e Nill
insertArbol (Node hi r hd) e | e<= r = Node (insertArbol hi e) r hd
							| e>r = Node hi r (insertArbol hd e)

numPerf:: Int -> Bool
numPerf 0 = True
numPerf n = eSuma (divis n) - n == n

listeta:: Int -> [Int] -> [Int]
listeta _ [] = []
listeta n xs = filter (n==) xs

imp:: Bool -> Bool -> Bool
imp False _ = True
imp True True = True
imp True False = False

--203-211

smn:: Int -> [Int] -> Bool
smn _ [] = False
smn z (x:xs)= (x==z+ eSuma(x:xs)-x) || smn (z+x) xs 

minimo:: [Int] -> Int
minimo (x:[]) = x
minimo (x:(y:xs)) = minimo (min x y : xs)

--mariana:: Int -> Bool
--mariana 0 = False
--mariana x = x=1 || x= x + 1 == 1 || 


lucete::[Int]->Bool
lucete [] = True
lucete xs = xs == reverse xs