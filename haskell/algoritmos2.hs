kg:: [Int] -> Int
kg [] = -1
kg (x:[]) = 0
kg (x:xs) = if ((max x (xs!!(kg xs))) == x) then 0 
 else 1 + kg xs
