evalDC::Eq a=>(a->Bool)->(a -> b)->(a->[a])->([b]->b)->a->b
evalDC isBase base split join x 
 | isBase x = base x
 | otherwise = join (map ( evalDC isBase base split join) (split x ))

fibonacci:: Int -> Int
fibonacci s = evalDC (\x->x<=1) (\x->1) (\x->[x-1,x-2]) (\l->(head l)+(last l)) s

subsec:: [Int] -> [Int] -> Bool
subsec p t = evalDC (subisBase p) (subase p) (splitSub p) (foldl (||) (False)) t

subisBase:: [Int] -> [Int] -> Bool
subisBase p t = (p == t) || ((length p) >= (length t))

subase:: [Int] -> [Int] -> Bool
subase p t = p == t 


splitSub :: [a] ->[a] -> [[a]]
splitSub xs ys = [take mitad ys, drop mitad ys] ++ [y | y<-subSplit (length xs) total]
                    where   mitad = div (length ys) 2
                            tirarPrincipio = (mitad - ((length xs) - 1))
                            tempSec = drop tirarPrincipio ys
                            total = take (mitad - tirarPrincipio + (length xs)) tempSec

subSplit :: Int -> [a] -> [[a]]
subSplit n [] = []
subSplit n (x:xs)   | n <= (length (x:xs)) = (take n (x:xs)) : (subSplit n xs)
                    | otherwise = []

