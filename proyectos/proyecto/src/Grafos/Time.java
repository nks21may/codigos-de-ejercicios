/*Proyecto 2017 | Estructura de Datos y Algoritmos - Algoritmos I
 * Alumnos: - D'alessandro, Nicolas
 * 				D.N.I.: 41.105.833
 * 			- Garcia, Valeria
 * 				D.N.I.: 39.545.287
 */

package Grafos;

public class Time {
	
	//convierte una hora a gmt 0 y a minutos
	public static int conversor(int i, char c, int gmt){
		int res = 0;
		if(c == 'P'){
			if (i/100 == 12){
				i %= 100;
			}
			
			res = 720 + (i/100)*60 + i%100  - (gmt/100*60);
			if(res >= 1440){
				res -= 1440;
			}
			return res;
		}else{
			if (i/100 == 12){
				return 720 - (gmt/100*60);
			}
			res = (i/100)*60 + i%100  - (gmt/100*60);
			if(res < 0){
				res += 1440;
			}
			return res;
		}
	}
	
	//calcula el tiempo entre 2 horas
	public static int tiempoEnViaje(int partida, char apPar, int gmtP, int llegada, char apLle, int gmtL){
		int tiempoL, tiempoP;
		int res = 0;
		tiempoL = conversor(llegada, apLle,gmtL);//convierte la hora a gmt 0 y a minutos
		tiempoP = conversor(partida, apPar, gmtP);
		res = tiempoL - tiempoP;
		if(res<0){
			res += 720;
		}
		if(res<0){
			res += 720;
		}
		return res;
	}
	
	public static String timeTotal(int i){
		return (i/60)+"h "+i%60+"min";
	}
}
