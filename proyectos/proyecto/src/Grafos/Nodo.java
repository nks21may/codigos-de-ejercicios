/*Proyecto 2017 | Estructura de Datos y Algoritmos - Algoritmos I
 * Alumnos: - D'alessandro, Nicolas
 * 				D.N.I.: 41.105.833
 * 			- Garcia, Valeria
 * 				D.N.I.: 39.545.287
 */

package Grafos;

import java.util.LinkedList;

//Clase que representan los vertices del grafo
public class Nodo<N,A>{
	private LinkedList<Arco<N,A>> relaciones; //lista de adyacentes de ese vertice
	private boolean marca; //flag para que que identicar cuando un vertice fue visitado
	private N info; //contiene la informacion del vertice. En el caso del proyecto, contiene la informacion de los aeropuertos
	
//<Constructores>
	public Nodo(){ 
		relaciones = new LinkedList<Arco<N,A>>();
		marca = false;
	}
	
	public Nodo(N inf){
		info = inf;
		relaciones = new LinkedList<Arco<N,A>>();
		marca = false;
	}
	
//<set, get>
	//setea el campo info
	public void setInfo(N e){
		info = e;
	}
	
	//retorna lo que hay en el campo info
	public N getInfo(){
		return info;
	}
	
	//Retorna la Lista de Adyacentes de un vertice
	public LinkedList<Arco<N,A>> getLista(){
		return relaciones;
	}
//</set, get>

	/*contains(Nodo<N,A> b): retorna true si el Nodo b es adyacente
	 * pre: true
	 * pos: retorna true si el Nodo b es adyacente
	 */
	public boolean contains(Nodo<N,A> b){
		Arco<N,A> aux = new Arco<N,A>(b);
		return relaciones.contains(aux);
	}
	
	/*contains(Arco<N,A> b): retorna true si el Nodo b es adyacente
	 * pre: true
	 * pos: retorna true si el Nodo b es adyacente
	 */
	public boolean contains(Arco<N,A> b){
		return relaciones.contains(b);
	}
	
	/*indexOf(Arco<N,A> a): retorna el indice en el que se encuentra de 'a' en la lista de adyacente, si existe. Si no, devuelve -1
	 * pre: true
	 * pos:  retorna el indice en el que se encuentra de 'a' en la lista de adyacente, si existe. Si no, devuelve -1
	 */
	public int indexOf(Arco<N,A> a){
		return relaciones.indexOf(a);
	}
	
	/*remove(Arco<N,A> e): elimina un Nodo e de la lista de adyacente
	 * pre: true
	 * pos: elimina un Nodo e de la lista de adyacente
	 */
	public boolean remove(Arco<N,A> e){
		return relaciones.remove(e);
	}
	
	//borra todas las apariciones del nodo en la lista de adyacentes
	public boolean removeAll(Nodo<N, A> e){
		Arco<N,A> aux = new Arco<N, A>(e);
		boolean bol = false;
		if(relaciones.remove(aux)){
			bol = true;
			while(relaciones.remove(aux));
		}
		
		return bol;
	}
	
	/*remove(Nodo<N,A> e): elimina un Nodo e de la lista de adyacente
	 * pre: true
	 * pos: elimina un Nodo e de la lista de adyacente
	 */
	public boolean remove(Nodo<N,A> e){
		Arco<N,A> aux = new Arco<N,A>(e);
		return relaciones.remove(aux);
	}
	
	/*add(Arco<N,A> e): Crea un arco entre dos nodos
	 * pre: true
	 * pos:  Crea un arco entre dos nodos
	 */
	public boolean add(Arco<N,A> e){
		return relaciones.add(e);
	}
	
	/*add(Arco<N,A>,int peso):  Crea un arco entre dos nodos con un peso en particular
	 * pre: true
	 * pos:Crea un arco entre dos nodos con un peso en particular
	 */
	public boolean add(Arco<N,A> e, int peso){
		e.setCosto(peso);
		return relaciones.add(e);
	}
		
	/*add(Nodo<N,A>):  Crea un arco entre dos nodos
	 * pre: true
	 * pos:Crea un arco entre dos nodos
	 */
	public boolean add(Nodo<N,A> e){
		Arco<N,A> aux = new Arco<N,A>(e);
		return relaciones.add(aux);
	}

	/*add(Nodo<N,A>,int peso):  Crea un arco entre dos nodos con un peso en particular
	 * pre: true
	 * pos:Crea un arco entre dos nodos con un peso en particular
	 */
	public boolean add(Nodo<N,A> e, int peso){
		Arco<N,A> aux = new Arco<N,A>(e, peso);
		return relaciones.add(aux);
	}
	
	/*marcar(): Marca un vertice como visitado
	 * pre: true
	 * pos: Marca un vertice como visitado
	 */
	public void marcar() {
		marca = true;
	}
	
	/*desmarcar(): desarca un vertice que fue visitado
	 * pre: true
	 * pos:desarca un vertice que fue visitado
	 */
	public void desMarcar(){
		marca = false;
	}
	
	/*visit(): retorna el valor de la marca para verificar si el vertice fue visitado
	 * pre: true
	 * pos: retorna el valor de la marca para verificar si el vertice fue visitado
	 */
	public boolean visit(){
		return marca;
	}
	
	//Imprimi el Vertice y sus relaciones (adyacentes)
		public String toStringRelaciones(){
			String aux = "["+info.toString()+"]: ";
			for (Arco<N,A> ar : relaciones){
				aux = aux + ar.toString();
			}
			return aux;
		}

//<Redefinidas>
	//Retorna en un string la informacion mas importante del campo info
	public String toString(){
		return "["+info.toString()+"]";
	}

	public int hashCode(){
		return info.hashCode();
	}
	
	@SuppressWarnings("unchecked")
	//Compara dos Nodo's por el campo Info
	public boolean equals(Object obj) {
		if ((obj instanceof Nodo)){
			return ((Nodo<N,A>) obj).getInfo().equals(this.info);
		}
	    return false;
	}
//</Redefinidas>
}