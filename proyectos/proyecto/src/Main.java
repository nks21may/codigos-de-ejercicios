/*Proyecto 2017 | Estructura de Datos y Algoritmos - Algoritmos I
 * Alumnos: - D'alessandro, Nicolas
 * 				D.N.I.: 41.105.833
 * 			- Garcia, Valeria
 * 				D.N.I.: 39.545.287
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import Grafos.*;
import apiVuelos.*;

public class Main{
	
	public static Grafo<Aeropuerto,Vuelo> g = new Grafo<Aeropuerto,Vuelo>();
	
	public static void setup(){ //inicializador
		ApiVuelos a = new ApiVuelos();
		try {
			a.cargarAeropuertos("aeropuertos.txt");
			a.cargarVuelos("vuelos.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Aeropuerto[] air = a.getAeropuertos();
		Vuelo[] v = a.getVuelos();
		
		//carga el arco
		for(int i = 0; i < air.length; i++){
			g.insertarNodo(air[i]);
		}
		
		for(int i = 0; i < v.length; i++){
			Nodo<Aeropuerto,Vuelo> part = new Nodo<Aeropuerto,Vuelo>(v[i].getaPartida());
			Nodo<Aeropuerto,Vuelo> arri = new Nodo<Aeropuerto,Vuelo>(v[i].getaArribo());
			g.insertarArco(part, arri, Time.tiempoEnViaje(v[i].getHsPartida(),v[i].getPartidaAPm(),v[i].getaPartida().getAjusteHorario(),v[i].getHsArribo(),v[i].getArriboApm(),v[i].getaArribo().getAjusteHorario()),v[i]);
		}
	}
	
	public static void uso(){
		System.out.println("Uso: main <aeropuerto_salida> <aeropuerto_llegada> [aeropuerto de paro]");
	}

	public static void main(String[] args){
		setup(); //inicializa todo
		switch(args.length){
			 case 3:
					Aeropuerto aeropuertFicticio = new Aeropuerto();
					aeropuertFicticio.setCodigo(args[0]);
					int is = g.indexOf(new Nodo<Aeropuerto,Vuelo>(aeropuertFicticio)); //indice donde posiblemente(si existe) se encuentre el aeropuerto de salida
					aeropuertFicticio.setCodigo(args[1]);
					int il = g.indexOf(new Nodo<Aeropuerto,Vuelo>(aeropuertFicticio)); //indice donde posiblemente(si existe) se encuentre el aeropuerto de llegada
			 		aeropuertFicticio.setCodigo(args[2]);
					int ip = g.indexOf(new Nodo<Aeropuerto,Vuelo>(aeropuertFicticio)); //indice donde posiblemente(si existe) se encuentre el aeropuerto que esta de paro
					if(ip != -1){
						g.get(ip).marcar();
						if(!args[0].equalsIgnoreCase(args[2]) && !args[1].equalsIgnoreCase(args[2])){
							g.shortPatch(g.get(is), g.get(il));
							System.out.println("El aeropuerto ["+args[2].toUpperCase()+"] se encuentra de paro*");
						}else{
							if(args[0].equalsIgnoreCase(args[2])){
								System.out.println("El aeropuerto ["+args[2].toUpperCase()+"] se encuentra de paro, defina otro lugar de partida*");
							}else{
								System.out.println("El aeropuerto ["+args[2].toUpperCase()+"] se encuentra de paro, defina otro lugar de llegada*");
								}
						}
					}else{
						System.out.println(">>Aeropuerto de paro no encontrado");
					}
			 break;
			 case 2:
			 		Aeropuerto aeropuertoFicticio = new Aeropuerto();
					aeropuertoFicticio.setCodigo(args[0]);
					int js = g.indexOf(new Nodo<Aeropuerto,Vuelo>(aeropuertoFicticio)); //indice donde posiblemente(si existe) se encuentre el aeropuerto de salida
					
					aeropuertoFicticio.setCodigo(args[1]);
					int jl = g.indexOf(new Nodo<Aeropuerto,Vuelo>(aeropuertoFicticio)); //indice donde posiblemente(si existe) se encuentre el aeropuerto de llegada
		 			g.shortPatch(g.get(js), g.get(jl));
			 	break;
			default:
			 	uso();
		}
	}
}