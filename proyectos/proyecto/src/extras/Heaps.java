/*Proyecto 2017 | Estructura de Datos y Algoritmos - Algoritmos I
 * Alumnos: - D'alessandro, Nicolas
 * 				D.N.I.: 41.105.833
 * 			- Garcia, Valeria
 * 				D.N.I.: 39.545.287
 */

package extras;

import java.util.Arrays;

public class Heaps{
	private Object[] arbol;
	private int[] tier; 
	private int capacidad = 20;
	private int cantElem = 0;

//<Constructores>
	public Heaps(){
		arbol = new Object[capacidad];
		tier = new int[capacidad];
	}
	
	public Heaps(int i){
		capacidad = i+1;
		arbol = new Object[i+1];
	}
//<Constructores>
	
	//retorna el elemento de menor prioridad
	public Object first(){
		return arbol[0];
	}
	
	//expande el size del arreglo
	public void changeSizeArray(){
		if(capacidad-1 == cantElem){
			capacidad = capacidad + capacidad/2;
			arbol = Arrays.copyOf(arbol, capacidad);
			tier = Arrays.copyOf(tier, capacidad);
		}
	}
	
	public boolean isEmpty(){
		return cantElem == 0;
	}
	
	public int padre(int i){
		return (i-1)/2;
	}
	
	public int hijoI(int i){
			return (2*i)+1;
	}
	
	public int hijoD(int i){
		return 2*(i+1);
	}
	
	//intercambia 2 elementos y sus prioridades
	private void swap(int i, int j){
		  Object temp = arbol[i];
	  	  arbol[i] = arbol[j];
		  arbol[j] = temp;
		  
		  int aux = tier[i];
		  tier[i] = tier[j];
		  tier[j] = aux;
	 }//end swap
	
	//ordena el heap
	public void ordening(int i){
		if(i > 0){
			if(tier[padre(i)] > tier[i]){
				swap(padre(i), i);
				ordening(padre(i));
			}
		}
		if(hijoI(i) < cantElem){
			if(arbol[hijoI(i)] != null && tier[hijoI(i)] < tier[i]){
				swap(hijoI(i), i);
				ordening(i);
				ordening(hijoI(i));
			}
		}
		if(hijoD(i) < cantElem){
			if(arbol[hijoD(i)] != null && tier[hijoD(i)] < tier[i]){
				swap(hijoD(i), i);
				ordening(i);
				ordening(hijoD(i));
			}
		}
	}
	
	//retorna la posicion donde se encuenta un elemento en el arco. En caso de no pertenecer, retorna -1.
	public int indexOf(Object e){
		int res = -1;
		for(int i = 0; i < cantElem && res == -1; i++){
			if(e.equals(arbol[i])){
				res = i;
			}
		}
		return res;
	}
	
	//inserta un elemento en el heap y devuelve true si logro insertarlo con exito
	public boolean insertar(Object e, int prio){
		if(isEmpty()){
			arbol[0] = e;
			tier[0] = prio;
			cantElem++;
		}else{
			changeSizeArray();
			boolean res = false;
			int i;
			for(i = 0; i < cantElem && !res; i++){
				res = arbol[i].equals(e);
			}
			
			if(i == cantElem){
				cantElem++;
				arbol[cantElem-1] = e;
				tier[cantElem-1] = prio;
				ordening(cantElem-1);
			}else{
				//System.out.println(i);
			}
			
		}
		return true;
	}
	
	//borra el elemento de menor prioridad
	public void borrar(){
		if(cantElem == 0){
			System.out.println("Vacio");
		}else{
			arbol[0] = arbol[cantElem-1];
			tier[0] = tier[cantElem-1];
			tier[cantElem-1] = Integer.MAX_VALUE;
			arbol[cantElem-1] = null;
			cantElem--;
			ordening(0);
		}
	}
	
	public void imprimir(){
		for(int i = 0; i< cantElem; i++){
			System.out.print(arbol[i].toString()+" ");
		}
		System.out.println("|");
		for(int i = 0; i< cantElem; i++){
			System.out.print(tier[i]+" ");
		}
		System.out.println();
		System.out.println();
	}

	//retorna y borra el elemento de menor prioridad
	public Object poll(){
		Object aux = arbol[0];
		borrar();
		return aux;
	}
	
	//actualiza la prioridad del elemento "c"
	public void actualizar(Object c, int a){
		int i = 0;
		while(i < cantElem && !arbol[i].equals(c)){
			i++;
		}
		if(i< cantElem){
			tier[i] = a;
			ordening(i);
		}
	}
}