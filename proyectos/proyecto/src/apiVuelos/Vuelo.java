package apiVuelos;
/**
 * Clase Vuelo: esta clase caracteriza un objeto Vuelo
 * 
 */
public class Vuelo {
	
	private String nombre_;
	private Aeropuerto aPartida_;
	private Aeropuerto aArribo_;
	private int hsPartida_;
	private char partidaAPm_;
	private int hsArribo_;
	private char arriboAPm_;
	
	public Vuelo(){		
	}

	public Vuelo(String nombre, Aeropuerto aPartida, Aeropuerto aArribo, int hsPartida,char partidaAPm, int hsArribo, char arriboAPm){
		nombre_ = nombre;
		aPartida_=aPartida;
		aArribo_=aArribo;
		hsPartida_=hsPartida;
		hsArribo_= hsArribo;
		partidaAPm_=partidaAPm;
		arriboAPm_= arriboAPm;
	}
	
	public String getNombre() {
		return nombre_;
	}
	
	public void setNombre(String nombre_) {
		this.nombre_ = nombre_;
	}
	
	public Aeropuerto getaPartida() {
		return aPartida_;
	}
	
	public void setaPartida(Aeropuerto aPartida_) {
		this.aPartida_ = aPartida_;
	}
	
	public Aeropuerto getaArribo() {
		return aArribo_;
	}
	
	public void setaArribo(Aeropuerto aArribo_) {
		this.aArribo_ = aArribo_;
	}
	
	public int getHsPartida() {
		return hsPartida_;
	}
	
	public void setHsPartida(int hsPartida_) {
		this.hsPartida_ = hsPartida_;
	}
	
	public int getHsArribo() {
		return hsArribo_;
	}
	
	public void setHsArribo(int hsArribo_) {
		this.hsArribo_ = hsArribo_;
	}
	
	public char getPartidaAPm() {
		return partidaAPm_;
	}
	
	public void setPartidaAPm(char partidaAPm_) {
		this.partidaAPm_ = partidaAPm_;
	}
	
	public char getArriboApm() {
		return arriboAPm_;
	}
	
	public void setArriboAPm(char arriboAPm_) {
		this.arriboAPm_ = arriboAPm_;
	}	
	
	public String toString2(){
		return ("Vuelo: "+ nombre_+ "- Desde: "+aPartida_.getCodigo()+"- Hora: "+hsPartida_+" "+partidaAPm_+"- Hasta: "+aArribo_.getCodigo()+"- Hora: "+ hsArribo_+" "+ arriboAPm_ );
	}
	
	//Metodo adicional para mostrar de otra manera la informacion del vuelo.
	public String toString(){
		String hSal = String.format("%02d:%02d", hsPartida_/100, hsPartida_%100);
		String hLle = String.format("%02d:%02d", hsArribo_/100, hsArribo_%100);
		return ("Vuelo: "+ nombre_+ " | " + hSal + " "+ partidaAPm_ +"M"+ " ["+ aPartida_.getCodigo()+ "]" + " ->> " + hLle + arriboAPm_+"M" + " [" + aArribo_.getCodigo() + "]" );
	}
	
	//(Adicional)equals(Object obj) entre dos Vuelo's, compara por los campos 'nombre_', 'aPartida_', 'hsPartida_', 'partidaAPm'
	public boolean equals(Object obj){
		if (obj instanceof Vuelo){
			return nombre_.equalsIgnoreCase(((Vuelo) obj).getNombre()) && aPartida_.equals(((Vuelo) obj).aPartida_) && (hsPartida_== ((Vuelo) obj).hsPartida_) && (partidaAPm_==(((Vuelo) obj).partidaAPm_));
		}
		return false;
	}
}