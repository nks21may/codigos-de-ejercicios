package apiVuelos;

/**
 * Clase Aeropuerto: esta clase caracteriza un objeto Aeropuerto
 *       
 */

public class Aeropuerto {

	private String codigo;		/* Código de 3 letras*/
	private int ajusteHorario; /* Diferencia huso horario en base a GTM.*/
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getAjusteHorario() {
		return ajusteHorario;
	}

	public void setAjusteHorario(int ajusteHorario) {
		this.ajusteHorario = ajusteHorario;
	}
	
	public String toString(){
		return (codigo);
	}
	
	//Compara 2 aeropuertos.
	public boolean equals(Object air){
		if (air instanceof Aeropuerto){
			return this.codigo.equalsIgnoreCase(((Aeropuerto) air).getCodigo());
		}
		return false;
	}
}
