%include "io.inc"

; .bss tiene datos no inicializados en RAM y se inicializan con 0 en el codigo
; .data es para variables inicializadas y cuentan con RAM y FLASH

section .data
    l1 dd 88h
    l2 dw 'a', 0
    l3 dd 'ndadaa', 0
    l4 db 'corta', 0
    
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    xor eax, eax
    movsw eax, l2
    movsd ebx, l3
    movsb ecx, l4
    ret