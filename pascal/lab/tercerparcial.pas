{***************************************************************************** 
 * Tercer Parcial Ejercicio de Laboratorio
 * 
 * Nombre: Nicolas D'alessandro
 * Comision: 1 rojo
 *****************************************************************************
}

program tercerParcial;

uses crt;

const
	N=200;
type
	{Registro definido para llevar el arreglo y la marca}
	Tcinta= record
               sec: array[1..N] of integer;
               marca:integer; //va a utilizar marca real o virtual?
               end;
               //Indique que tipo de marca utiliza: marca virtual
var
	cintaInt:Tcinta;
	c : integer; //almacenar la cantidad de primos

{Acción que carga de numeros positivos un arreglo
Modifique la acción para utilizar algun tipo de marca ya sea virtual o real
}
procedure Cargar(var a:Tcinta);
var
	q:integer;
	int:integer;
begin	
	q:=1;
	writeln('Cargando el arreglo. Para finalizar la carga ingrese 0 o un negativo');
	repeat		
		write('Ingrese un numero: ');
		readln(int);
		if int > 0 then
		begin
			a.sec[q]:=int;
			q:=q+1;
		end;	
	until (int<=0);
	writeln(' ');
	a.marca := q;
	//que tiene el campo a.marca? se corresponde a la eleccion que hizo de marca virtual o real?
end;

procedure escribirln(g : String);
begin
	writeln(g);
end;

function esPrimo(a : integer): Boolean;
var
	gg: integer;
	con : integer;
begin
	if (a mod 2 = 0) then
		esPrimo := false
	else
	begin // Esquema R1. 
		con := 0; //inicializacion del tratamiento
		gg := 1; 
		while (gg <= a) do //recorre todos los posibles divisores
		begin
			if (a mod gg = 0) then //tratamiento del EC
			begin
				con := con +1;
			end;
			gg := gg + 1; //obtencion del siguiente
		end;
		esPrimo := (con = 2) //tratamiento final
	end;
end;


function cantidadPrimos(a : Tcinta): Integer; //Esquema R2
var 
	i, cont : integer;
begin
	i := 1; //inicializacion
	case a.marca of
		1 : begin
			escribirln('El arreglo esta vacio'); //tratamiento secuencia vacia
			cantidadPrimos := 0;
			end; 
		else
			begin
				cont := 0; //inicializacion del tratamiento
				while (i < a.marca) do //mientras no fin de secuencia
				begin
					if (esPrimo(a.sec[i])) then //tratamiento del elemento corriente
					begin
						cont := cont + 1;
					end;
					i := i + 1; //obtener siguiente elemento
				end;
				cantidadPrimos := cont; //tratamiento final
			end;
	end;
end;

Begin
	c:= 0;
	new(asd);
	Cargar(cintaInt);
	c := cantidadPrimos(cintaInt);
	if c = 0 then 
		writeln('No hay numeros primos en el arreglo')
	else 
		writeln('Hay ', c,' numeros primos en el arreglo');
end.
