//  *************************************************************
//  Unidad con funciones comunes para el tratamiento de palabras
//  *************************************************************

unit Trat_Palabras_Unit;
interface
uses crt;

const
	N=100;
type
	cinta=array[1..N] of char;
	//registro con el arreglo, indice que referencia al elemento corriente y la marca.
	TFrase = record
	   sec:cinta;
	   ind:integer;
	   marca:integer;
	 end;

//Funcion que nos dice si es ultimo elemento	 
function EsUlt( a:TFrase):boolean;
//Accion que encuentra la primer palabra (consume los primeros espacios en blanco)
procedure ArrPal(var a:TFrase);
//Accion que avanza a la siguiente palabra (consume los espacios en blanco)	
procedure AvPal(var a:TFrase);
//Accion que carga el arreglo
procedure Cargar(var a:TFrase);
//Accion que muestra el arreglo
procedure Mostrar(a:TFrase);
	
implementation

//Accion que carga el arreglo
procedure Cargar(var a:TFrase);
var
	car:char;
begin	
	a.ind:=1;
	writeln('Ingrese el caracter o * para terminar');
	repeat		
		car:=readkey;
		write(car);
		delay(50);
		if car <> '*' then
		begin
			a.sec[a.ind]:=car;
			a.ind:=a.ind+1;
		end;	
	until (car='*');
	writeln(' ');
	a.marca:=a.ind;
	a.ind:= a.ind - 1;
end;

//Accion que muestra el arreglo
procedure Mostrar(a:TFrase);
var i:integer;
begin
 i := 1;
 if (i = a.marca)
 then
    writeln('Secuencia vacia')
 else
    while i <> a.marca do
      begin
       write(a.sec[i]);
       i := i + 1;
      end;   
  write('   ind = ', a.ind,'   marca = ', a.marca);
 writeln;   
end;

//Accion que encuentra la primer palabra, consume los primeros espacios en blanco, 
//nos deja como elemento corriente la primera letra de la primera palabra que encuentra
procedure ArrPal(var a:TFrase);
var
	j:integer;
begin
	j:=1;
	while a.sec[j]=' ' do
	begin
		j:=j+1
	end;
	a.ind:=j
end;

//Accion que avanza a la siguiente palabra, es decir, consume los espacios en blanco
//hasta la siguiente palabra
procedure AvPal(var a:TFrase);
begin
	while a.sec[a.ind]=' ' do
	begin
		a.ind:=a.ind+1
	end;
end;

//Funcion que nos dice si es ultimo elemento	 
function EsUlt(a:TFrase):boolean;
begin
	EsUlt:=(a.ind = a.marca)
end;

end.

