program agenda;
uses crt;

const
	n = 5;

type Tarreglo = array[1..n] of Integer;

var arre : Tarreglo;

procedure cargaA();
	var i : Integer;
	begin
		for i := 1 to n do
		begin
			arre[i] := random(10);
		end;
	end;

procedure muestraArre(a : Tarreglo);
	var i : Integer;
	begin
		writeln('arreglo...');
		for i := 1 to n do
		begin
			write(a[i],' ');
		end;
		writeln;
	end;

function nnn(a : Tarreglo): Boolean;
	var j, i: Integer;
	aux : Boolean;
	begin
	aux := true;
		for i := 1 to n do
		begin
			for j := 1 to n do
			begin
				if (a[i] = a[j]) and (i <> j) then
				begin
					aux := false;
				end;
			end;
		end;
		nnn := aux;
	end;

begin
	randomize;
	cargaA;
	clrscr;
	muestraArre(arre);
	writeln(nnn(arre))
end.
