program preguntitas;
uses crt;
type Tpreguntas = record
	preg : String;
	resp : String;
	n : integer;
end;

type TpregFile = file of Tpreguntas;

var
	arch : TpregFile;
	cant : integer;
	e : boolean;
	z : char;
	nomb : String;

procedure escribir(a : String);
begin
	writeln(a);
end;

procedure cargaPreguntas(var a : TpregFile; var i : integer);
var 
	g : Tpreguntas;
	c : char;
	begin
	i := 0;
	reset(a);
	writeln('Desea carga preguntas? (y/n)');
	c := readkey();
	if (c = 'y') or (c = 'Y') then
	begin
		repeat
			clrscr;
			i := i +1;
			writeln('Ingrese la pregunta');
			readln(g.preg);
			writeln('Ingrese la respuesta de la pregunta cargada');
			readln(g.resp);
			g.n := i;
			write(a, g);
			writeln('Otra pregunta? (y/n)');
			c := readkey();
		until (c = 'n') or (c = 'N');
	end;
end;

function pregEnPos(var a : TpregFile; i : integer): Tpreguntas;
var g : Tpreguntas;

begin
	reset(a);
	read(a, g);
	while g.n <> i do
	begin
		read(a,g);
	end;
	close(a);
	pregEnPos := g;
end;

procedure mostrarPreguntas(var a : TpregFile; var n : integer);
var
	c : char;
	g : Tpreguntas;
	r : Boolean;
	i : integer;
begin
	randomize;
	writeln('Desea preguntas aleatorias? (y/n)');
	c := readkey();
	r := false;
	i := ;0
	if (c = 'y') or (c = 'Y') then
		r := true;
	repeat
		if r then 
			i := random(n)+1
		else 
			i := i+1;
		g := pregEnPos(a, i);
		writeln(g.preg);
		readkey();
		writeln(g.resp);
		writeln('Desea otra pregunta? (y/n)');
		c := readkey();

		if (i = n) then
		begin
			i := 0;
		end;
	until (c = 'n') or (c = 'N');
end;

begin
	nomb := 'preg';
	assign(arch, nomb);
	e := true;
	{$I-}
	reset(arch);
	{$I+}
	if IOResult = 2 then
	begin
		rewrite(arch);
		e := false;
	end;
	close(arch);
	if e then
	begin
		writeln('Desea usar las preguntas ya cargadas? (y/n)');
		z := readkey();
	end;
	if (z <> 'y') or (z <> 'Y') then
		cargaPreguntas(arch, cant);
	if cant <> 0 then
		mostrarPreguntas(arch, cant)
end.	