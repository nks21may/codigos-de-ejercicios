program test;
uses crt, unitLista;

var
    pal1: Lista;
    pal2: Lista;
    a: TInfo;

procedure cargaLista(var l: Lista);
var
    letra: TInfo;
    letraant : char; 
    c: Byte;
begin
    c := 0;
    letraant := 'x';
    while ((c <> 27) and (c <> 13)) do
    begin
        clrscr;
        writeln('Ingrese palabra/frase a cargar y presione enter');
        writeln();
        MostrarLista(l);
        c := ord(readkey());
        letra.caracter := chr(c);
        if ((letra.caracter <> letraant) and (c <> 27) and (c <> 13)) then
        begin
            if (c = 8) then
            begin      
                if (l.cant>1) then
                    EliminarFinal(l)
            end
            else
                InsertarEnPos(letra, l, l.cant)
        end;
        delay(70);
    end;
    clrscr;
end;

begin
    Inicializar(pal1);
    Inicializar(pal2);
    cargaLista(pal1);
    cargaLista(pal2);
    MostrarLista(pal1);
    MostrarLista(pal2);

    writeln(TieneElem(pal1));
    a.caracter := '5';
    InsertarEnPos(a, pal1, 5);

    MostrarLista(pal1);
    InsertarEnPos(a, pal1, 2);
    MostrarLista(pal1);
    InsertarEnPos(a, pal1, 7);
    MostrarLista(pal1);
    writeln(EsVacia(pal2));
    writeln(longitud(pal2));

    eliminarEnPos(pal2, 4);
    MostrarLista(pal2);
    eliminarEnPos(pal2, 2);
    MostrarLista(pal2);
    eliminarEnPos(pal2, 7);
    MostrarLista(pal2);

end.
