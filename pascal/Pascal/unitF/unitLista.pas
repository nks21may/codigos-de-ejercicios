{********** Lista Simplemente Enlazada (LSE)  **********************
       Implementación utilizando un elemento ficticio.
Esta unit, crea la lista (deben decidir si con elemento ficticio o no)
y tiene dos punteros, uno q apunta siempre al
Inicio de la lista y otro que apunta siempre al final de la lista.
Nicolas Dalessandro
********************************************************************}

unit unitLista;

Interface
  
{********** Tipos visibles para quienes utilizan esta Unidad **********}
 type
    TPuntero = ^TNodo; {Puntero a un nodo de la lista.}
    TInfo= record {Tipo de la informacion que va a contener la lista.}
            caracter:char; 
           end;
    TNodo = record {Representa un nodo de la lista.}  
             info: TInfo; 
             next: TPuntero; {Puntero al nodo siguiente.}                
            end;
    Lista = record
             Pri: TPuntero; {Puntero al primer nodo de la lista.}
             Ult: TPuntero; {Puntero al ultimo nodo de la lista.}
             cant : integer; {Contiene la cantidad de nodos que tiene la lista.}
            end;

{======================================================================================}
{Declaraciones de las acciones y funciones visibles para quienes utilizan esta unidad}

{Retorna un Puntero al primer elemento valido de la Lista, Fin(l) si esta vacia}
function Inicio( l : Lista ) : TPuntero;

{Retorna un Puntero al primer elemento no valido de la Lista}
function Fin ( l : Lista ) : TPuntero;

{Retorna un Puntero al ultimo elemento valido de la Lista, Fin(l) si esta vacia}
function Ultimo (l : Lista) : TPuntero;

{Retorna el puntero al siguiente elemento de un elemento valido}
{pre: pos <> nil}
function Siguiente(pos : TPuntero ) : TPuntero;

{Retorna True si la Lista esta vacia, falso en caso contrario}
function esVacia( l : Lista ) : boolean;

{Retorna el valor del elemento que corresponde a un Puntero }
{pre: pos <> nil}
function Obtener( pos : TPuntero) : TInfo;

{Retorna la longitud de la Lista }
function Longitud( l : Lista ) : integer;

{Inicializa la Lista como vacia }
procedure Inicializar( var l : Lista );

{Modifica el elemento que esta en un Puntero }
{pre: pos <> nil}
procedure Modificar( e : TInfo; var pos : TPuntero);

{Inserta un elemento al principio de la Lista }
procedure InsertarAlInicio(  e : TInfo; var l : Lista);

{Inserta un elemento al final de la Lista }
procedure InsertarAlFinal(  e : TInfo;  var l : Lista);

{Inserta un elemento en una posicion dada}
procedure InsertarEnPos(e: TInfo; var l: Lista; pos: integer);
        
{Elimina el primer elemento de la Lista}
{pre: Longitud(l) >= 1 }
procedure EliminarPrincipio(var l : Lista);

{Elimina el elemento que se encuentra en la posicion dada}
{pre: pos <= Loncitud(l)}
procedure EliminarEnPos(var l: Lista; pos: integer);

{Elimina el ultimo elemento de la Lista}
{pre: Longitud(l) >= 1 }
procedure EliminarFinal(var l : Lista);

{Controla si un elemento dado se encuentra en la lista}
function TieneElem(l : Lista): boolean;

{Muestra los elementos de l}
procedure MostrarLista(l: Lista);

{Verifica condiciones para que una accion/funcion funcione correctamente, de no ser asi, cierra el programa.}
procedure Verificar( cond : boolean; mensaje_error : string );

{======================================================================================}
{Implementación del Módulo}
Implementation
{******************************************************************************************}
{********** Declaracion de Acciones y Funciones Auxiliares locales al módulo **************}
{******************************************************************************************}

{Incrementa en 1 el campo cant de la lista }
procedure IncrementarCantidadElementos(var l : Lista); forward;

{Decrementa en 1 el campo cant de la lista }
procedure DecrementarCantidadElementos(var l : Lista); forward;

{Crea un nuevo Nodo de la Lista y le setea la informacion recibida como parametro}
procedure CrearNuevoNodo( var nuevoNodo : TPuntero; e: TInfo); forward;


{******************************************************************************************}
{************************ Implementación de Acciones y Funciones Exportadas ***************}
{******************************************************************************************}

{Retorna un Puntero al primer elemento valido de la Lista, Fin(l) si esta vacia}
function Inicio( l : Lista ) : TPuntero;
begin
    Inicio:= (l.pri^).next;      //retorno el puntero que sigue al ficticio. 
end;

{Retorna un Puntero al primer elemento no valido de la Lista}
function Fin ( l : Lista ) : TPuntero;
begin
    Fin := nil;
end;

{Retorna un Puntero al ultimo elemento valido de la Lista, Fin(l) si esta vacia}
function Ultimo (l : Lista) : TPuntero;
begin
    if (l.pri = l.ult) then //lista vacia
        Ultimo:= (l.ult^).next
    else
        Ultimo := l.ult
end;

{Retorna el puntero al siguiente elemento de un elemento valido}
{pre: pos <> nil}
function Siguiente(pos : TPuntero ) : TPuntero;
begin
    Verificar(pos <> nil,'No se puede obtener siguiente posicion. Puntero nulo.');
    Siguiente := pos^.next;
end;

{Retorna True si la Lista esta vacia, falso en caso contrario}
function EsVacia( l : Lista ) : boolean;
begin
    if l.cant=0 then
        EsVacia:=true
    else
        EsVacia:=false;
end;

{Retorna el valor del elemento que corresponde a un Puntero }
{pre: pos <> nil}
function Obtener( pos : TPuntero) : TInfo;
begin
    Verificar(pos <> nil,'No se puede obtener valor. Puntero nulo.');
   Obtener := pos^.info;
end;

{Retorna la longitud de la Lista.}
function Longitud( l : Lista ) : integer;
begin
    Longitud := l.cant;
end;

{Inicializa la Lista como vacia.}
procedure Inicializar( var l : Lista );
begin
    //usa elemento ficticio o no?
    l.pri := nil;
    l.ult := nil;
    l.cant := 1;
end;

{Modifica el elemento que esta en un Puntero.}
{pre: pos <> nil}
procedure Modificar (e:TInfo; var pos : TPuntero);
begin
    Verificar(pos <> nil,'No se puede modificar valor. Puntero nulo.'); 
    pos^.info.caracter := e.caracter;
end;


{Inserta un elemento al principio de la Lista.}
procedure InsertarAlInicio(e:TInfo; var l : Lista);
var
    r: TPuntero; { puntero al Nuevo nodo que se va a Insertar.}   
begin
    CrearNuevoNodo(r, e);
    r^.next := l.pri;
    l.pri := r;
end;

{Inserta un elemento en una posicion dada}
{pre: pos <= Longitud(l) && pos > 0}
procedure InsertarEnPos(e: TInfo; var l: Lista; pos: integer);
var
    i: Integer;
    a : TPuntero;
    b : TPuntero;
begin
    Verificar(pos <= l.cant,'No se puede insertar en la posicion.');
    if (pos = 1) then
    begin
        InsertarAlInicio(e, l);
    end
    else
    begin
        a := l.pri;
        i := 1;
        while (i < (pos-1)) do
        begin
            i := i+1;
            a := siguiente(a);
        end;
        CrearNuevoNodo(b, e);
        b^.next := a^.next;
        a^.next := b;
    end;
    l.cant := l.cant +1;
end;

{Inserta un elemento al final de la Lista }
procedure InsertarAlFinal(e:TInfo;  var l : Lista);
var
    r: TPuntero; { puntero al Nuevo nodo que se va a Insertar.} 
begin
    CrearNuevoNodo(r,e);    
    InsertarEnPos(e, l, l.cant);
    l.ult := l.ult^.next;
    l.ult^.next := nil;
end;

{ Elimina el primer elemento de la Lista}
{ pre: Longitud(l) >= 1 }
procedure EliminarPrincipio(var l : Lista);
var
    r : TPuntero; { puntero al nodo q vamos a eliminar}    
begin
    Verificar((l.pri <> l.ult), 'La lista esta vacia no se puede Eliminar.');
    r:=l.pri;   
    l.pri := l.pri^.next;
    dispose(r);
end;

{Elimina el elemento que se encuentra en la posición dada}
{pre: pos <= Longitud(l) && pos > 0}
procedure EliminarEnPos(var l: Lista; pos: integer);
var
    aux: TPuntero;
    ne: TPuntero;
    i: integer;
begin
    Verificar(l.cant <> 0 ,'No se puede eliminar. Esta vacio');
    Verificar(l.cant >= pos ,'No se puede eliminar en esa posicion');
    if (l.cant = 1) then 
        EliminarPrincipio(l)
    else 
    begin
        i := 1;
        aux := l.pri;
        while (i < (pos -1)) do
        begin
            aux := siguiente(aux);
            i:= i+1;
        end;
        ne := aux^.next^.next;
        dispose(aux^.next);
        aux^.next := ne;
    end;
    l.cant := l.cant -1;
end;

{Elimina el ultimo elemento de la Lista}
{ pre: Longitud(l) >= 1 }
procedure EliminarFinal(var l : Lista);    
begin
    EliminarEnPos(l, (l.cant - 1));
end;

{Controla si un elemento dado se encuentra en la lista}
function TieneElem(l : Lista): boolean;
begin
    TieneElem := (l.cant > 1);
end;

{Muestra los elementos de l}
procedure mostrarNodo(a : TPuntero);
begin
    write(a^.info.caracter);
end;

procedure MostrarLista(l: Lista);
var
    i: Integer;
    a: TPuntero;
begin
    i:= 1;
    a:= l.pri;
    while (i< l.cant) do
    begin
        mostrarNodo(a);
        a := siguiente(a); 
        i := i+1
    end;
    writeln();
end;

{ Verifica condiciones para que una accion/funcion funcione correctamente, de no ser asi, cierra el programa.}
procedure Verificar( cond : boolean; mensaje_error : string );
begin
    if (not cond) then
    begin
        writeln('ERROR: ', mensaje_error);
        halt;
    end;
end;

{******************************************************************************************}
{********** Implementación de Acciones y Funciones Auxiliares locales al módulo **********}
{******************************************************************************************}

//--------------------------------------------------------
procedure IncrementarCantidadElementos(var l : Lista);
begin
    l.cant := l.cant + 1;
end;

//--------------------------------------------------------
procedure DecrementarCantidadElementos(var l : Lista);
begin
    l.cant := l.cant - 1;
end;


//--------------------------------------------------------
procedure CrearNuevoNodo( var nuevoNodo : TPuntero; e: TInfo);
begin 
    new(nuevoNodo);
    (nuevoNodo^.info).caracter := e.caracter;
end;

end.
