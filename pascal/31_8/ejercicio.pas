program ejercicio;
uses crt;
const
	n = 5;
	m = 5;
	ran = 10;

type Tmatriz = array[1..n, 1..m] of Integer;

var a : Tmatriz;
	l : integer;
	h : integer;

function mayor(s: Integer; d: Integer): Boolean;
begin
	if s >= d then 
		mayor := true
	else 
		mayor := false
end;

procedure mostrar(g : Tmatriz);
	var i, h : integer;
begin
	for i := 1 to n do
	begin
		for h := 1 to m do
		begin
			write(g[i,h],' ');
		end;
		writeln;
	end;
end;

procedure carga();
	var i, h : integer;
begin
	for i := 1 to n do
	begin
		for h := 1 to m do
		begin
			a[i,h] := random(ran+1);
		end;
		writeln;
	end;
end;

function sumaFila(b : Tmatriz; x : integer): Integer;
var i, aux :integer;
begin
	aux := 0;
	i := 1;
	while i <= m do
	begin
		aux := aux + b[x,i];
		i := i+1;
	end;
	sumaFila := aux;
end;


begin
	clrscr;
	randomize;
	l := 1;
	h := 1;
	carga();
	mostrar(a);
	while l <= n  do
	begin
		if (mayor(sumaFila(a, l), sumaFila(a, h))) then
		begin
			h:= l;
		end;
		l:= l+ 1;
	end;
	writeln('La fila mas grande es ',h);
	
end.
