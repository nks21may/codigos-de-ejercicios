{Nicolas Dalessandro C1}
program parcialete; {Permite cargar una lista y un numero y muestra cuantos multiplos de ese numero aparecen en la lista}
uses crt;

type
	Tpuntero = ^Tnodo;
	Tnodo = record
		nro : integer;
		next : Tpuntero;
	end;
var
	a: Tpuntero;


procedure cerrarListas(var x: Tpuntero);{libera todos los punteros}
var
	aux: Tpuntero;
begin
	while (x <> nil) do
	begin
		aux := x;
		x := x^.next;
		dispose(aux);	
	end;
end;

procedure inicializarPuntero(var x: Tpuntero); {obtiene memoria}
begin
	new(x);
	x^.next := nil;
end;

procedure cargarLista(x:Tpuntero); { permite cargar la lista con los posibles multiplos}
var
	aux: Tpuntero;
	n: integer;
	i: integer;
begin
	repeat
	writeln('Cuantos elementos desea que tenga la lista? (numero positivo)');
	readln(n);
	until(n>= 0);
	for i := 1 to n do
	begin
		inicializarPuntero(aux);
		writeln('Ingrese numero');
		readln(aux^.nro);
		x^.next := aux;
		x := x^.next;
	end;
end;

procedure mostrarLista(x: Tpuntero);{muestra una LSE}
begin
	x := x^.next;
	while (x <> nil) do
	begin
		write(x^.nro, ' - ');
		x := x^.next;
	end;
	writeln();
end;

procedure multi(v: integer; x: Tpuntero); {Busca los multiplos de un numero y los guarda en una LSE}
var
	i: integer;
	aux: Tpuntero;
begin
	for i := 1 to v do
	begin
		if ((v mod i) = 0) then
		begin
			inicializarPuntero(aux);
			x^.next := aux;
			aux^.nro := i;
			x := aux;
		end;
	end;
end;

function aparece(n: integer; x: Tpuntero): boolean; {se fija si un numero aparece en una LSE}
var
	b: boolean;
begin
	b := false;
	x := x^.next;
	while ((x <> nil) and (not b)) do
	begin
			if (x^.nro < 0) then {por si el usuario busca negativos}
			begin
				x^.nro := (x^.nro)*(-1);
			end;

		if (x^.nro = n) then
		begin
			b := true;
		end;
		x := x^.next;
	end;
	aparece := b;
end;

procedure multiplo(x: Tpuntero);{Parte principal del programa}
var
	n: integer;
	m: Tpuntero;
begin
	clrscr;
	writeln('Ingrese el numero a buscar multiplos');
	readln(n);
	if (n < 0) then {por si el usuario busca negativos}
	begin
		n := n*(-1);
	end;
	inicializarPuntero(m);
	multi(n, m);
	writeln('Los multiplos de ese numero son:');
	mostrarLista(m);
	writeln('Los multiplos de su numero que aparecen en la lista que usted cargo son');
	m := m^.next;
	while (m <> nil) do
	begin
		if aparece(m^.nro, x) then
		begin
			write(m^.nro, ' - ');
		end;
		m := m^.next;
	end;
	writeln();
	writeln('Su lista contiene los siguientes numeros');
	mostrarLista(x);
end;

begin
	inicializarPuntero(a);
	cargarLista(a);
	mostrarLista(a);
	multiplo(a);
	cerrarListas(a);
end.
