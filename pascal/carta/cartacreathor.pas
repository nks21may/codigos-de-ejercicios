program creathor;
uses crt;

type Tdir = record
		nom : string;
		direc : string;
	
end;
type Ttext = Text;
type Tarch = file of Tdir;

var
	soc: Tarch;
	carta: Ttext;

procedure cargaSocios(var a : Tarch);
var
	g : Tdir;
	c : char;
begin
	reset(a);
	seek(a, filesize(a));
	c := 'g';
	writeln('¿Desea cargar socios? (y/n)');
	c := readkey();
	while (upcase(c) <> 'N') do
	begin
		writeln('Ingrese el nombre');
		readln(g.nom);
		writeln('Ingrese la direccion');
		readln(g.direc);
		write(a, g);

		writeln('¿Desea cargar otro socio? (y/n)');
		c := readkey();
	end;
	close(a);
end;

procedure archExiste(var a : Tarch; var b : Ttext); {se fija si el archivo esta creado, sino lo creo}
begin
	{$I-}
	reset(a);
	{$I+}
	if IOResult = 2 then
		rewrite(a);
	close(a);

	{$I-}
	reset(b);
	{$I+}
	if IOResult = 2 then
	begin
		rewrite(b);
		writeln('No cargo la carta');
	end;
	close(b);
end;

procedure mostrarSocios(var a : Tarch);
	var p : Tdir;
begin
	reset(a);
	if EOF(a) then
		writeln('No hay socios cargados')
	else
	begin
		writeln('Nombre      Direccion');
		while not (EOF(a)) do
		begin
			read(a, p);
			writeln(p.nom,'     ', p.direc);
		end;
		
	end;
	close(a);
end;

procedure crearCartas(var s : Tarch; var c : Ttext);
var
	g : Tdir;
	z : Ttext;
	q : string;
begin
	reset(s);
	while not(EOF(s)) do
	begin
		read(s, g);
		assign(z, ('carta para '+ g.nom));
		rewrite(z);
		writeln(z, ('Estimado/a '+g.nom));
		writeln(z, g.direc);
		reset(c);
		while not (EOF(c)) do
		begin
			readln(c, q);
			writeln(z, q);
		end;
		close(c);
		close(z);
	end;
	close(s);
end;

procedure menu(var so : Tarch; var cart : Ttext);
	var
		c : char;
begin
	c:= '0';
	repeat
		clrscr;
		writeln('Presione la letra para la opcion');
		writeln('a) Cargar registro');
		writeln('b) Mostrar registro');
		writeln('c) Generar carta');
		writeln('x) Cierra este programa');
		c := readkey();
		case c of
			'a': cargaSocios(so);
			'b': mostrarSocios(so);
			'c': crearCartas(so, cart);
		end;
		delay(1000);
	until upcase(c) = 'X';
end;

begin
	assign(soc, 'lista');
	assign(carta, 'carta.txt');
	archExiste(soc, carta);
	menu(soc, carta);
end.

