program ejnueve;
uses crt;

type 
	Tpuntero = ^Tnodo;
	Tnodo = record
		n : Integer;
		next : Tpuntero;
	end;
	Tarr = record
		a : Tpuntero;
		i : Integer;
	end;

	TpuntPer = ^TnodoPer;
	TnodoPer = record
		ape : String;
		nom : String;
		ant : Integer;
	end;
	TarrPer = record
		i : Integer;
		a : TpuntPer;
	end;

var
	a: Tarr;

procedure inicializar(var g: Tarr);
begin
	g.i:= 0;
	new(g.a);
	g.a^.next := nil;
end;

procedure carga(var g: Tarr);
var
	aux: Tpuntero;
	r: Tpuntero;
	i: Integer;
begin
	writeln('Cuantos elementos desea cargar?');
	readln(g.i);
	aux := g.a;
	for i := 1 to g.i do
	begin
		clrscr;
		writeln('Ingrese el numero');
		new(r);
		readln(r^.n);
		aux^.next := r;
		aux := r;
	end;
	aux^.next := nil;
end;

procedure cerrar(var g: Tarr);
var
	aux: Tpuntero;
begin
	while (g.a <> nil) do
	begin
		aux := g.a^.next;
		dispose(g.a);
		g.a := aux;
	end;
end;

procedure mostrarPositivos(a: Tarr);
var
	pos: Integer;
begin
	pos := 0;
	a.a := a.a^.next;
	while a.a <> nil do
	begin
		if (a.a^.n > 0) then
			pos := pos + 1;
		a.a := a.a^.next;
	end;
	if pos >0 then 
		writeln('Hay ', pos, ' numeros positivos')
	else 
		writeln('No hay positivos');
end;

procedure nueve(g : Tarr);
begin
	inicializar(g);
	carga(g);
	mostrarPositivos(g);
	cerrar(g);
end;

procedure cargaEsc(var g: Tarr);
var
	aux: Tpuntero;
	r: Tpuntero;
	i: Integer;
	f: Integer;
begin
	writeln('Cuantos elementos desea que se carguen?');
	readln(g.i);
	writeln('Desde que numero?');
	readln(f);

	aux := g.a;

	for i := 1 to g.i do
	begin
		clrscr;
		new(r);
		r^.n := f;
		f:= f+1;
		aux^.next := r;
		aux := r;
	end;
	aux^.next := nil;
end;

procedure sen(g: Tarr; z: Integer);
begin
	while (g.a^.n <> z) and (g.a <> nil) do
	begin
		g.a := g.a^.next;		
	end;
	if g.a <> nil then
	begin
		while (g.a <> nil) do
		begin
			writeln(g.a^.n);
			g.a := g.a^.next;	
	end;
	end
	else
		writeln('No aparece tu numerito en la lista :"c')
end;

begin
	clrscr;
	nueve(a);
end.
