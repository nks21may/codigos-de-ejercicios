program prueva;

const N = 10;
type TarregloInt = array[1..N] of integer;

function Pertenece(a:TarregloInt; x:integer): boolean;
var
	i: integer;
	res: boolean;
begin
	i := 1;
	res := false;
	while (i<N) and (not res) do
	begin
		if (a[i] = x) then
			res := true;
		i:= i+1;
	end;
	Pertenece := res;
end;

begin

end.
