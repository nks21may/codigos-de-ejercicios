{Nicolas Dalessandro. Compilado en debian.}
program test;
uses crt, unitLista;

type Tarreglo = array[1..100] of Lista;

type Telem = record
    i : integer; //marca virtual
    a : Tarreglo;
    
end;
var
    a: Telem;

procedure inserta(var g: Telem);
begin
    g.i:= g.i+1;
    Inicializar(g.a[g.i]);
    cargaLista(g.a[g.i]);
end;

procedure borraelem(var g: Telem ; b : integer);
begin
    while (TieneElem(g.a[b])) do
    begin
        EliminarFinal(g.a[b]);
    end;
    g.i := g.i -1;
end;

procedure borrall(var g: Telem);
var
    f: integer;
begin
    if g.i >0 then
    begin
        for f := g.i to 1 do
        begin
            borraelem(g, f);
        end;
        writeln('listas borradas con exito');
    end;
end;

procedure mostrall(g: Telem);
var
    i: integer;
begin
    for i := 1 to g.i do
    begin
        writeln('Lista nro ', i);
        MostrarLista(g.a[i]);
        writeln('Contiene ', g.a[i].cant, ' caracteres');
        writeln;
    end;
end;

procedure acciones(var g: Telem);
var
    opp: integer;
    el: integer;
begin
    g.i := 0;
    opp := 5;
    while opp <> 0 do
    begin
        writeln('1. Cargar nueva lista (*Nota: a medida que presione teclas las ira guardando en los dobletes*)');
        writeln('2. Borrar lista completa');
        writeln('3. Comprobar estado de una lista');
        writeln('4. Comprobar estado de todas las listas');
        writeln('5. Borra todos los elementos de las listas');
        writeln('0. Para cerrar');
        readln(opp);
        clrscr;
        case opp of
            1: inserta(g);
            2: begin
                clrscr;
                writeln('Ingrese el numero de la lista que desea borrar');
                readln(el);
                if (el <= g.i) and (el > 0) then 
                borraelem(g, el)
                else 
                    writeln('No existe ese elemento')
            end; 
            3: begin
                clrscr;
                writeln('Ingrese el numero de la lista que desea ver');    
                readln(el);
                if (el <= g.i) and (el > 0) then 
                MostrarLista(g.a[el])
                else 
                    writeln('No existe ese elemento')
            end;
            4: mostrall(g);
            5: borrall(g);
            0: borrall(g);
        end;
    end;
end;


begin
    acciones(a);
end.
