import java.util.HashMap;

public class auxiliarRandom{

	
	public static int minCosto(int[] patch, int d, int h, HashMap<Integer, Integer> mapa){
		if (d == h){
			return 0;
		}
		if(patch[d] == 0){
			return Integer.MAX_VALUE;
		}
		int res = Integer.MAX_VALUE;
		if (mapa.get(d) != Integer.MAX_VALUE) {
			return mapa.get(d);
		}
		
		for (int i = d+1; i <= patch[d]+d && i <= h; i++) {	
			int jumps;
			jumps = minCosto(patch, i, h, mapa);
			if (jumps != Integer.MAX_VALUE  && jumps+1 < res) {
				res = jumps+1;
			}
		}
		mapa.replace(d, res);
		return mapa.get(d);
	}
	
	// arr arreglo, i = inicio, f=final
	public static boolean position(int arr[], int i, int f) {
		if (i < f) {
			int m = (f+i)/2;
			boolean res = false;
			if (arr[m] == m) {
				return true;
			}
			if (arr[m] > m) {
				res = position(arr,i,m);
			}
			if (!res) {
				res = position(arr,m+1, f);
			}
			
			return res;
		}
		return false;
	}

	public static int[][] maxRepe(int[][] a, int[][] b){
		if (a.length == 1 && b.length == 1) {
			if (a[0][0] == b[0][0]){
				a[0][1] += b[0][1];
				return a;	
			}else{
				if (a[0][1] > b[0][1]){
					return a;
				}
				if(a[0][1] < b[0][1]){
					return b;
				}
			}
		}

		for (int i = 0; i < a.length; i++){
			for (int j = 0; j < b.length; j++){
				if (a[i][0] == b[j][0]){
					a[i][1] += b[j][1]; 
				}
			}	
		}

		int[][] aux = {a[0]};
		for(int i = 1; i < a.length;i++){
			int[][] r = {a[i]};
			aux = maxRepe(aux,r);
		}

		if(aux[0][1] < b[0][1]){
			return b;
		}
		if(aux[0][1] > b[0][1]){
			return aux;
		}
		int[][] res = new int[b.length + aux.length][2];
		System.arraycopy(aux, 0, res, 0, aux.length);
		System.arraycopy(b, 0, res, aux.length, b.length);
		return res;
	}

	public static int[][] merege(int[] x, int begin, int end){
		int[][] res = {{x[begin],1}};
		if (begin < end) {
			int mid = (begin + end)/2;
			res = maxRepe(merege(x, begin, mid), merege(x, mid+1, end));
		}
		return res;
	}

	public static boolean mayoria(int[] x){
		return merege(x, 0, x.length-1)[0][1] >= x.length/2;
	}

	
	public static void main(String[] args) {
		int[] a = {1,1,1,2,4,3,5,6,7,1};
		System.out.println(merege(a,0,a.length-1)[0][0]);
		System.out.println(merege(a,0,a.length-1)[0][1]);
	}
	
}