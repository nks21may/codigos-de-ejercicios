
public class persona {

	public int age;
	public String name;
	public String lname;

	public void setAge(int a){
		age = a;
	}

	public void setName(String s){
		name = s;
	}

	public void setApellido(String s){
		lname = s;
	}

	public void show(){
		System.out.println("Apellido: "+lname);
		System.out.println("Nombre: "+name);
		System.out.println(("Edad: "+age));
	}
}