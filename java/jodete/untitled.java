import java.util.Scanner;
//import persona;

public class untitled {
	
	public static persona cargaPersona(){
		persona p = new persona();
		Scanner teclado = new Scanner(System.in);

		System.out.println("Ingrese el apellido de la persona");
		p.setApellido(teclado.next());
		System.out.println("Ingrese el nombre de la persona");
		p.setName(teclado.next());

		System.out.println("Ingrese la edad de la persona");
		p.setAge(teclado.nextInt());
		teclado.close();
		return p;
	}

	public static void main(String[] args) {
		persona p1 = new persona();
		int n;
		Scanner teclado = new Scanner(System.in);
		p1 = cargaPersona();
		p1.show();

		teclado.close();
	}
}
