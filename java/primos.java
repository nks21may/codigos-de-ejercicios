/**
 * Implementa algoritmos que generan listas de primos
 *
 */

import java.util.List;
import java.util.ArrayList;

public class PrimeAlgorithms {

    
    /**
     * Genera una Lista de elementos primos menores o iguales que input.
     * @param input cota de numeros primos.
     * @pre input > 1
     */
    public static List<Integer> primesByDef(int input) {
        // Tu tarea:
        // - implementar un algoritmo que retorne la lista de todos los primos menores o iguales a un input dado 
        // - controlar adecuadamente la entrada, lanzando las excepciones 
        //   correspondientes cuando las entradas sean invalidas
        List<Integer> lista = new ArrayList<Integer>();
        if (input > 1){
            for(int i = 2; i <= input; i++){
                boolean primo = true;
               for (int j = 2; j < i && primo; j++) {
                    primo = i%j != 0;
                }
                if(primo){
                    lista.add(i);     
                } 
            }
        }else{     
            throw new IllegalArgumentException();
        }
        return lista;
    }
    
    /**
     * Genera una Lista de elementos primos menores o iguales que input.
     * @param input cota de numeros primos.
     * @pre input > 1
     */
    public static List<Integer> primesBySieve(int input) {
        // Tu tarea:
        // - implementar un algoritmo que retorne la lista de todos los primos menores o iguales a un input dado 
        // - controlar adecuadamente la entrada, lanzando las excepciones 
        //   correspondientes cuando las entradas sean invalidas
        List<Integer> lista = new ArrayList<Integer>();
        if (input > 1){
            for(int i = 2; i <= input; i++) lista.add(i);
            
            boolean primo = true;
            for (int i = 0; i < lista.size(); i++) {
                for (int j = i+1; j < lista.size(); j++){
                    if(lista.get(j)%lista.get(i) == 0){
                        lista.remove(j);
                        j--;
                    }
                }
            }
        }else{     
            throw new IllegalArgumentException();
        }
        return lista;
    }

}