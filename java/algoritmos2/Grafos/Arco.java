/*Proyecto 2017 | Estructura de Datos y Algoritmos - Algoritmos I
 * Alumnos: - D'alessandro, Nicolas
 * 				D.N.I.: 41.105.833
 * 			- Garcia, Valeria
 * 				D.N.I.: 39.545.287
 */

package algoritmos2.Grafos;

@SuppressWarnings("unchecked")
//Clase que representa las aristas entre dos vertices de un grafo
public class Arco<N,A>{
	
	private int costo; //Costo del recorrido de un vertice a otro
	private Nodo<N,A> referencia; //Nodo hacia el que esta dirigido
	private A rute; // Informacion del Arco. En el caso del proyecto, contiene la informacion del Vuelo.
	
	
//<Constructores>
	public Arco(){
		costo = 0;
		referencia = null;
		rute = null;
	}
	
	public Arco(Nodo<N,A> e){
		costo = 0;
		referencia = e;
		rute = null;
	}
	
	public Arco(Nodo<N,A> e, int i){
		costo = i;
		referencia = e;
		rute = null;
	}
	
	public Arco(Nodo<N,A> e, int i, A s){
		costo = i;
		referencia = e;
		rute = s;
	}
//</constructores>	
	
//<get y set>
	//Setea el campo Referencia con el Nodo 'e'
	public void setRef(Nodo<N,A> e){
		referencia = e;
	}
	
	//Retorna el nodo al que esta dirigido
	public Nodo<N,A> getRef(){
		return referencia;
	}
	
	//Setea el costo del arco
	public void setCosto(int e){
		costo = e;
	}
	
	//Retorna el costo del arco
	public int getCosto(){
		return costo;
	}
	
	//Retorna el campo rute
	public A getRute(){
		return rute;
	}
//</get and set>

//<Redefinidas>
	//imprime la informacion del campo Rute
	public String toString(){
		return rute.toString();
	}
	
	//Equals(Object obj) para la clase Arco que compara en cuanto a el campo 'rute' y el campo 'costo'
	public boolean equals(Object obj) {
		if(obj instanceof Arco){
			if(rute != null && ((Arco<N,A>) obj).getRute() != null){
	    		return this.rute.equals(((Arco<N,A>)obj).getRute()) && this.costo == ((Arco<N,A>) obj).getCosto();
	        }
		}
	    return false;
	 }
	
	public int hashCode(){
		return rute.hashCode();
	}
	
	//Clona un Arco
	public Arco<N,A> clone(){
		return new Arco<N,A>(referencia, costo, rute);
	}

//</Redefinidas>
}