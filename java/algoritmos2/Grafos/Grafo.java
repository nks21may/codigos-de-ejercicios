/*Proyecto 2017 | Estructura de Datos y Algoritmos - Algoritmos I
 * Alumnos: - D'alessandro, Nicolas
 * 				D.N.I.: 41.105.833
 * 			- Garcia, Valeria
 * 				D.N.I.: 39.545.287
 */

package algoritmos2.Grafos;

import java.util.ArrayList;
import java.util.LinkedList;

import algoritmos2.extras.Heaps;

@SuppressWarnings("unchecked")
//Clase que representa la estructura de un grafo
public class Grafo<N,A>{
	private ArrayList<Nodo<N,A>> lista; //Lista de Vertices del Grafo
	private int cantArcos; //Cantidad de arcos del grafo
	
	public Grafo(){
		lista = new ArrayList<Nodo<N,A>>();
		cantArcos = 0;
	}
	
	/*isEmpty(): retorna true si el grafo esta vacio
	 * pre: true
	 * pos: retorna true si el grafo esta vacio
	 */
	public boolean isEmpty(){
		return lista.isEmpty();
	}
	
	/*size(): retorna la cantida de vertices que tiene el grafo
	 * pre: true
	 * pos: retorna el tamaño del grafo
	 */
	public int size(){
		return lista.size();
	}
	
	/*cantArcos(): retorna la cantida de arcos que tiene el grafo
	 * pre: true
	 * pos: retorna la cantidad de arcos que tiene el grafo
	 */
	public int cantArcos(){
		return cantArcos;
	}
	
	//Retorna la lista de vertices del grafo
	public ArrayList<Nodo<N,A>> getLista(){
		return lista;
	}
	
	/*get(int i): retorna el vertice en la posicion i
	 * pre: i>=0
	 * pos: retorna el vertice en la posicion i
	 */
	public Nodo<N,A> get(int i){
		if(0<=i && i<lista.size()){
			return lista.get(i);
		}
		return null;
	}
	
	/*indexOf(Nodo<N,A> n): retorna el indice del Nodo n. Si el Nodo n no existe, retorna -1.
	 * pre: true
	 * pos: retorna el indice del Nodo n. Si el Nodo n no existe, retorna -1.
	 */
	public int indexOf(Nodo<N,A> n){
		return lista.indexOf(n);
	}
	
	/*insertarNodo(Nodo<N,A> e): Inserta un nuevo vertice en el grafo.
	 * pre: true
	 * pos: Inserta un nuevo vertice en el grafo.
	 */
	public void insertarNodo(Nodo<N,A> e){
		if(!lista.contains(e)){
			lista.add(e);
		}else{
			System.out.println("Elemento no insertado");
		}
	}
	
	/*insertarNodo(N a): Inserta un nuevo vertice en el grafo.
	 * pre: true
	 * pos: Inserta un nuevo vertice en el grafo.
	 */
	public void insertarNodo(N a){
		Nodo<N,A> e = new Nodo<N,A>(a);
		if(!lista.contains(e)){
			lista.add(e);
		}else{
			System.out.println("Elemento no insertado");
		}
	}
	
	/*borrarNodo(int e): Borra un vertice del grafo.
	 * pre: true
	 * pos: Borra un vertice del grafo.
	 */
	public void borrarNodo(int e){
		Nodo<N,A> aux = lista.remove(e);
		if(aux != null){
			for(int i= 0; i< lista.size();i++){
				(lista.get(i)).removeAll(aux);
				cantArcos--;
			}
		}
	}
	
	/*insertarArco(Nodo<N,A> desde, Nodo<N,A> hacia, int costo, A rute): Crea un arco entre el Nodo 'desde' y el Nodo 'hacia', con 'costo' y en el campo Info la informacion de rute.
	 * pre: true
	 * pos: Crea un arco entre el Nodo 'desde' y el Nodo 'hacia', con 'costo' y en el campo Info la informacion de 'rute'
	 */
	public void insertarArco(Nodo<N,A> desde, Nodo<N,A> hacia, int costo, A rute){
		int salida = lista.indexOf(desde);
		int llegada = lista.indexOf(hacia);
		if(salida != -1 && llegada != -1){
			Arco<N,A> aux = new Arco<N,A>(lista.get(llegada), costo, rute);
			if(!(lista.get(salida)).contains(aux)){
				(lista.get(salida)).add(aux);
				cantArcos++;
			}
		}	
	}
	
	/*borrarArco(int i, Nodo<N,A> e): Borra un arco entre el Nodo de la posicion 'i' y el Nodo 'e'.
	 * pre: i<lista.size();
	 * pos: Borra un arco entre el Nodo de la posicion 'i' y el Nodo 'e'.
	 */
	public void borrarArco(int i, Nodo<N,A> e){	
		Arco<N,A> aux = new Arco<N,A>(e);
		if(lista.get(i).remove(aux)){
			cantArcos--;
		}
	}
	
	/*contains(Nodo<N,A> e): verifica si el Nodo e pertenece al grafo.
	 * pre: true
	 * pos:verifica si el Nodo e pertenece al grafo.
	 */
	public boolean contains(Nodo<N,A> e){
		return lista.contains(e);
	}
	
	/*contains(N e): verifica si el Nodo e pertenece al grafo.
	 * pre: true
	 * pos:verifica si el Nodo e pertenece al grafo.
	 */
	public boolean contains(N e){
		Nodo<N,A> aux = new Nodo<N,A>(e);
		return lista.contains(aux);
	}
	
	//muestra el camino mas corto entre 2 nodos
	public void shortPatch(Nodo<N,A> s, Nodo<N,A> l){
		if(s != l && lista.contains(s) && lista.contains(l)){
			int[] dist = new int[lista.size()];
			Nodo<N,A>[] ant = new Nodo[lista.size()]; //arreglo con el antecesor de los nodos
			Arco<N,A>[] arch = new Arco[lista.size()]; //arreglo con el arco de menos peso(acumulado) hacia el nodo
			String escala = "No";
			String fris = "Su consulta: "+"\nOrigen: " + s.toString() + " Destino: " + l.toString()+ "\n"; //Cadena principal
			
			dijkstra(s, ant, dist, arch);
			
			//mostrar
			String auxV = ""; //arreglo auxiliar para la organizacion de el String referido con los Arcos
			String auxE = ""; //arreglo auxiliar para la organizacion de el String referido con los tiempos
			Nodo<N,A> last = l; //nodo auxiliar para moverse dentro del arreglo de nodos
			int t = dist[lista.indexOf(l)]; //tiempo total de viaje
			int esc = 0;
			Arco<N,A> arcAux = null; //arco auxiliar para moverse entre el arreglo de Arcos
			//recorre desde el Nodo de salida para encontrar el camino. En caso de llegar a null, significa que no se pudo establecer un camino.
			while(last != null && !last.equals(s)){
				arcAux = arch[lista.indexOf(last)];
				if(arcAux != null && esc>0){
					auxE = "\n 	Duracion del vuelo: " + Time.timeTotal(arcAux.getCosto());
				}
				auxV = "\n" + arch[lista.indexOf(last)] + " " + auxE + " " +auxV;
				last = ant[lista.indexOf(last)];
				esc++;
			}
			if(last != null) {
				if(esc>1){ //en caso de tener escalas
					escala="Si";
					auxV += "\n	Duracion del vuelo: " + Time.timeTotal(arch[lista.indexOf(l)].getCosto()); //parcheo para primer caso
				}
				System.out.println(fris + "¿Hay escalas?: " + escala + "\n"+ "\n********************************************" + "\nVuelo/s a Tomar:" +auxV +"\n\n"+ "Tiempo total de su viaje(en vuelo): " + Time.timeTotal(t));
			}else{
				System.out.println("No se pudo encontrar una ruta");
			}
				
		}else{
			System.out.println("Ingrese 2 aeropuertos validos");
		} 
	}
	
	//Algoritmo de dijkstra.
	public void dijkstra(Nodo<N,A> n, Nodo<N,A>[] ant, int[] dist, Arco<N,A>[] awn){
		//inicializacion de los elementos
		for(int i = 0; i < lista.size();i++){
			dist[i] = Integer.MAX_VALUE; // la dist. mas corta el inf.
			awn[i] = null; 
			ant[i] = null;
		}
		dist[lista.indexOf(n)] = 0; // la dist. al origen es 0
		
		Heaps pendientes = new Heaps(); //Cola de prioridad
		pendientes.insertar(n, 0); //inserta arco ficticio
		
		Nodo<N,A> u;
		while (!pendientes.isEmpty()){
			u = (Nodo<N, A>) pendientes.poll(); // se saca el mas cercano
			u.marcar();
			for(Arco<N,A> y: (LinkedList<Arco<N,A>>) u.getLista()){
				if(!y.getRef().visit()){ //si el adyacente no fue visitado
					if(dist[lista.indexOf(u)] + y.getCosto() < dist[lista.indexOf(y.getRef())]){ //si encuentra un camino con menor costo hacia el nodo que apunta y, lo reemplaza
						dist[lista.indexOf(y.getRef())] = y.getCosto() + dist[lista.indexOf(u)]; //actualiza la distancia
						awn[lista.indexOf(y.getRef())] = y; //actualiza el arco por el cual llego
						ant[lista.indexOf(y.getRef())] = u; //actualiza el nodo anterior
						//y.getRef().desMarcar();
						pendientes.insertar(y.getRef(),dist[lista.indexOf(y.getRef())]); //actualiza la prioridad 
					}
				}
			}
		}
		for(int i = 0; i < lista.size(); i++){ //desmarca todos los nodos
			lista.get(i).desMarcar();
		}
	}
	
	//Imprime el grafo, cada vertice seguido de sus adyacentes.
	public String toString(){
		String ret = "";
		for (int i = 0; i < lista.size(); i++) {
			Nodo<N,A> aux =  lista.get(i);
			ret += aux.toStringRelaciones();
			ret += "\n";
		}
		ret += "\nCantidad de Vuelos: "+cantArcos;
		return ret;
	}
}