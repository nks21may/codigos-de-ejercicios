package algoritmos2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import org.jgrapht.graph.*;

public class Greedy{
	
	//3
	private ArrayList<Clase> ejercicioClases(ArrayList<Clase> c) {
		ArrayList<Clase> res = new ArrayList<Clase>();
		Collections.sort(c);
		int i = 0;
		Clase act;
		while(i < c.size()){
			act = c.get(i);
			res.add(act);
			while(act.getHf() > c.get(i).getHi() && i < c.size()){
				i++;
			}
		}
		return res;
	}
	
	DefaultDirectedWeightedGraph<String, DefaultWeightedEdge>  graph = new DefaultDirectedWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
	//5
	private DefaultDirectedWeightedGraph<String, DefaultWeightedEdge> prim(String x) {
		int cantVertex = graph.vertexSet().size();
		if (!graph.containsVertex(x)) {
			return null;
		}else{
			DefaultDirectedWeightedGraph<String, DefaultWeightedEdge>  newgraph = new DefaultDirectedWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);
			graph.vertexSet().stream().forEach((v) -> {
				                newgraph.addVertex(v);
			});
			PriorityQueue<DefaultWeightedEdge> cola = new PriorityQueue<DefaultWeightedEdge>((DefaultWeightedEdge a1, DefaultWeightedEdge a2) -> Double.compare(graph.getEdgeWeight(null),graph.getEdgeWeight(null)));
			HashMap<DefaultWeightedEdge, Boolean> visitado = new HashMap<DefaultWeightedEdge, Boolean>();
			int cont = 0;
			
			while(cont < cantVertex) {
				for(DefaultWeightedEdge z : graph.edgesOf(x)) 
					if (!visitado.containsKey(z)) 
						cola.offer(z);
				
				DefaultWeightedEdge arco = cola.poll();
				
				if (!visitado.containsKey(arco)){
					visitado.put(arco, true);
					newgraph.addEdge(graph.getEdgeSource(arco), graph.getEdgeTarget(arco));
					cont++;
					x = graph.getEdgeSource(arco);
				}
			}
			
			return newgraph;
		}
	}
	
	//6
	public static List<Ojeto> mochila(List<Ojeto> p, int w) {
		Collections.sort(p);
		List<Ojeto> res = new LinkedList<Ojeto>();
		int pt = 0;
		while(!p.isEmpty() && pt < w){
			Ojeto temp = p.get(0);
			if (pt + temp.peso < w) {
				res.add(temp);
				p.remove(0);
				pt+=temp.peso;
			}else {
				p.remove(0);
			}
		}
		return res;
	}
	
    public static void main(String[] args){

    }
	
}
