public interface Estados{

	abstract boolean equals(Object otro);

	abstract void setPadre(Estados e);

	abstract Estados getPadre();

	abstract String toString();
}
