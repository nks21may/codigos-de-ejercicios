package algoritmos2;

class UtilidadesGrales{
	
	public static int mergeSort(Integer[] array, int begin, int end){
		int res = 0;
		if (begin < end){
			int mid = (begin + end)/2;
			res += mergeSort(array, begin, mid);//ordena la primera mitad
			res += mergeSort(array, mid+1, end);//ordena la segunda mitad
			res += merge(array, begin, mid, end);//mezcla las mitades ordenadas
		}
		return res;
	}
	
	// merge: mezcla dos partes consecutivas de array
	// pre: 0 <= begin, mid, end <= array.lenght
	private static int merge(Integer[] array, int begin, int mid, int end){
		int i = begin;
		int j = mid+1;
		int k = 0;
		int[] aux = new int[end-begin+1];
		int res = 0;
		
		while(i <= mid && j <= end){
			if(array[i] < array[j]){
				aux[k] = array[i];
				i++;
			}else{
				aux[k] = array[j];
				j++;
				res += (mid-i)+1;
			}
			k++;
		}
		while(i <= mid){
			aux[k] = array[i];
			i++; k++;
			//res += 1;
		}
		while(j <= end){
			aux[k] = array[j];
			j++; k++;
			
		}
		for (k=0; k < end-begin+1; k++){
			array[begin+k] = aux[k];
		}
		return res;
	}
	
	public static void reverseArray(Object[] validData) {
		for(int i = 0; i < validData.length / 2; i++){
		    Object temp = validData[i];
		    validData[i] = validData[validData.length - i - 1];
		    validData[validData.length - i - 1] = temp;
		}
	}
}