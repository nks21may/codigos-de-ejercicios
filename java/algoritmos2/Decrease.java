package algoritmos2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collection;


public class Decrease{
	public static void main(String[] args){
		System.out.println(pro(5,4,0)[0]);
	}
	//1
	public static int[] pro(int n, int m, int k){
		int[] res = {m,k};
		if(n <= 1){
			return res;
		}
		if (n == 2) {
			res[0] = m+m;
			res[1]++;
			return res;
		}

		if(n>1){
			if(n%2 == 0){
				res[0] = n/2 + 2*m;
				res[1]++;
				return res;
			}else{
				res[0] = pro(((n-1)/2), 2*m, k)[0] + m;
				res[1] += 2; 
			}
		}
		return res;
	}

	//2
	public static int acum(int[] a){
		int res = 0;
		for (int x: a) {
			res+=x;
		}
		return res;
	}

	public static int[] balanza(int[] a, int[] b, int[] c){
		int p1a = acum(a);
		int p2a = acum(b);
		if (p1a < p2a){
			return a;
		}
		if (p1a > p2a){
			return b;
		}
		return c;
	}

	public static int falsaMoneda(int[] a){
		if(a.length > 1){
			int[] p1;
			int[] p2;
			int[] p3;
			if(a.length%3 == 0){
				int l = a.length/3;
				p1 = new int[l];
				p2 = new int[l];
				p3 = new int[l];
				System.arraycopy(a,0,p1,0,l);
				System.arraycopy(a,l,p2,0,l);
				System.arraycopy(a,(l*2),p3,0,l);
				return falsaMoneda(balanza(p1,p2,p3));
			}
			if(a.length%2 == 0){
				int l = a.length/2;
				p1 = new int[l];
				p2 = new int[l];
				System.arraycopy(a,0,p1,0,l);
				System.arraycopy(a,l,p2,0,l);
				return falsaMoneda(balanza(p1,p2,null));

			}else{
				p3 = new int[1];
				p3[0] = a[a.length-1];
				int l = (a.length-1)/2;
				p1 = new int[l];
				p2 = new int[l];
				System.arraycopy(a,0,p1,0,l);
				System.arraycopy(a,l,p2,0,l);

				return falsaMoneda(balanza(p1,p2,p3));
			}
		}else{
			return a[0];
		}
	}
	//5)
	
	public static HashMap<Integer,ArrayList<Integer>> sinAristas(HashMap<Integer,ArrayList<Integer>> mapa) {
		HashMap<Integer,ArrayList<Integer>> res = new HashMap<Integer,ArrayList<Integer>>();
		res.putAll(mapa);
		Collection<ArrayList<Integer>> s = mapa.values();
		for (ArrayList<Integer> array : s) {
			if (array != null) {
				for (Integer i : array) {
					res.remove(i);
				}
			}
		}
		return res;
	}
	
	public static HashMap<Integer,ArrayList<Integer>> reversor(HashMap<Integer,ArrayList<Integer>> mapa) {
		HashMap<Integer,ArrayList<Integer>> res = new HashMap<Integer,ArrayList<Integer>>();
		for (Integer i: mapa.keySet()) {
			ArrayList<Integer> v = mapa.get(i);
			if(v != null){
				for (Integer j : v) {
					if (res.containsKey(j)) {
						ArrayList<Integer> z = res.get(j);
						z.add(i);
						res.put(j, z);
					}else{
						ArrayList<Integer> z = new ArrayList<Integer>();
						z.add(i);
						res.put(j, z);
					}
				}
			}
		}
		return res;
	}
	
	public static boolean asiclo(HashMap<Integer,ArrayList<Integer>> mapa){
		HashMap<Integer,ArrayList<Integer>> porAnalizar = sinAristas(mapa);
		HashMap<Integer,Integer> analizado = new HashMap<Integer,Integer>(); 
		ArrayList<Integer> s = new ArrayList<Integer>();
		boolean res = false;
		s.addAll(porAnalizar.keySet());
		while(!s.isEmpty() && !res){
			Integer n = s.get(0);
			s.remove(0);
			ArrayList<Integer> g = porAnalizar.get(n);
			if(g != null){
				boolean patchet = false;
				while(!g.isEmpty() && !res && !patchet){
					Integer e = g.get(0);
					g.remove(0);

					if(analizado.containsKey(e)){
						if(analizado.get(e) == n){
							System.out.println(analizado.get(e));
							res = true;	
						}else{
							patchet = true;
						}

					}else{
						analizado.put(e, n);
						if(mapa.get(e) != null) {
							g.addAll(mapa.get(e));
						}
					}
				}	
			}
		}
		return res;
	}

	public static int distancia(int[] a, int[] b){
		return Math.abs(a[0]-b[0]) + Math.abs(a[1]-b[1]);
	}

	//6 
	public static int[][] distanciaMenor(int[][] p){
		int[][] res = {{Integer.MAX_VALUE/2,Integer.MAX_VALUE/2},{Integer.MIN_VALUE/2,Integer.MIN_VALUE/2}};
		boolean casos = false;
		switch (p.length){
			case 0: casos = true;
				return res;
			case 1: casos = true; 
				return res;
			case 2:	casos = true;
				res = p;
				break;
		}
		if(!casos){
			int l = p.length/2;
			int[][] dLMin = new int[l][2];
			int[][] dRMin = new int[l][2];
			System.arraycopy(p,0,dLMin,0,l);
			System.arraycopy(p,l,dRMin,0,l);
			dLMin = distanciaMenor(dLMin);
			dRMin = distanciaMenor(dRMin);
			
			int[][] dLRMin;
			if(distancia(dLMin[0],dLMin[1]) < distancia(dRMin[0], dRMin[1])){
				dLRMin = dLMin;
			}else{
				dLRMin = dRMin;
			}
			int d = distancia(dLRMin[0],dLRMin[1]);
			int m = (p[l][0] + p[l+1][0])/2;
			
			int pi = l*2-1;
			int b = 0;
			while(pi != b){
				if(p[(pi+b)/2][0] < m-d){
					b = (pi+b)/2+1;
				}else {
					pi = (pi+b)/2;
				}
			}
			b = 0;
			int pd = l*2-1; 
			while(pd != b){
				if(p[(pd+b)/2][0] < m+d){
					b = 1+(pd+b)/2;
				}else {
					pd = (pd+b)/2;
				}
			}
			int[][] dMMin = new int[pd-pi][2];
			System.arraycopy(p,pi,dMMin,0,pd-pi);
			dMMin = distanciaMenor(dMMin);
			if(distancia(dMMin[0],dMMin[1]) < distancia(dLRMin[0], dLRMin[1])){
				res = dMMin;
			}else{
				res = dLRMin;
			}
		}
		return res;
	}

	//adicionales/robados

	    public static void merge(int[][] arr, int l, int m, int r){
	    int i, j, k;
	    int n1 = m - l + 1;
	    int n2 =  r - m;
	 
	    int[][] L = new int[n1][2];
	    int[][] R = new int[n2][2];
	 
	    for (i = 0; i < n1; i++)
	        L[i] = arr[l + i];
	    for (j = 0; j < n2; j++)
	        R[j] = arr[m + 1+ j];
	 
	    i = 0; // Initial index of first subarray
	    j = 0; // Initial index of second subarray
	    k = l; // Initial index of merged subarray
	    while (i < n1 && j < n2){
	        if (L[i][0] <= R[j][0]){
	            arr[k] = L[i];
	            i++;
	        }
	        else{
	            arr[k] = R[j];
	            j++;
	        }
	        k++;
	    }
	 
	    while (i < n1){
	        arr[k] = L[i];
	        i++;
	        k++;
	    }
	 
	    while (j < n2){
	        arr[k] = R[j];
	        j++;
	        k++;
	    }
	}
	 
	public static void mergeSort(int[][] arr, int l, int r){
	    if (l < r){
	        int m = l+(r-l)/2;
	 		mergeSort(arr, l, m);
	        mergeSort(arr, m+1, r);
	        merge(arr, l, m, r);
	    }
	}
}
