package algoritmos2;

class subsec{
    
    //res[0] indice left; res[1] indice right; res[2] cantidad
    //pre a.length ==3 == b.length;
	public static int[] min3(int[] a,int[] b){
		if(a[2] < b[2]){
			return a;
		}else{
			return b;
		}
	}

	public static int[] minSuc(int l, int r, int[] arr){
		int[] res = new int[3];
		if(l != r){
			int[] secLef = minSuc(l, ((r+l)/2), arr);
			int[] secRig = minSuc(((r+l)/2)+1, r, arr);
			int[] mid = new int[3];
			mid[0] = l;
			mid[1] = r;
			mid[2] = Integer.MAX_VALUE;
			int acum = 0;
			for(int i = ((r+l)/2); i>=l ;i--){
				acum += arr[i];
				if(acum < mid[2]){
					mid[2] = acum;
					mid[0] = i;
				}
			}
			acum = mid[2];
			for(int i = ((r+l)/2)+1; i<=r ;i++){
				acum += arr[i];
				if(acum < mid[2]){
					mid[2] = acum;
					mid[1] = i;
				}
			}
			res = min3(mid, min3(secLef,secRig));

		}else{
			res[0] = l;
			res[1] = r;
			res[2] = arr[l];
		}
		return res;
	}

}

class TheMatrix{

	public static boolean esPotencia(int x){
		boolean res = false;
		for(int i = 1; i <= x && !res; i*=2){
			res = i == x;   
		}
		return res;
	}

	public static Integer[][] suma(Integer[][] x, Integer[][] y){
		Integer[][] res = new Integer[x.length][x[0].length];
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[0].length; j++){
				res[i][j] = x[i][j] +  y[i][j];
			}
		}
		return res;
	}
	
	public static Integer[][] resta(Integer[][] x, Integer[][] y){
		Integer[][] res = new Integer[x.length][x[0].length];
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[0].length; j++){
				res[i][j] = x[i][j] -  y[i][j];
			}
		}
		return res;
	}
	
	
	//el i es el indice del cuarto que quiere
	public static Integer[][] cuarter(Integer[][] a, int n){
		Integer[][] res = new Integer[a.length/2][a[0].length/2];
		int i = 0;
		int j = 0;
		int il = 0;
		int jl = 0;
		int l = a.length/2;
		switch(n) {
			case 0:
				i = 0;
				j = 0;
				il = l;
				jl = l;
				break;
			case 1:  
				i = 0;
				j = l;
				il = l;
				jl = a.length;
				break;
			case 2:  
				i = l;
				j = 0;
				il = a.length;
				jl = l;
				break;
			case 3:  
				i = l;
				j = l;
				il = a.length;
				jl = a.length;
				break;
		}
		int l1 = 0;
		for (int k = i; k < il; k++) {
			int l2 = 0;
			for (int k2 = j; k2 < jl; k2++) {
				res[l1][l2] = a[k][k2]; 
				l2++;
			}
			l1++;
		}
		return res;
	}
	
	public static Integer[][] union(Integer[][] w, Integer[][] x, Integer[][] y, Integer[][] z){
		Integer[][] res = new Integer[x.length*2][x[0].length*2];
		int i = 0;
		int j = 0;
		Integer[][][] r = {w,x,y,z};
		int l = x.length;
		int f = 0;
		for (Integer[][] a : r) {
			switch(f){
			case 0:
				j = 0;
				break;
			case 1:  
				j = l;
				break;
			case 2:  
				j = 0;
				break;
			case 3:  
				j = l;
				break;
			}
			for (int k = 0; k < a.length; k++) {
				switch(f) {
				case 0:
					i = 0;
					break;
				case 1:  
					i = 0;
					break;
				case 2:  
					i = l;
					break;
				case 3:  
					i = l;
					break;
				}
				for (int k2 = 0; k2 < a[0].length; k2++) {
					res[i][j] = a[k2][k];
					i++;
				}
				j++;
			}
			f++;
		}
		return res;
	}

	public static Integer[][] strassen(Integer[][] a, Integer[][] b){
		Integer[][] res = null;
		if(a.length == b.length && a[0].length == a.length && a[0].length == b[0].length && esPotencia(a.length)){
			if(a.length == 1){
				Integer[][] r = {{a[0][0]*b[0][0]}};
				res = r;
			}else{
				Integer[][] m1,m2,m3,m4,m5,m6,m7,a11,a12,a21,a22,b11,b12,b21,b22,c11,c12,c21,c22;
				a11 = cuarter(a, 0);
				a12 = cuarter(a, 1);
				a21 = cuarter(a, 2);
				a22 = cuarter(a, 3);
				b11 = cuarter(b, 0);
				b12 = cuarter(b, 1);
				b21 = cuarter(b, 2);
				b22 = cuarter(b, 3);
				m1 =(strassen(suma(a11,a22),suma(b11,b22)));
				m2 = strassen(suma(a21,a22),b11);
				m3 = strassen(a11,resta(b12, b22));
				m4 = strassen(a22, resta(b21, b11));
				m5 = strassen(suma(a11, a12), b22);
				m6 = strassen(resta(a21,a11),suma(b11, b12));
				m7 = strassen(resta(a12, a22), suma(b21, b22));
				c11 = suma(suma(m1, m7), resta(m4, m5));
				c12 = suma(m3, m5);
				c21 = suma(m2, m4);
				c22 = suma(suma(resta(m1, m2),m3),m6);
				res = union(c11, c12, c21, c22);
			}
		}else {
			System.out.println("fail");
		}
		return res;
	}
}



public class Divide{
	
	/*private static void imprimirMatriz(Integer[][] x){
		for (Integer[] a : x) {
			for (Integer e : a) {
				System.out.print(e);
				System.out.print(" ");
			}
			System.out.println();
		}
		System.out.println();
	}*/
	
	public static void main(String[] args) {
        int[] x = {6,8,5,7,3};
        for (Integer i : x) {
			System.out.print(i+" ");
		}
        System.out.println();
        
        System.out.println(UtilidadesGrales.mergeSort(x, 0, x.length-1));
    }
}
