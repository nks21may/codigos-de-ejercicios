package algoritmos2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

class Pair{
	public int a,b;
	
	public Pair(int x, int y) {
		a = x;
		b = y;
	}
	
	public int getA(){
		return a;
	}
	
	public int getB(){
		return b;
	}
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (other instanceof Pair) return (this.a == ((Pair) other).getA() && this.b == ((Pair) other).getB());
		return false;
	}
	
	public boolean compatible(Pair x) {
		return x.getA() == this.getA() || x.getA() == this.getB() || x.getB() == this.getA() || x.getB() == this.getB();
	}
	
	public Pair compatibleR(Pair b) {
		Pair res = null;
		if (this.getA() == b.getA()){
			res = new Pair(this.getB(), b.getB());
		}
		if (this.getA() == b.getB()){
			res = new Pair(this.getB(), b.getA());
		} 
		if (this.getB() == b.getA()){
			res = new Pair(this.getA(), b.getB());
		} 
		if (this.getB() == b.getB()){
			res = new Pair(this.getA(), b.getA());
		} 
		return res;
	}
	
	@Override
    public int hashCode(){
        int result = a;
        result = 31 * result + b;
        return result;
    }
}


class Memorization{
	
	//2
	public static int combinatorio(int n, int k, HashMap<Pair,Integer> mapa){
		int res = -1;
		if(n>k) {
			if(n != 0 && k != 0) {
				if (mapa.containsKey(new Pair(n,k))) {
					res = mapa.get(new Pair(n,k));
				}else{
					res = combinatorio(n-1, k-1, mapa) + combinatorio(n-1, k, mapa);
					mapa.put(new Pair(n, k),res);
				}
			}else {
				if (n == 0){
					res = 0;
				}
				if(k == 0){
					res = 1;
				}
			}
		}
		if(n==k){
			res = 1;
		}
		return res;
	}
	
	public static int fibonachi(int n, HashMap<Integer,Integer> mapa){
		if (n <= 0){
			return 0;
		}
		if (n == 1){
			return 1;
		}
		if (mapa.containsKey(n)) {
			return mapa.get(n);
		}else{
			int res = fibonachi(n-1, mapa) + fibonachi(n-2, mapa);
			mapa.put(n, res);
			return res;
		}
	}
	
	public static int costoCalcu(Pair a, Pair b) {
		int res = Integer.MAX_VALUE;
		if (a.getA() == b.getA()){
			res = a.getA() * a.getB() * b.getB();
		}
		if (a.getA() == b.getB()){
			res = a.getA() * a.getB() * b.getA();
		} 
		if (a.getB() == b.getA()){
			res = a.getA() * a.getB() * b.getB();
		} 
		if (a.getB() == b.getB()){
			res = a.getA() * a.getB() * b.getA();
		} 
		
		return res;
	}
	
	public static Integer[][] calculoMultMatrices(ArrayList<Pair> a){
		Integer[][] res = new Integer[a.size()][a.size()];
		for (int i = 0; i < res.length; i++) {
			for (int j = i+1; j < res[0].length; j++) {
				int r = costoCalcu(a.get(i), a.get(j));
				res[i][j] = r;
				res[j][i] = r;
			}
		}
		
		return res;
	}
	
	public static ArrayList<Pair> ejercicioMatrices(){
		return null; 
	}
	
	public static void mochila() {
		
	}
	
	public static void main(String[] par){
		System.out.println(fibonachi(8, new HashMap<Integer,Integer>()));
	}	
}