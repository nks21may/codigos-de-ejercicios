package algoritmos2;

public class Ojeto implements Comparable<Ojeto>{ 
    int peso; 
    int valor; 
    double ratio; 
     
    public Ojeto(){ 
        peso = 0; 
        valor = 0; 
        ratio = -1; 
    } 
     
    public Ojeto(int peso, int valor){ 
        this.peso = peso; 
        this.valor = valor; 
        if(peso > 0){ 
            ratio = this.valor / this.peso; 
        }else{ 
            ratio = -1; 
        } 
    } 
 
    @Override 
    public int compareTo(Ojeto o) { 
        int res = 0; 
        if((this.ratio > o.ratio) || 
            ((this.ratio == o.ratio) && (this.peso < o.peso))){ 
            res = -1; 
        }else if((this.ratio < o.ratio) || 
                ((this.ratio == o.ratio) && (this.valor > o.valor)) || 
                ((this.ratio == o.ratio) && (this.valor == o.valor) && 
                        (this.peso < o.peso))){ 
            res = 1; 
        } 
        return res; 
    } 
     
    public String toString(){ 
        return "(" + peso + " , " + valor + " , " + ratio + ")"; 
    } 
} 