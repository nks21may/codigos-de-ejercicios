package algoritmos2;

import java.util.ArrayList;
import java.util.List;

class Main{
	
	public static List<List<Integer>> name() { //  send -> 9567, more -> 1085, money -> 10652
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		for (int i = 0; i < 10; i++) { //s
			for (int j = 0; j < 10; j++) { //e
				for (int j2 = 0; j2 < 10; j2++) {//n
					for (int k = 0; k < 10; k++) {//d
						for (int k2 = 0; k2 < 10; k2++) {//m
							for (int l = 0; l < 10; l++) {//o
								for (int l2 = 0; l2 < 10; l2++) {//r
									for (int m = 0; m < 10; m++) {//y
										if (!(i == j || i == j2 || i == k || i == k2 || i == l || i == l2 || j == j2 || j == k || j == k2 || j == l || j == l2 || j2 == k || j2 == k2  || j2 == l || j2 == l2 || k == k2  || k == l || k == l2 || k2 == l || k2 == l2 || l == l2 || m == i || m == j || m == j2 || m == k || m == k2 || m == l || m == l2 )){
											int o = i*1000+j*100+j2*10+k; //send
											int p = k2*1000+l*100+l2*10+j; //more
											int po = k2*10000+l*1000+j2*100+j*10+m; //money
											if(o+p==po){
												ArrayList<Integer> a = new ArrayList<Integer>();
												a.add(o);
												a.add(p);
												a.add(po);
												res.add(a);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return res;
	}
	
	public static void main(String[] par){
		List<List<Integer>> a = name();
		for (int i = 0; i < a.size(); i++){
			List<Integer> b = a.get(i); 
			for (int j = 0; j < b.size();j++){
				System.out.print(b.get(j)+" ");
			}
			System.out.println();
		}
	}
	
}