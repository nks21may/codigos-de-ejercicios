import java.util.Collections;
import java.util.Comparator;

import javafx.util.*;

public class Ejercicio1{
	
	public static void reverseArray(Object[] validData) {
		for(int i = 0; i < validData.length / 2; i++){
		    Object temp = validData[i];
		    validData[i] = validData[validData.length - i - 1];
		    validData[validData.length - i - 1] = temp;
		}
	}
	
	//<Costo, cant>
	//supongo que las flores estan ordenadas de mayor a menor segun el costo de las mismas (xq no arranca el collections.sort)
	@SuppressWarnings("restriction")
	public static int floreria(int k, Pair<Integer, Integer>[] flores){
		Pair<Integer, Integer> x;
		/*Comparator<Pair<Integer,Integer>> compa = new Comparator<Pair<Integer,Integer>>(){
            public int compare(Pair<Integer,Integer> p1, Pair<Integer,Integer> p2){
            	return p1.getKey().compareTo(p2.getKey());
            }};
		Collections.sort(flores, compa);*/
		int i = 0;
		int p = k;
		int v = 0;
		int costo = 0;
		while(i < flores.length){
			while(flores[i].getValue() > 0) {
				costo += flores[i].getKey() * (1+v);
				flores[i] = new Pair<Integer,Integer>(new Integer(flores[i].getKey()), new Integer(flores[i].getValue() -1));
				p--;
				if (p == 0) {
					p = k;
					v++;
				}
			}
			i++;
		}
		
		return costo;
	}
	
    public static void main(String[] args){

    }
	
}
