package algoritmos2;

class Clase implements Comparable<Clase>{

	private String id;
	private int hi;
	private int hf;

	public Clase(String idn, int hin, int hfn){
		id = idn;
		hi = hin;
		hf = hfn;
	}

	public String getId(){
		return id;
	}

	public void setId(String i){
		id = i;
	}

	public int getHi(){
		return hi;
	}

	public void setHi(int i){
		hi = i;
	}

	public int getHf(){
		return hf;
	}

	public void setHf(int i){
		hf = i;
	}

	public Integer duracion(){
		return hf - hi;
	}

	public int compareTo(Clase c) {
		if(new Integer(hi).compareTo(((Clase) c).getHi()) != 0) return (new Integer(hi).compareTo(c.getHi()));  
	    return this.duracion().compareTo(((Clase) c).duracion());
    }
	
	public int compare(Clase a, Clase b){
        return  a.compareTo(b);
    }
}