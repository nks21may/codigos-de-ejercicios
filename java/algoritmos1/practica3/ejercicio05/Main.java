package practica3.ejercicio05;

public class Main{
	
	public static boolean guido(int[] arreg){
		int i = 0;
		int j = 0;
		boolean result = false;
		while (i< arreg.length && !result){
			j = i;
			
			while(j+1<arreg.length){
				j++;
				if(arreg[i] == arreg[j]){
					result = true; 
				}
				
			}
			i++;
		}
		return result;
	}
	
	public static void main(String[] args){
		int[] arr = new int[args.length];
		for(int i = 0; i<args.length; i++){
			arr[i] = Integer.parseInt(args[i]);
		}
		System.out.println(guido(arr));
	}
}