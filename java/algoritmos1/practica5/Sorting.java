package practica5;
import java.util.*;

import practica6.ABB;
import practica6.Heaps;

/*Clase Sorting que provee metodos de ordenacion*/

@SuppressWarnings({ "rawtypes", "unchecked" })
public class Sorting{
	// Swap: intercambia dos posiciones de un array
	// pre:  0 <= i,j <= array.lenght
	// post: intercambia los valores
	static void swap(Comparable[] array, int i, int j){
	  Comparable temp = array[i];
  	  array[i] = array[j];
	  array[j] = temp;
 	}//end swap
 	
 	// bubbleSort: Implementa el algoritmo de sorting bubbleSort
 	// pre: n = array.lenght
 	// post: ordena el arreglo de menor a mayor
	public static void bubbleSort(Comparable[] array, int n){
	  boolean sorted = false;
	  for (int pass = 1; (pass < n)&& !sorted; ++pass){
	    sorted = true;
	    for (int index = 0; index < n - pass; ++index){
	    int nextIndex = index + 1;
	    if (array[index].compareTo(array[nextIndex])>0){
		  swap(array, index, nextIndex);
		  sorted = false;
	    } // end if
	    }//end for
	  }//end for
	}// end bubbleSort	
	
	// selectionSort: Implementa el algortimo de selectionSort
	// pre: n = array.lenght
	// post: ordena el arreglo de menor a mayor
	public static void selectionSort(Comparable[] array, int n){
	// last: index del ultimo elemento de la parte no ordenada
    // largest: posicion del elemento mas grande
		for (int last = n-1; last >= 1; last--){
	    	int largest = indexOfLargest(array, last+1);
	    	swap(array, last, largest);
    	}// end for
	}// end selectionSort
	
	// indexOfLargest: Metodo Privado que retorna el indice del elemento mas grande
	// hasta n
	// pre: n < array.lenght
	// post: devuelve el indice del mas grande hasta n
	private static int indexOfLargest(Comparable[] array, int n){
		int largest = 0;
		for (int i = 1; i < n; i++){
			if (array[i].compareTo(array[largest]) > 0){
				largest = i;
			}
		} //end for 	
		return largest;
	}// end indexOfLargest
	
	// mergeSort: implementa el algoritmo MergeSort
	// pre: 0 <= begin <= end <= array.lenght
	// post: ordena array.  
	public static void mergeSort(Comparable[] array, int begin, int end){
		if (begin < end){
			int mid = (begin + end)/2;
			mergeSort(array, begin, mid);//ordena la primera mitad
			mergeSort(array, mid+1, end);//ordena la segunda mitad
			merge(array, begin, mid, end);//mezcla las mitades ordenadas
		}
	}
	
	// merge: mezcla dos partes consecutivas de array
	// pre: 0 <= begin, mid, end <= array.lenght
	private static void merge(Comparable[] array, int begin, int mid, int end){
		int i = begin;
		int j = mid+1;
		int k = 0;
		Comparable[] aux = new Comparable[end-begin+1];
		
		while(i <= mid && j <= end){
			if(array[i].compareTo(array[j])<= 0){
				aux[k] = array[i];
				i++;
			}else{
				aux[k] = array[j];
				j++;
			}
			k++;
		}
		while(i <= mid){
			aux[k] = array[i];
			i++; k++;
		}
		while(j <= end){
			aux[k] = array[j];
			j++; k++;
		}
		
		for (k=0; k < end-begin+1; k++){
			array[begin+k] = aux[k];
		}
	}
	
	
	// quickSort: implementa el algoritmo de quicksort
	// pre: 0 <= begin <= end <= array.lenght
	// post: ordena array
	public static void quickSort(Comparable[] array, int begin, int end){
	if (begin < end){
		// Calculo la particion 
		int p = partition(array, begin, end);
		// ordeno la parte izq
		quickSort(array, begin, p);
		// ordeno la parte derecha
		quickSort(array, p+1, end);
	}
	}	
	
	// partition: dado un arreglo retorna un p, el cual es el pivote.
	// pre: 0 <= begin <= end <= array.lenght
	// post: para todo begin <= k <= p: array[k] <= array[p] y para todo k: p <= k <= end : array[p] <= array[k]
	private static int partition(Comparable[] array, int begin, int end){
		Random generathor = new Random();
		Comparable pivot = array[begin+generathor.nextInt(end-begin+1)];
		int i = begin - 1;
		int j = end + 1;
		while (i < j) {
			//invariante: 
			//para k < = i : a[k] <= pivot y para k >= j : pivot <= a[k] 
			do j--; while (array[j].compareTo(pivot) > 0); 
			do i++; while (array[i].compareTo(pivot) < 0);
			if (i < j) {swap(array, i, j);}	
		}
		return j;
	}
	
	public static void insertionSort (Comparable [] array , int n){
		for ( int unsorted = 1; unsorted < n; unsorted++){
		// array [0.. unsorted -1] esta ordenado
			Comparable nextItem = array [ unsorted ];
			int loc = unsorted ;
			while ((loc > 0) && (array[loc-1].compareTo(nextItem) > 0)){
				array[loc] = array[loc-1];
				loc--;
			}//end while
			array [loc] = nextItem ;
		}//end for
	}
	
	public static int max(Integer[] a){
		Integer aux = a[0];
		for(int i = 0; i<= a.length-1; i++){
			if(aux.intValue() < a[i].intValue()){
				aux = a[i];
			}
		}
		return aux.intValue();
	}
	
	private static int min(Integer[] a){
		Integer aux = a[0];
		for(int i = 0; i<= a.length-1; i++){
			if(aux.intValue() > a[i].intValue()){
				aux = a[i];
			}
		}
		return aux.intValue();
	}
	
	public static void countingSort(Integer[] array){
		if(array.length != 0){
			int max = max(array);
			int min = min(array);
			int[] arrux = new int[(max+1)-(min)];
			
			for(int i=0; i<array.length; i++){
				arrux[array[i]-min]++;
			}
			int k = 0;
			
			for(int i = 0; i<array.length; i++){
				if(arrux[k]!=0){
					array[i] = min+k;
					arrux[k]--;
				}else{
					k++;
					i--;
				}
			}
		}
	}
	
	public static void radixSort(Integer[] array){
		int max = String.valueOf(max(array)).length();
		for(int i = 1; i <= max; i++){
			countingSort(array,i);
		}
	}

    //n es el numero a analizar y p es la posicion de la que se espera obtener el numero
	//devuelve el numero de la posicion ingresada
    public static int sep(Integer n, int p){
    	char[] e = n.toString().toCharArray();
    	if(e.length-p >= 0){
    		return (Character.getNumericValue(e[e.length-p]));
    	}else{
    		return 0;
    	}
    }
    
    public static void countingSort(Integer[] array, int p){
    	if(array.length != 0){
			int[] cont = new int[10]; //cantidad de valores que puede llegar a tomar
			Integer[] out = new Integer[array.length];
			
			//contador de apariciones
			for(int i=0; i<array.length; i++){
				int dig = sep(array[i],p);
				cont[dig]++;
			}
			
			//acumulada
			for(int i = 1; i<cont.length; i++){
				cont[i] += cont[i-1];
			}
			
			//se arranca desde atras para adelante para no perder el orden que ya tenia
			for(int i = array.length-1; i>=0; i--){
				int dig = sep(array[i],p);
				
				out[cont[dig]-1] = array[i];
				cont[dig]--;
			}
			//se copia el resultado al arreglo original
			for(int i = 0; i< array.length; i++){
				array[i] = out[i];
			}
		}
	}
    
	public static void shellSort(Comparable[] arreglo){
    	for(int i = arreglo.length/2; i>0;i = (i/2)){
    		partido(arreglo, i);
    	}
    }
    
    //visualgo.net
	
	public static void partido(Comparable[] arr,int p){
    	for(int i = 0; i<p;i++){
    		Comparable[] ele = new Comparable[(arr.length/p + arr.length%p)];
    		for(int j = 0; (p*j)+i< arr.length;j++){ //genera el arreglo
    			ele[j] = arr[(p*j)+i];
    		}
    		insertionSort(ele, ele.length - arr.length%p); //lo ordena
    		for(int j = 0; (p*j)+i< arr.length;j++){ //lo vuelve a pisar
    			arr[(p*j)+i] = ele[j]; 
    		}
    	}
    }
	
	private static int repArr(ArrayList<Comparable> rep, Comparable e){
		int res = -3;
		if(rep.contains(e)){
			res = 1;
			rep.remove(e);
		}
		return res;
	}
	
	public static void TreeSort(Comparable[] arr){
		ABB arbol = new ABB();
		ArrayList <Comparable> rep = new ArrayList();
		for(int i=0; i< arr.length;i++){
			if(arbol.insertar(arr[i]) == -1){
				rep.add(arr[i]);
			}
		}
		Comparable[] arrux = arbol.returnInOrden();
		int j = 0;
		int l = 0; //pos del rep
		for(int i=0; i<arr.length; i++){
			if(j < arrux.length){
				l = repArr(rep, arrux[j]);
			}
			if(l != -3){
				arr[i] = arrux[j];
				l = -3;
			}else{
				arr[i] = arrux[j];
				j++;
			}
		}
		
	}
	
	public static void HeapSort(Comparable[] arr){
		Heaps hip = new Heaps();
		ArrayList <Comparable> rep = new ArrayList();
		
		for(int i = 0;i < arr.length;i++){
			if(!hip.insertar(arr[i])){
				rep.add(arr[i]);
			}
		}
		Comparable aux;
		for(int i = 0;i < arr.length;i++){
			aux = hip.first();
			if(!rep.remove(aux)){
				hip.borrar();
			}
			arr[i] = aux; 
		}
	}
}