package practica2.ejercicio12;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio12{
	
	public static String union(String[] a){
		String aux = "";
		for(int i = 0; i<a.length;i++){
			aux = aux +a[i];
		}
		return aux;
	}
	
	 public static String soloVocales(String s) {
		    Pattern pattern = Pattern.compile("[^a-z A-Z]");
		    Matcher matcher = pattern.matcher(s);
		    String number = matcher.replaceAll("");
		    return number;
	 }
	 
	 public static String reverse(String input){
		    char[] in = input.toCharArray();
		    int comienzo=0;
		    int termina=in.length-1;
		    char temp;
		    while(termina>comienzo){
		        temp = in[comienzo];
		        in[comienzo]=in[termina];
		        in[termina] = temp;
		        termina--;
		        comienzo++;
		    }
		    return new String(in);
		}
		
	public static boolean main(String[] args){
		if (args.length == 0){
			System.out.println("Faltan parametros. Uso ejercicio12 <cadena> [cadena] [cadena]");
			return false;
		}else{
			String todo = soloVocales(union(args));
			todo = todo.replaceAll(" ", "");
			todo = todo.toLowerCase();
			
			return (todo.equals(reverse(todo)));
		}
	}
	
}