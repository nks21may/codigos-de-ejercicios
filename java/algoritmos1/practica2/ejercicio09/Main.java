/**
 * 
 */
package practica2.ejercicio09;

/**
 * @author Nick
 *
 */
public class Main{
	
	public static boolean balanceada(String s){
		
		PilaPunt p = new PilaPunt();
		int i = 0;
		boolean b = true;
		
		while(i < s.length()){
				
			switch (s.charAt(i)){
				case '(': p.apilar(new Character('('));
					break;
				case ')': if(new Character('(').equals(p.tope())){
							p.desapilar();
						}else{
							b = false;
						}
					break;
				case '[': p.apilar('[');
					break;
				case ']':  if(new Character('[').equals(p.tope())){
					p.desapilar();
				}else{
					b = false;
				}
				break;
			}
			i++;
		}
		
		return (b && p.esVacia());
	}
	
	public static void main(String[] args){
		System.out.println(balanceada(args[0]));
		System.out.println(args[0]);
	}
}
