package practica2.ejercicio08;

class PilaArreg implements Pila{
	private int MAX = 200;
	private Object[] arreg = new Object[MAX];
	private int cantElem;

	/* Se fija si la pila esta vacia */
	public boolean esVacia(){
		return (cantElem == 0);
	}

	public void vaciar(){
		cantElem = 0;
	}

	public int longitud(){
		return cantElem;
	}

	public void apilar(Object a){
		if(cantElem != MAX){
			arreg[cantElem] = a;
			cantElem++;
		}else{
			Errores e = new Errores();
			e.llenito();
		}
	}

	public void desapilar(){
		if(cantElem>0){
			cantElem -= 1;
		}else{
			Errores e = new Errores();
			e.vacioException();
		}
		

	}

	public Object tope(){
		if(!esVacia()){
			return arreg[cantElem-1];
		}else{
			Errores e = new Errores();
			e.vacioException();
			return null;
		}
	}
}

class Nodo extends Object{
	private Object info;
	private Nodo next;
	
	public void init(){
		info = null;
		next = null;
	}
	
	public void setNext(Nodo e){
		this.next = e;
	}
	
	public Nodo nextNodo(){
		 return this.next;
	}
	
	public void setInfo(Object e){
		this.info = e;
	}
	
	public Object showInfo(){
		return this.info;
	}
}


class PilaPunt implements Pila{
	private Nodo cabeza, aux;
	private int cantElem;
	
	/**
	 * Inicia la Pila
	 */
	public void init(){
		cabeza = new Nodo();
		cabeza.init();
		cantElem = 0;
	}
	/*
	 *Devuelve si es vacia (Boolean) 
	 */
	@Override
	public boolean esVacia() {
		return cantElem == 0;
	}
	
	/* Vacia la pila dejando solamente la cabeza
	 * (non-Javadoc)
	 * @see practica2.ejercicio8.Pila#vaciar()
	 */
	@Override
	public void vaciar(){
		while(cantElem > 0){
			cabeza = cabeza.nextNodo();
			cantElem--;
		}
	}

	@Override
	public int longitud(){
		return cantElem;
	}

	@Override
	public void apilar(Object item){
		aux = new Nodo();
		aux.init();
		aux.setInfo(item);
		aux.setNext(cabeza);
		cabeza = aux;
		cantElem++;
	}
	
	@Override
	public void desapilar(){
		if(!esVacia()){
			cabeza = cabeza.nextNodo();
		}
		cantElem--;
	}
	
	@Override
	public Object tope(){
		return cabeza.showInfo();
	}
}