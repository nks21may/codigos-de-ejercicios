package practica2.ejercicio08;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		PilaArreg a = new PilaArreg();
		PilaArreg l = new PilaArreg();
		Scanner teclado = new Scanner(System.in);
		//Integer a = new Integer(0);
		int i = 0;
		
		//carga
		do{
			System.out.println("Ingrese el numero que quiere agregar a la pila");
			i = teclado.nextInt();
			a.apilar(i);
			l.apilar(i);
		}while (i != 666);
		
		//pila arreglo
		System.out.print("Con la lista en arreglos:");
		while(!a.esVacia()){
			System.out.print(" "+a.tope());
			a.desapilar();
		}
		System.out.println();
		
		//Con la lista de punteros
		System.out.print("Con la lista en punteros:");
		while(!l.esVacia()){
			System.out.print(" "+l.tope());
			l.desapilar();
		}
		System.out.print("/n");
		
		teclado.close();
	}

}
