package practica2;

public class ColaPunt{
	private Nodo cabeza;
	private Nodo cola;
	private int cantElem; 
	
	public ColaPunt(){
		cabeza = null;
		cola = cabeza;
		cantElem = 0;
	}
	
	public boolean esVacia() {
		return (cantElem == 0);
	}

	public void vaciar() {
		cabeza = new Nodo();
		cola = cabeza;
		cantElem = 0;
	}

	public int longitud() {
		return cantElem;
	}

	public void encolar(Object item) throws RuntimeException {
		Nodo aux = new Nodo();
		aux.setInfo(item);
		if (cantElem == 0){
			cabeza = aux;
			cola = aux;
		}else{
			cabeza.setNext(aux);
			cabeza = aux;
		}
		cantElem++;
	}

	public void desencolar(){
		if (cantElem > 0){
			cantElem--;
			cola = cola.nextNodo();
		}
	}

	public Object showUltimo(){
		return cola.showInfo();
	}
	
	public Object sacar(){
		if (cantElem > 0){
			cantElem--;
			cola = cola.nextNodo();
			return cola.showInfo();
		}
		return null;
	
	}
	
	
	
}