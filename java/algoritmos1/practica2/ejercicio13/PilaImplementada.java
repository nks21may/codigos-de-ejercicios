package practica2.ejercicio13;

class PilaArreg implements Pila{
	private final int MAX = 200;
	private Object[] arreg;
	private int cantElem;

	public PilaArreg(){
		cantElem = 0;
		arreg  = new Object[MAX];
	}
	
	/* Se fija si la pila esta vacia */
	public boolean esVacia(){
		return (cantElem == 0);
		
	}

	public void vaciar(){
		cantElem = 0;
	}

	public int longitud(){
		return cantElem;
	}

	public void apilar(Object a){
		if(cantElem != MAX){
			arreg[cantElem] = a;
			cantElem++;
		}else{
			Errores e = new Errores();
			e.llenito();
		}
	}

	public void desapilar(){
		if(cantElem>0){
			cantElem -= 1;
		}else{
			Errores e = new Errores();
			e.vacioException();
		}
		

	}

	public Object tope(){
		if(!esVacia()){
			return arreg[cantElem-1];
		}else{
			Errores e = new Errores();
			e.vacioException();
			return null;
		}
	}
}

class Nodo extends Object{
	private Object info;
	private Nodo next;
	
	public Nodo(){
		info = null;
		next = null;
	}
	
	public void setNext(Nodo e){
		this.next = e;
	}
	
	public Nodo nextNodo(){
		 return this.next;
	}
	
	public void setInfo(Object e){
		this.info = e;
	}
	
	public Object showInfo(){
		return this.info;
	}
}


class PilaPunt implements Pila{
	private Nodo cabeza;
	private int cantElem;
	
	/**
	 * Inicia la Pila
	 */
	public PilaPunt(){
		cabeza = new Nodo();
		cantElem = 0;
	}
	
	/*
	 *Devuelve si es vacia (Boolean) 
	 */
	@Override
	public boolean esVacia() {
		return cantElem == 0;
	}
	
	/* Vacia la pila dejando solamente la cabeza
	 * @see practica2.ejercicio8.Pila#vaciar()
	 */
	@Override
	public void vaciar(){
		while(cantElem > 0){
			cabeza = cabeza.nextNodo();
			cantElem--;
		}
	}

	@Override
	public int longitud(){
		return cantElem;
	}

	@Override
	public void apilar(Object item){
		Nodo aux = new Nodo();
		aux.setInfo(item);
		aux.setNext(cabeza);
		cabeza = aux;
		cantElem++;
	}
	
	@Override
	public void desapilar(){
		if(!esVacia()){
			cabeza = cabeza.nextNodo();
		}
		cantElem--;
	}
	
	@Override
	public Object tope(){
		return cabeza.showInfo();
	}
}

class ColaPunt{
	private Nodo cabeza;
	private Nodo cola;
	private int cantElem; 
	
	public ColaPunt(){
		cabeza = null;
		cola = cabeza;
		cantElem = 0;
	}
	
	public boolean esVacia() {
		return (cantElem == 0);
	}

	public void vaciar() {
		cabeza = new Nodo();
		cola = cabeza;
		cantElem = 0;
	}

	public int longitud() {
		return cantElem;
	}

	public void apilar(Object item) throws RuntimeException {
		
		Nodo aux = new Nodo();
		aux.setInfo(item);
		if (cantElem == 0){
			cabeza = aux;
			cola = aux;
		}else{
			cabeza.setNext(aux);
			cabeza = aux;
		}
		cantElem++;
	}

	public void desapilar(){
		if (cantElem > 0){
			cantElem--;
			cola = cola.nextNodo();
		}
	}

	public Object ultimo(){
		return cola.showInfo();
	}
	
}