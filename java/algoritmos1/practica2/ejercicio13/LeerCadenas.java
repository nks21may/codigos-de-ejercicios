package practica2.ejercicio13;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LeerCadenas {
	
	public static void main(String [] args) {
		ColaPunt lista = new ColaPunt();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			boolean go = true;
			String i, input;
			
			while(go){
				System.out.println("Ingrese una palabra: ");
				input = (reader.readLine());					
				if (input.equals("q")) {
					System.out.println("Finalizado!");
					go = false;
				}else{
					System.out.println("Ingrese la cantidad: ");
					i = (reader.readLine());
					Object[] item = new Object[2];
					item[0] = input;
					item[1] = Integer.parseInt(i);
					lista.apilar(item);
				}
				System.out.println("Leido: " + input);
				System.out.println("-----------");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("La lista contiene los siguientes articulos: ");
		System.out.println("-------------------------------------------");
		while(!lista.esVacia()){
			
			Object[] item = (Object[]) lista.ultimo();
			System.out.println(item[0]+"     ["+item[1]+"]");
			lista.desapilar();
		}
	}
	
}
