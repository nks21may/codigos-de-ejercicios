package practica2.ejercicio14;
public class Main{
	private static Lista listeta;
	private static Lista esplist;
	
	public static void inic(){
		listeta = new Lista();
		esplist = new Lista();
	}
	
	public static void main(String[] args){
		inic();
		listeta.insertar(new Nodo("hola"));
		esplist.insertar(new Nodo("como estas?"));
		
		listeta.union(esplist);
		listeta.mostrarAll();
		
	}
}