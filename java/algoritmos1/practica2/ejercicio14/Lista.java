package practica2.ejercicio14;

public class Lista{
	private int cantElem;
	private Nodo cabeza;
	
	public Lista(){
		cantElem = 0;
		cabeza = null;
	}
	
	public boolean esVacia() {
		return cantElem == 0;
	}

	public void vaciar(){
		cantElem = 0;
		cabeza = null;
	}
	
	public void insertar(Nodo x, int j) throws Exceptions{
		if(j <= this.cantElem && j>=0){
			Nodo aux = cabeza;
			for(int i = 1; i < j; i++){
				aux = aux.nextNodo(); 
			}
			x.setNext(aux.nextNodo());
			aux.setNext(x);
		}else{
			throw new Exceptions ("No se pudo meter");
		}
	}
	
	public void insertar(Nodo x){
		x.setNext(cabeza);
		cabeza = x;
		cantElem++;
	}
	
	public void borrar(int j)throws Exceptions{
		if(j <= this.cantElem && j>=0){
			Nodo aux = cabeza;
			for(int i = 0; i< j; i++){
				aux = aux.nextNodo(); 
			}
			aux.setNext((aux.nextNodo()).nextNodo());
		}else{
			throw new Exceptions ("No se puede borrar");
		}
	}
	
	public boolean pertenece(Nodo x){
		Nodo aux = cabeza;
		while(x != null){
			if(x.showInfo() == aux.showInfo()){
				return true;
			}
			aux = aux.nextNodo();
		}
		return false;
	}
	
	public void mostrarAll(){
		Nodo aux = cabeza;
		while(aux != null){
			System.out.println(aux.showInfo());
			aux = aux.nextNodo();
		}
	}
	
	public Lista copia(){
		Lista aux = new Lista();
		Nodo p = cabeza;
		int i = 0;
		while(i<= cantElem){
			try {
				Nodo c = new Nodo();
				//c.setInfo((p.showInfo()).clone());
				aux.insertar(c, i);
				p = p.nextNodo();
			} catch (Exceptions e) {
				e.printStackTrace();
			}
		}
		
		return aux;
	}
	
	
	public void union(Lista l){
		Nodo aux;
		boolean pri;
		if(l.cabeza != null && this.cabeza != null){
			if(this.cantElem <= l.cantElem){
				aux = this.cabeza;
				pri = true;
			}else{
				aux = l.cabeza;
				pri = false;
			}
			
			while(aux.nextNodo() != null){
				aux = aux.nextNodo();
			}
			if(pri){
				aux.setNext(l.cabeza);
			}else{
				aux.setNext(this.cabeza);
				this.cabeza = l.cabeza;
			}
		}else{
			if(this.cabeza == null && l.cabeza != null){
				this.cabeza = l.cabeza;
			}
		}
	}
}