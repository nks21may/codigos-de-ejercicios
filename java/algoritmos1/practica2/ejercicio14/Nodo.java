package practica2.ejercicio14;

class Nodo extends Object{
	public Object info;
	private Nodo next;
	
	public Nodo(){
		info = null;
		next = null;
	}
	
	public Nodo(Object e){
		info = e;
		next = null;
	}
	
	public Nodo(Object e, Nodo n){
		info = e;
		next = n;
	}
	
	public void setInfo(Object e){
		this.info = e;
	}
	
	public Object showInfo(){
		return this.info;
	}
	
	public void setNext(Nodo e){
		this.next = e;
	}
	
	public Nodo nextNodo(){
		 return this.next;
	}
	
	public Object clone(){  
	    try{  
	        return super.clone();  
	    }catch(Exception e){ 
	        return null; 
	    }
	}
}

