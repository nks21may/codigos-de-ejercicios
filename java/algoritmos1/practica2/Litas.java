package practica2;

public interface Litas<T>{
	public abstract boolean esVacia();
	public abstract void vaciar();
	public abstract void apilar(T a, int i);
	public abstract void eliminar(int i);
	public abstract int longitud();
	public abstract T info();	
}

