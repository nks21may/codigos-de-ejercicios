package practica2;

class Nodo extends Object{
	private Object info;
	private Nodo next;
	
	public Nodo(){
		info = null;
		next = null;
	}
	
	public void setNext(Nodo e){
		this.next = e;
	}
	
	public Nodo nextNodo(){
		 return this.next;
	}
	
	public void setInfo(Object e){
		this.info = e;
	}
	
	public Object showInfo(){
		return this.info;
	}
}

