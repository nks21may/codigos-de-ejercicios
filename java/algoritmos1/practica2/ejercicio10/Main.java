package practica2.ejercicio10;
import java.util.Arrays;

class PilaArregDinamic implements Pila{
	private int capacidad;
	private Object[] data;
	private int cantElem;

	public PilaArregDinamic(){
		capacidad = 16;
		data = new Object[capacidad];
		cantElem = 0;
	}
	
	public void changeSizeArray(){
		if(capacidad == cantElem){
			capacidad *= 2;
			data = Arrays.copyOf(data, capacidad);
		}
	}
	
	/* Se fija si la pila esta vacia */
	public boolean esVacia(){
		return (cantElem == 0);
	}

	public void vaciar(){
		cantElem = 0;
	}

	public int longitud(){
		return cantElem;
	}

	public void apilar(Object a){
		if(cantElem != capacidad){
			data[cantElem] = a;
			cantElem++;
		}else{
			Errores e = new Errores();
			e.llenito();
		}
	}

	public void desapilar(){
		if(cantElem>0){
			cantElem -= 1;
		}else{
			Errores e = new Errores();
			e.vacioException();
		}
		

	}

	public Object tope(){
		if(!esVacia()){
			return data[cantElem-1];
		}else{
			Errores e = new Errores();
			e.vacioException();
			return null;
		}
	}
}