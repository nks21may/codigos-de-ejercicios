package practica2.ejercicio04;

import java.util.Scanner;

class Four{
	private long a,b;

	public Four(long num, long de){
		if (de == 0){
			a = 0;
			b = 0;
		}else{
			a = num;
			b = de;
		}
	}
//throws	
	public Four suma(Four p){
		return new Four((this.a * p.b) + (p.a * this.b),this.b * p.b);
	}

	public Four resta(Four p){
		Four aux = new Four(0,0);
		aux.a = (this.a * p.b) - (p.a * this.b);
		aux.b = this.b * p.b;
		
		return aux;
	}

	public boolean equals(Four p){
		return (this.a / this.b == p.a / p.b);
	}

	public void mostrar(){
		System.out.println(this.a+"/"+this.b);
	}

	public void cargar(){
		Scanner teclado = new Scanner(System.in);
		this.a = teclado.nextLong();
		this.b = teclado.nextLong();
		teclado.close();
	}

	public static void main(String[] args) {
		Four a = new Four(0,0);
		Four b = new Four(0,0);
		Four c = new Four(0,0);
		
		a.cargar();
		b.cargar();

		a.mostrar();
		b.mostrar();

		c = a.suma(b);
		c.mostrar();

		c = a.resta(b);
		c.mostrar();

		System.out.println(a.equals(b));
		
	}
}