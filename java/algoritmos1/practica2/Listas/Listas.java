package practica2.Listas;

class Listas{
	int cantElem;
	Nodo cabeza;
	
	public Listas(){
		cantElem = 0;
		
	}
	
	public boolean esVacia(){
		return (cantElem == 0);
	}
		
	public void vaciar(){
		cantElem = 0;
		cabeza = new Nodo();
	}
	
	public int longitud(){
		return cantElem;
	}

	public void apilar(Object item, int i){
		Nodo aux = new Nodo();
		aux.setInfo(item);
		Nodo mv = cabeza;
		
		while(mv.nextNodo() != null){
			mv = mv.nextNodo();
		}
		mv.setNext(aux);
	} 
	
	
	public void desapilar(){}
	

	public Object infoEn(int i){
		Nodo aux = cabeza;
		return aux.showInfo();
	}
}

class Nodo extends Object{
	private Object info;
	private Nodo next;
	
	public Nodo(){
		info = null;
		next = null;
	}
	
	public void setNext(Nodo e){
		this.next = e;
	}
	
	public Nodo nextNodo(){
		 return this.next;
	}
	
	public void setInfo(Object e){
		this.info = e;
	}
	
	public Object showInfo(){
		return this.info;
	}
}
