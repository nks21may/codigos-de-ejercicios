package practica2.ejercicio11;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LectorDeArchivos {

	/* Uso: LectorDeArchivos archivo.txt n, donde:
		- archivo.txt es el nombre de un archivo de texto valido
		- n es un entero
		*/
	public static void main(String [] args) {
		ColaPunt datos = new ColaPunt();
		
		if (args.length != 2 && Integer.parseInt(args[1]) >= 0) 
			System.out.println("Error en los argumentos. Invocar con LectorDeArchivos archivo.txt n");
	
		try {
			BufferedReader reader = new BufferedReader(new FileReader(args[0]));
			/* TODO: Actualmente imprime todo el archivo. Modificar para que imprima 
				solo las ultimas n lineas
				*/
			String linea;
			while ((linea = reader.readLine()) != null){
				if (linea != null && datos.longitud() < Integer.parseInt(args[1])){
					datos.apilar(linea);
				}else if(linea != null){
					datos.apilar(linea);
					datos.desapilar();
				}
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("Error al leer el archivo: " + args[0]);
		}finally{
			while(!datos.esVacia()){
				System.out.println(datos.ultimo());
				datos.desapilar();
			}
		}
	}
}

