package recupParcial;


public class Polinomio implements intPolinomios{
	public int[] formula;
	final int max = 10;
	
	
	public Polinomio(){
		formula = new int[max];
		for(int i = 0; i<max; i++){
			formula[i] = 0; 
		}
	}
	
	public Polinomio(int n){
		formula = new int[max];
		for(int i = 0; i< max; i++){
			formula[i] = n; 
		}
	}
	
	
	public int potencia(int x, int n){
		int res = 1;
		for (int i=0; i < n; i++){
			res *= x;
		}
		return res;
	}
	
	public Polinomio suma(Polinomio a){
		Polinomio aux = new Polinomio();
		for(int i = 0; i< max; i++){
			aux.formula[i] = this.formula[i] + a.formula[i];
		}
		return aux;
	}

	public int evaluar(int n) {
		int ev = 0;
		for(int i=0; i < max;i++){
			ev += this.formula[i] * potencia(n,i);
		}
		return ev;
		
	}

	@Override
	public Polinomio resta(Polinomio a) {
		Polinomio aux = new Polinomio();
		for(int i = 0; i< max; i++){
			aux.formula[i] = this.formula[i] - a.formula[i];
		}
		return aux;
	}	// TODO Auto-generated method stub
		
	}
	
	//public void suma(Polinomio a);
	//public void evaluar(int n);
	

