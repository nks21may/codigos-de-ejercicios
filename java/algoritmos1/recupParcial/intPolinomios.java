package recupParcial;


public interface intPolinomios{
	
	public Polinomio suma(Polinomio a);
	public Polinomio resta(Polinomio a);
	public int evaluar(int n);
	
}
