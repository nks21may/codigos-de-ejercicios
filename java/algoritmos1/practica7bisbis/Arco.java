package practica7bisbis;

public class Arco{
	
	private int costo;
	private Nodo referencia;
	
	public Arco(){
		costo = 0;
		referencia = null;
	}
	
	public Arco(Nodo e){
		referencia = e;
		costo = 0;
	}
	
	public Arco(Nodo e, int s){
		referencia = e;
		costo = s;
	}
	
	public void setRef(Nodo e){
		referencia = e;
	}
	
	public Nodo getRef(){
		return referencia;
	}
	
	public void setCosto(int e){
		costo = e;
	}
	
	public int getCosto(){
		return costo;
	}
	
	public String toString(){
		return "["+referencia.toPalabra()+"•"+costo+"]->";
	}
	
	public boolean equals(Object obj) {
	    if(! (obj instanceof Arco || obj instanceof Nodo)) return false;
	    if(obj instanceof Arco){
	    	return referencia.equals(((Arco) obj).getRef());
	    }
	    return false;
	 }
	
	public int hashCode(){
		return referencia.hashCode();
	}
}