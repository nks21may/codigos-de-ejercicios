package practica7bisbis;

import java.util.ArrayList;
import practica2.ColaPunt;

public class Grafo{
	private ArrayList<Nodo> lista;
	private int cantArcos;
	
	public Grafo(){
		lista = new ArrayList<Nodo>();
		cantArcos = 0;
	}
	
	public boolean esVacio(){
		return lista.isEmpty();
	}
	
	public int cantVertices(){
		return lista.size();
	}
	
	public int cantArcos(){
		return cantArcos;
	}
	
	public ArrayList<Nodo> getLista(){
		return lista;
	}

	public boolean relacion(Nodo a, Nodo b){
		int auxa = lista.indexOf(a);
		int auxb = lista.indexOf(b);
		System.out.println(lista.contains(b));
		if(auxa != -1 && auxb != -1){
			System.out.println(lista.get(auxa).contains(b));
			return lista.get(auxa).contains(b) || lista.get(auxb).contains(a);
		}
		return false;
	}
	
	public void insertarNodo(Nodo e){
		if(!lista.contains(e)){
			lista.add(e);
		}else{
			System.out.println("Elemento no insertado");
	}	}
	
	public void borrarNodo(int e){
		Nodo aux = lista.remove(e);
		if(aux != null){
			for(int i= 0; i< lista.size();i++){
				(lista.get(i)).remove(aux);
				cantArcos--;
			}
		}
	}
	
	//Inserta el nodo "este" en la posicion "pos"
	public void insertarArco(Nodo este, int pos){
		if(pos < lista.size()){
			int lugar = lista.indexOf(este);
			if(lugar != -1){
				Arco aux = new Arco(este);
				if(!(lista.get(pos)).contains(aux)){
					(lista.get(pos)).add(lista.get(lugar));
					cantArcos++;
				}
			}
		}	
	}
	
	public void borrarArco(int i, Nodo e){	
		Arco aux = new Arco(e);
		(lista.get(i)).remove(aux);
		cantArcos--;
	}
	
	public boolean inGrafo(Nodo e){
		return lista.contains(e);
	}
	
	public void imprimir(){
		for (int i = 0; i < lista.size(); i++) {
			Nodo aux =  lista.get(i);
			System.out.println(aux.toString());
		}
		System.out.println("Cantidad de arcos: "+cantArcos);
	}
	
	//Procedimiento DFS. Implementa recursivamente el depth-first search
	public void dfs(Nodo v){
		Nodo temp = new Nodo();
		temp.add(v);
		dfasado(temp);
	}
	
	public void dfasado(Nodo v){ 
		for(int i = 0; i < v.getLista().size(); i++){
			Arco aux = v.getLista().get(i); 
			if(aux.getRef().vistit() != 1){
				aux.getRef().marcar();
				System.out.print(aux.getRef().toPalabra());
				dfasado(aux.getRef());
			}
			aux.getRef().desMarcar();
		}
	}
	
	public void bfs(Nodo v){
		ColaPunt q = new ColaPunt();
		q.encolar(v); // se pone le primer vertice en la cola
		v.marcar();// se lo marca
		while (!q.esVacia()){ // mientras la cola no sea vacia
			Nodo w = (Nodo) q.sacar(); // se saca el primer nodo en la cola de prioridades
			
			//process(w); // se lo procesa
			for(Arco a: w.getLista()){
				if(0 == a.getRef().vistit()){
				 //all unvisited vertices x adjacent to w
				 // para todos los adyacentes no visitado y no en la cola
				 a.getRef().marcar(); // se los marca
				 q.encolar(a.getRef()); // se los encola
				}
			}
			}
		}
}