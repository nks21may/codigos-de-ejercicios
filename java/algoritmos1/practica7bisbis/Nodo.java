package practica7bisbis;

import java.util.LinkedList;

@SuppressWarnings("rawtypes")
public class Nodo{
	
	private Comparable info;
	private LinkedList<Arco> relaciones;
	private int marca;
	
	public Nodo(){
		info = null;
		relaciones = new LinkedList<Arco>();
		marca = 0;
	}
	
	public Nodo(Comparable e){
		info = e;
		relaciones = new LinkedList<Arco>();
		marca = 0;
	}
	
	public void setInfo(Comparable e){
		info = e;
	}
	
	public Comparable getInfo(){
		return info;
	}
	
	//cambiar para mostrar relaciones
	public String toPalabra(){
		return "["+info.toString()+"]";
	}
	
	public String toString(){
		String aux = "["+info.toString()+"]: ";
		for (Arco ar : relaciones) {
			aux = aux + ar.toString();
		}
		return aux;
	}
	
	
	public boolean contains(Nodo b){
		Arco aux = new Arco(b);
		return relaciones.contains(aux);
	}
	
	public boolean contains(Arco b){
		return relaciones.contains(b);
	}

	public void remove(Arco e){
		relaciones.remove(e);
	}
	
	public void remove(Nodo e){
		Arco aux = new Arco(e);
		relaciones.remove(aux);
	}
	
	public void add(Arco e){
		relaciones.add(e);
	}
	
	public void add(Nodo e){
		Arco aux = new Arco(e);
		relaciones.add(aux);
	}

	public void relacionar(Nodo e, int peso){
		Arco aux = new Arco(e, peso);
		relaciones.add(aux);
	}

	public void marcar() {
		marca++;
	}
	
	public void desMarcar(){
		marca--;
	}
	
	public int vistit(){
		return marca;
	}
	
	public LinkedList<Arco> getLista(){
		return relaciones;
	}
	
	public int hashCode(){
		return info.hashCode();
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Nodo)) return false;
	    if (((Nodo) obj).getInfo() == this.getInfo()) return true;
	    return false;
	 }
}