package practica7bisbis;

public class Mein{
	public static void main(String[] args){
		Grafo gra = new Grafo();
		Nodo as = new Nodo(9);
		gra.insertarNodo(as);
		as = new Nodo(2);
		gra.insertarNodo(as);
		as = new Nodo(5);
		gra.insertarNodo(as);
		as = new Nodo(3);
		gra.insertarNodo(as);
		as = new Nodo(7);
		gra.insertarNodo(as);
		
		as = new Nodo(2);
		gra.insertarArco(as, 0);
		as = new Nodo(3);
		gra.insertarArco(as, 0);
		as = new Nodo(7);
		gra.insertarArco(as, 2);
		as = new Nodo(5);
		gra.insertarArco(as, 3);
		as = new Nodo(7);
		gra.insertarArco(as, 4);
		as = new Nodo(7);
		gra.insertarArco(as, 4);
		gra.imprimir();
		as = new Nodo(9);
		
		gra.dfs(gra.getLista().get(0));
		
	}
}