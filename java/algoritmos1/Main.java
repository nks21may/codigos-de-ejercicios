import java.util.ArrayList;
import java.util.List;

class Main{
	
	public static void printArray(Integer arr[]){
		int n = arr.length;
		for (int i=0; i<n; ++i)
			System.out.print(arr[i] + " ");
	    
		System.out.println();
	}
	
	public static void main(String[] args) {
	    Integer[] x = {1,2,3,4};
	    ArrayList<Integer[]> l = new ArrayList<Integer[]>();
	    l.addAll(practica2ejercicio10.desarreglos(x)); 
	    
	    for (Integer[] y : l) {
			printArray(y);
		}
	}
	
	
}

class practica2ejercicio7{
	public static List<List<Integer>> sendMoney() { //  send -> 9567, more -> 1085, money -> 10652
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		for (int i = 0; i < 10; i++) { //s
			for (int j = 0; j < 10; j++) { //e
				for (int j2 = 0; j2 < 10; j2++) {//n
					for (int k = 0; k < 10; k++) {//d
						for (int k2 = 0; k2 < 10; k2++) {//m
							for (int l = 0; l < 10; l++) {//o
								for (int l2 = 0; l2 < 10; l2++) {//r
									for (int m = 0; m < 10; m++) {//y
										if (!(i == j || i == j2 || i == k || i == k2 || i == l || i == l2 || j == j2 || j == k || j == k2 || j == l || j == l2 || j2 == k || j2 == k2  || j2 == l || j2 == l2 || k == k2  || k == l || k == l2 || k2 == l || k2 == l2 || l == l2 || m == i || m == j || m == j2 || m == k || m == k2 || m == l || m == l2 )){
											int o = i*1000+j*100+j2*10+k; //send
											int p = k2*1000+l*100+l2*10+j; //money
											int po = k2*10000+l*1000+j2*100+j*10+m;
											if(o+p==po){
												ArrayList<Integer> a = new ArrayList<Integer>();
												a.add(o);
												a.add(p);
												a.add(po);
												res.add(a);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return res;
	}
}

class practica2ejercicio10{
	
	public static boolean esDesarreglo(Integer[] ori, Integer[] per){
		boolean res = true;
		if(ori.length == per.length){
			for (int i = 0; i < ori.length && res; i++){
				res &= ori[i] != per[i];
			}
		}
		return res;
	}
	
	public static List<Integer[]> insertador(Integer[] a, int x) {
		ArrayList<Integer[]> lista = new ArrayList<Integer[]>();
		for(int i = 0; i < a.length+1; i++){
			ArrayList<Integer> aux = new ArrayList<Integer>(); 
			for (Integer y : a)
				aux.add(y);
			aux.add(i, x);
			Integer[] r = new Integer[aux.size()];
			int j = 0;
			for (Integer z : aux) {
				r[j] = z;
				j++;
			}
			lista.add(r);
		}
		return lista;
	}

	public static List<Integer[]> generaDesarreglos(Integer[] a){
		ArrayList<Integer[]> lista = new ArrayList<Integer[]>();
		Integer[] aux = new Integer[0];
		lista.add(aux);
		List<Integer[]> temp = null;
		for (Integer x: a){
			for (Integer[] l: lista) {
				temp = insertador(l, x);
			}
			lista.clear();
			lista.addAll(temp);
		}
		return lista;
	}
	
	public static List<Integer[]> desarreglos(Integer[] a) {
		List<Integer[]> lista = generaDesarreglos(a);
		ArrayList<Integer[]> res = new ArrayList<Integer[]>();
		for (Integer[] i : lista) {
			if(esDesarreglo(a, i))
				res.add(i);
		}
		return res;
	}
	
}

class practica3ejercicio2{
	public static int mergeSort(int[] array, int begin, int end){
		int res = 0;
		if (begin < end){
			int mid = (begin + end)/2;
			res += mergeSort(array, begin, mid);//ordena la primera mitad
			res += mergeSort(array, mid+1, end);//ordena la segunda mitad
			res += merge(array, begin, mid, end);//mezcla las mitades ordenadas
		}
		return res;
	}
	
	// merge: mezcla dos partes consecutivas de array
	// pre: 0 <= begin, mid, end <= array.lenght
	private static int merge(int[] array, int begin, int mid, int end){
		int i = begin;
		int j = mid+1;
		int k = 0;
		int[] aux = new int[end-begin+1];
		int res = 0;
		boolean x = false;
		
		while(i <= mid && j <= end){
			if(array[i] < array[j]){
				aux[k] = array[i];
				i++;
			}else{
				aux[k] = array[j];
				j++;
				x = true;
			}
			k++;
		}
		while(i <= mid){
			aux[k] = array[i];
			i++; k++;
			if(x) 
				res++;
		}
		while(j <= end){
			aux[k] = array[j];
			j++; k++;
			if(x) 
				res++;
		}
		for (k=0; k < end-begin+1; k++){
			array[begin+k] = aux[k];
		}
		return res;
	}
}

class practica3ejercicio4{ //pendiente 
	
	public static void mergeSort(int[] array, int begin, int end){
		if (begin < end){
			int mid = (begin + end)/2;
			mergeSort(array, begin, mid);//ordena la primera mitad
			mergeSort(array, mid+1, end);//ordena la segunda mitad
			merge(array, begin, mid, end);//mezcla las mitades ordenadas
		}
	}
	
	// merge: mezcla dos partes consecutivas de array
	// pre: 0 <= begin, mid, end <= array.lenght
	private static void merge(int[] array, int begin, int mid, int end){
		int i = mid;
		int j = mid+1;
		int k = 0;
		int[] aux = new int[end-begin+1];
		int acum = array[mid];
		i--;
		while(begin <= i){
			if(acum > acum + array[i])
				acum = acum + array[i];
		}
		while(j <= end){
			aux[k] = array[j];
			j++; k++;
		}
		for (k=0; k < end-begin+1; k++)	
			array[begin+k] = aux[k];
		
	}
}
