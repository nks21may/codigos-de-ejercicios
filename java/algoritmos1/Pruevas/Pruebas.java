package Pruevas;

public class Pruebas{
    
    public static void main(String args[]) {
    	Integer a = new Integer(1222234);
    	Integer b = new Integer(10000);
    	System.out.println(a.compareTo(b));
    }
    
    //n es el numero a analizar y p es la posicion de la que se espera obtener el numero
    public static int sep(Integer n, int p){
    	char[] e = n.toString().toCharArray();
    	if(e.length-p >= 0){
    		return (Character.getNumericValue(e[e.length-p]));
    	}else{
    		return 0;
    	}
    }
    public static void countingSort(Integer[] array, int p){
    	if(array.length != 0){
			int[] cont = new int[10];
			Integer[] out = new Integer[array.length];
			
			//contador de apariciones
			for(int i=0; i<array.length; i++){
				int dig = sep(array[i],p);
				cont[dig]++;
			}
			
			//acumulada
			for(int i = 1; i<array.length; i++){
				cont[i] += cont[i-1];
			}
			
			for(int i = 0; i<array.length; i++){
				int dig = sep(array[i],p);
				
				out[cont[dig]-1] = array[i];
				cont[dig]--;
			}
			array = out;
		}
	}
    
    
    public static int max(Integer[] a){
		Integer aux = a[0];
		for(int i = 0; i<= a.length-1; i++){
			if(aux.intValue() < a[i].intValue()){
				aux = a[i];
			}
		}
		return aux.intValue();
	}
	

}
