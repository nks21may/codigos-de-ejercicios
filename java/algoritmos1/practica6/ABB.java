package practica6;

@SuppressWarnings("rawtypes")
public class ABB{
	
	private TreeNode raiz;
	
	public ABB(){
		raiz = null;
	}
	
	public ABB(TreeNode e){
		raiz = e;
	}
	
	public ABB(Comparable e){
		raiz = new TreeNode(e);
	}
	
	public Comparable getRoot() {
		return raiz.getInfo();
	}
	
	public TreeNode getRot() {
		return raiz;
	}
	
	//Setea la raiz
	public void setRoot(Comparable item){
		raiz.setInfo(item);
	}
	public void setRoot(TreeNode e){
		raiz = e;
	}
	
	//Dice si el arbol es vacio
	public boolean isEmpty() {
		return raiz == null;
	}
	
	//Remueve todo los nodos del arbol
	public void makeEmpty() {
		raiz = null;
	}
	
	//recorrido preOrder
	public void printPreOrder(){
		raiz.printPreOrden();
		System.out.println();
	}
	
	//recorrido postOrder
	public void printPostOrder(){
		raiz.printPostOrden();
		System.out.println();
	}
	
	//recorrido inOrder
	public void printInOrder(){
		raiz.printInOrden();
		System.out.println();
	}
	
	public Comparable[] returnInOrden(){
		return raiz.returnInOrden();
	}
	
	public TreeNode buscar(Comparable e){
		if(raiz != null){
			return raiz.buscar(e);
		}else{
			return null;
		}
	}
	
	public int insertar(Comparable e){
		if(raiz != null){
			if(null == raiz.buscar(e)){
				return raiz.insertar(e);	
			}else{
				return -1;
			}
		}else{
			raiz = new TreeNode(e);
		}
		return 0;
		
	}
	public void borrar(Comparable e){
		if(raiz != null && raiz.buscar(e) != null){
			raiz.borrar(e);
		}else{
			System.out.println("El elemento nunca estuvo");
		}
	}
	
	public boolean repOk(){
		if(raiz == null){
			return true;
		}else{
			return raiz.repOk();
		}
	}
}