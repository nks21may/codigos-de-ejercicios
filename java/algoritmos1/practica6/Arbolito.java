package practica6;

public class Arbolito implements BinaryTreeBasis{
	//Devuelve el elemento de la raiz
	private TreeNode raiz;
	
	public Arbolito(){
		raiz = new TreeNode();
	}
	
	public Arbolito(TreeNode e){
		raiz = e;
	}
	
	public Object getRoot() {
		return raiz.getInfo();
	}
	
	//Setea la raiz
	@SuppressWarnings("rawtypes")
	public void setRoot(Comparable item){
		raiz.setInfo(item);
	}
	
	//Dice si el arbol es vacio
	public boolean isEmpty() {
		return raiz == null;
	}
	
	//Remueve todo los nodos del arbol
	public void makeEmpty() {
		raiz = null;
	}
	
	//recorrido preOrder
	public void printPreOrder(){
		raiz.printPreOrden();
		System.out.println();
	}
	
	//recorrido postOrder
	public void printPostOrder(){
		raiz.printPostOrden();
		System.out.println();
	}
	
	//recorrido inOrder
	public void printInOrder(){
		raiz.printInOrden();
		System.out.println();
	}
}