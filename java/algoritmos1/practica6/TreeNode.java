
package practica6;

@SuppressWarnings({"rawtypes", "unchecked"})
public class TreeNode{
	private Comparable element; // elemento del nodo
	private TreeNode left; // hijo izquierdo
	private TreeNode right; // hijo derecho
	
	// constructor del NodoArbol por defecto
	public TreeNode(){
		left = null;
		right = null;
	}
	
	public TreeNode(Comparable e){
		element = e;
		left = null;
		right = null;
	}
	
	public TreeNode(Comparable e, TreeNode l, TreeNode r){
		element = e;
		left = l;
		right = r;
	}
	
	public Comparable getInfo(){
		return this.element;
	}
	
	public void setInfo(Comparable e){
		this.element = e;
	}
	
	public TreeNode getHD(){
		return this.right;
	}
	
	public TreeNode getHI(){
		return this.left;
	}
	
	public void setHD(TreeNode e){
		this.right = e;
	}
	
	public void setHI(TreeNode e){
		this.left = e;
	}
	
	public void printPreOrden(){
		if(this.element != null){
			System.out.print(this.element+" ");
		}
		TreeNode aux;
		if(this.left != null){
			aux = this.left;
			aux.printPreOrden();
		}
		if(this.right != null){
			aux = this.right;
			aux.printPreOrden();
		}
	}
	
	public void printPostOrden(){
		TreeNode aux;
		if(this.left != null){
			aux = this.left;
			aux.printPostOrden();
		}
		if(this.right != null){
			aux = this.right;
			aux.printPostOrden();
		}
		if(this.element != null){
			System.out.print(this.element+" ");
		}
	}
	
	public void printInOrden(){
		TreeNode aux;
		if(this.left != null){
			aux = this.left;
			aux.printInOrden();
		}
		if(this.element != null){
			System.out.print(this.element+" ");
		}
		if(this.right != null){
			aux = this.right;
			aux.printInOrden();
		}
	}
	
	public static Comparable[] ConcatenarArray(Comparable[] o1, Comparable[] o2){
		Comparable[] ret = new Comparable[o1.length + o2.length];
		System.arraycopy(o1, 0, ret, 0, o1.length);
		System.arraycopy(o2, 0, ret, o1.length, o2.length);
		return ret;
	}
	
	public Comparable[] returnInOrden(){
		Comparable[] arr = new Comparable[0];
		TreeNode aux;
		if(this.left != null){
			aux = this.left;
			arr = ConcatenarArray(arr,aux.returnInOrden());
		}
		if(this.element != null){
			Comparable[] ret = new Comparable[1]; 
			ret[0] = element;
			arr = ConcatenarArray(arr,ret);
		}
		if(this.right != null){
			aux = this.right;
			arr = ConcatenarArray(arr,aux.returnInOrden());
		}
		return arr;
	}
	
	public int compareTo(Comparable e){
		return this.element.compareTo(e);
	}
	
	public int compareTo(TreeNode e){
		return this.element.compareTo(e.getInfo());
	}
	
	//estaba en el arbol
	public TreeNode buscar(Comparable e){
		switch (element.compareTo(e)) {
			case 0:  return this;
			case 1: if(left != null){
						return left.buscar(e);
					}else{
						return null;
					}	
			case -1: if(right != null){
						return right.buscar(e);
					}else{
						return null;
					}
        }
		return null;
		
	}
	
	public int insertar(Comparable e){
		switch (this.element.compareTo(e)){
			case 0: return -1;
			case 1: if(left != null){
						left.insertar(e);
					}else{
						left = new TreeNode(e);
					}				
				break;
			case -1: if(right != null){
						right.insertar(e);
					}else{
						right = new TreeNode(e);
					}
				break;
		}
		return 0;
	}
		
	public boolean repOk(){
		boolean res = true;
		if(right != null){
			res = res && element.compareTo(right.element) == -1 && right.menon(element);
			if(res){
				res = right.repOk() && res; 
			}
		}
		if(left != null){
			res = res && element.compareTo(left.element) == 1 && left.longuevo(element);
			if(res){
				res = left.repOk() && res;
			}
		}
		return res;
	}
	
	public boolean menon(Comparable e){
		boolean res = true;
		if(right != null){
			res = right.getInfo().compareTo(e) == 1;
			if(res){
				res = right.menon(e) && res;
			}
		}
		if(left != null){
			res = left.getInfo().compareTo(e) == 1 && res;
			if(res){
				res = left.menon(e) && res;
			}
		}
		return res;
	}
	
	public boolean longuevo(Comparable e){
		boolean res = true;
		if(right != null){
			res = right.getInfo().compareTo(e) == -1 && res;
			if(res){
				res = right.longuevo(e) && res;
			}
		}
		if(left != null){
			res = left.getInfo().compareTo(e) == -1 && res;
			if(res){
				res = left.longuevo(e) && res;
			}
		}
		return res;
	}
	
	
	
	
	
	private Comparable elegido() {
		Comparable e = null;
		TreeNode aux = this.getHI();
		if(aux.getHD() != null){
			if(this.getHI().getHD().eshoja()){
				e = aux.getHD().getInfo();
				aux.setHD(null);
			}else{
				e = aux.getInfo();
				while (null != aux.getHD().getHD()){
					aux = aux.getHD();
					e = aux.getHD().getInfo();
				}
				aux.setHD(null);
			}
		}
		return e;
	}	
	
	private boolean eshoja() {
		return (this.getHD() == null && this.getHI() == null);
	}

	//usar solo si el elemento pertenece al arbol
	public TreeNode padre(Comparable e){
		TreeNode dad = null;
		TreeNode aux = this;
		boolean f = false;

		while(!f && !(aux.getHI() == null && aux.getHD() == null)){
			switch (aux.getInfo().compareTo(e)) {
				case 0: f = true;
					break;
				case 1: if(aux.getHI() != null){
							dad = aux;
							aux = aux.getHI();
						}
					break;
				case -1: if(aux.getHD() != null){
							dad = aux;
							aux = aux.getHD();
						}
					break;
			}
		}	
		return dad;
	}
	
	public void borrar(Comparable e) {
		TreeNode paux = padre(e); 
		TreeNode aux = buscar(e);
		
		if(aux.eshoja()){ //caso huerfano
			if(paux.getHD() != null){
				if(paux.getHD().buscar(e) != null){
					paux.setHD(null);
				}
			}
			if(paux.getHI() != null){
				if(paux.getHI().buscar(e) != null){
					paux.setHI(null);
				}
			}
		}else{
			if(aux.getHI() == null && aux.getHD() != null){ //caso 1 hijo derecho
				if(paux.getHI() != null){
					if(paux.getHI().buscar(e) != null){
						paux.setHI(aux.getHD());
					}}
				if(paux.getHD() != null){
					if(paux.getHD().buscar(e) != null){
						paux.setHD(aux.getHD());
					}
				} //fin caso 1 hijo derecho
			}else{
				if(aux.getHI() != null && aux.getHD() == null){//caso 1 hijo izquierdo
					if(paux.getHI() != null){
						if(paux.getHI().buscar(e) != null){
							paux.setHI(aux.getHI());
						}
					}
					if(paux.getHD() != null){
						if(paux.getHD().buscar(e) != null){
							paux.setHD(aux.getHI());
						}
					}//caso 1 hijo izquierdo
				}else{ //caso 2 hijos
					if(aux.getHI() != null && aux.getHD() != null){
						aux.setInfo(aux.elegido());
					}
				}
			}
		}
	}
}