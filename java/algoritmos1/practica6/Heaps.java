package practica6;

import java.util.Arrays;

@SuppressWarnings({"rawtypes","unchecked"})
public class Heaps{
	private Comparable[] arbol;
	private int capacidad = 20;
	private int cantElem = 0;

	public Heaps(){
		arbol = new Comparable[capacidad];
	}
	
	public Comparable first(){
		return arbol[0];
	}
	
	public void changeSizeArray(){
		if(capacidad == cantElem){
			capacidad = capacidad + capacidad/2;
			arbol = Arrays.copyOf(arbol, capacidad);
		}
	}
	
	public boolean esVacio(){
		return cantElem == 0;
	}
	
	public int padre(int i){
		return (i-1)/2;
	}
	
	public int hijoI(int i){
			return (2*i)+1;
	}
	
	public int hijoD(int i){
		return 2*(i+1);
	}
	
	private static void swap(Comparable[] array, int i, int j){
		  Comparable temp = array[i];
	  	  array[i] = array[j];
		  array[j] = temp;
	 }//end swap
	
	public void ordening(int i){
		if(i > 0){
			if(arbol[padre(i)].compareTo(arbol[i]) == 1){
				swap(arbol, padre(i), i);
				ordening(padre(i));
			}
		}
		if(hijoD(i)<= cantElem){
			if(arbol[hijoI(i)] != null && arbol[hijoI(i)].compareTo(arbol[i]) == -1){
				swap(arbol, hijoI(i), i);
				ordening(hijoI(i));
			}
		}
		if(hijoD(i)<= cantElem){
			if(arbol[hijoD(i)] != null && arbol[hijoD(i)].compareTo(arbol[i]) == -1){
				swap(arbol, hijoD(i), i);
				ordening(hijoD(i));
			}
		}
	}
	
	public int esta(Comparable e){
		int res = -1;
		for(int i = 0; i < cantElem && res == -1; i++){
			if(e.compareTo(arbol[i]) == 0){
				res = i;
			}
		}
		return res;
	}
	
	public void printArreglo(){
		for(int i = 0; i< cantElem; i++){
			System.out.print(arbol[i]+" ");
		}
		System.out.println();
	}
	
	public boolean insertar(Comparable e){
		if(esVacio()){
			arbol[0] = e;
			cantElem++;
		}else{
			if(esta(e) == -1){
				changeSizeArray();
				arbol[cantElem] = e;
				ordening(cantElem);
				cantElem++;
			}else{
				return false;
			}
		}
		return true;
	}
	
	public boolean repOk(int i){
		boolean res = true; 
		if(hijoI(i)< cantElem){
			if((arbol[i].compareTo(arbol[hijoI(i)]) == -1)){
				res = res && repOk(hijoI(i));
			}else{
				res = false;
			}
		}
		if(hijoD(i)< cantElem){
			if(arbol[i].compareTo(arbol[hijoD(i)]) == -1){
				res = res && repOk(hijoD(i));
			}else{
				res = false;
			}
		}
		return res;
	}
	
	public void borrar(){
		if(cantElem == 0){
			System.out.println("Vacio");
		}else{
			cantElem--;
			arbol[0] = arbol[cantElem];
			arbol[cantElem] = null;
			ordening(0);
			
		}
	}
	
	public void imprimir(){
		for(int i = 0; i< cantElem; i++){
			System.out.print(arbol[i]+" ");
		}
		System.out.println();
	}
}