package practica6;

public interface BinaryTreeBasis{
	//Devuelve el elemento de la raiz
	public Object getRoot();
	//Setea la raiz
	@SuppressWarnings("rawtypes")
	public void setRoot(Comparable item);
	//Dice si el arbol es vacio
	public boolean isEmpty();
	//Remueve todo los nodos del arbol
	public void makeEmpty();
	//recorrido preOrder
	public void printPreOrder();
	//recorrido postOrder
	public void printPostOrder();
	//recorrido inOrder
	public void printInOrder();
}