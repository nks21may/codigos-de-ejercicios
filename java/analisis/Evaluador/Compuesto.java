package analisis.Evaluador;

import java.util.ArrayList;

public class Compuesto implements InterfaceEvaluador{

	private ArrayList<InterfaceEvaluador> subFunc = new ArrayList<InterfaceEvaluador>();
	
	public boolean add(InterfaceEvaluador e) {
		return subFunc.add(e);
	}
	
	private boolean remove(InterfaceEvaluador x) {
		return subFunc.remove(x);
	}
	
	@Override
	public double evaluar() {
		
		return 0;
	}
	
}