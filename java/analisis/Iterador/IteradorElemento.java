package analisis;

public class IteradorElemento<T> implements InterfaceIterador{

	private Elemento<T> agregado;
	public int posActual = 0;

	public IteradorElemento(Elemento<T> elemento){
		agregado = elemento;
	}

	public Object first(){
		Object res = null;

		if (agregado.data.isEmpty() == false) {
			res = agregado.data.toArray()[0];
			posActual = 0;
		}
		return res;
	}
	public Object siguiente(){
		Object res = null;

		if (posActual < agregado.data.size()) {
			res = agregado.data.toArray()[posActual];
			posActual++;
		}
		return res;
	}


	public boolean hayMas(){
		return posActual < agregado.data.size();
	}

	public Object elementoActual(){
		return agregado.data.toArray()[posActual];
	}
}