package analisis;

public interface InterfaceIterador{
	public Object first();
	public Object siguiente();
	public boolean hayMas();
	public Object elementoActual();
}