package analisis;

import java.util.Vector;

public class Elemento<T> implements InterfaceElemento{
	
	protected Vector<T> data;

	public Elemento(){
		data = new Vector<T>();
	}

	@Override
	public IteradorElemento<T> getIterador() {
		return new IteradorElemento<T>(this);
	}
}