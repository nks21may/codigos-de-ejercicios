public class Persona extends Thread{

	private Cuenta c;
	
	public Persona(Cuenta s){
		c = s;
	}

	@Override
	public void run(){
		c.deposita(1000);
		c.retira(500);
		c.deposita(1000);
		System.out.println(c.getSaldo());
	}
}