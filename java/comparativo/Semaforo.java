public class Semaforo{
	
	private int i = 0;

	public Semaforo(int n){
		i = n;
	}

	public synchronized void p(){
		while(i == 0){
			try{
				wait();
			}catch(InterruptedException e){}
		}
		i--;
	}

	public synchronized void v(){
		i++;
		notifyAll();
	}
}