public class Cuenta{
	
	private Semaforo s = new Semaforo(1);

	private int saldo;
	
	public Cuenta(int saldoInicial) {
		s.p();
		saldo = saldoInicial;
		s.v();
	}

	public void deposita(int monto) {
		s.p();
		saldo += monto;
		s.v();
	}

	public void retira(int monto) {
		s.p();
		saldo -= monto;
		s.v();
	}
	public int getSaldo() { 

		return saldo; 
	}
}